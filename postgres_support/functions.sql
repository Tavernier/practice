-- Functions:
select 
	pn.nspname as schema_name,		-- schema name
	pp.proname as object_name,		-- function name
	pp.proowner,
	pp.proisagg,		-- is an aggregate function?
	pp.proisstrict,		-- will return null if any argument is null
	pp.proretset,		-- returns a result set
	pp.provolatile,
	case pp.provolatile
	 when 'i' then 'immutable'
	 when 's' then 'stable'
	 when 'v' then 'volatile'
	end as provolatile_dsc,
	proargnames,
	pt.typname		-- return data type (record = result set)
from pg_catalog.pg_namespace pn	-- schema
join pg_catalog.pg_proc pp		-- functions and procedures
	on pn.oid = pp.pronamespace
join pg_catalog.pg_type pt
	on pp.prorettype = pt.oid
where pn.nspname not in ('pg_catalog','postgres','public','information_schema')
order by pn.nspname, pp.proname;


-- Some cool built-in functions:

-- Strings:
select
	  'Chic' || 'ago' as string_concat
    , 'Chic' || 'ago' || null as string_concat_null
    , concat('Chic', 'ago', null) as concat_null
    , concat_ws(',', 'one', 'two','three') as concat_with_separator
    , bit_length('howdy') as bits
    , octet_length('howdy') as bytes
    , lower('Chicago') as lower
    , upper('Chicago') as upper
    , position('ic' in 'Chicago') as location_of_substring_in_string
    , quote_literal('O''Connoer') as quote_for_sql_use
    , replace('What in the world?', 'Wh', 'ff') as replace_text  -- case sensitive
    , split_part('one,two,three', ',', 3) as piece_of_string  -- one-based
    , trim(' what the heck? ') as trim_spaces
    , trim(both 'x' from 'xwhat the heck?x') as trim_exes;
    
-- Expressions
select
   case
    when 1=0 then 'yep'
    when 1=2 then 'howdy'
    else 'nope'
   end as case_expression
 , coalesce('one', null, 'two') as first_non_null
 , nullif('apple', 'apple') as return_null_if_values_match
 , nullif('apple', 'orange') as return_first_if_values_differ
 , greatest(10, 2, 34, 8) as largest_value
 , least(10, 2, 34, 8) as smallest_value;


-- Generate series:
select * from generate_series(1, 10, 2) as t(num);  -- 1 to 10 by 2 starting at 1
select * from generate_series('2016-05-20', '2016-05-25', interval '1 day') as t(dt);  -- list of days
select * from generate_series('2016-05-20', '2016-05-25', interval '12 hours') as t(dt);  -- list of days with midnight and noon


-- System info: https://www.postgresql.org/docs/9.6/static/functions-info.html
select
	  current_catalog
    , current_database()
    , current_user
    , version();
    
    
-- Aggregate strings:
select d.state_name, string_agg(d.city_name, ', ' order by d.city_name) as cities
from (
    values
    ('IL','Rockford'),
    ('IL','Chicago'),
    ('WI','Madison')
) d(state_name, city_name)
group by d.state_name;
