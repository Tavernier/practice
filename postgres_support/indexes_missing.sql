-- We may want to add indexes to tables that have a high amount of table scans vs 
-- index activity.  Postgres may prefer to scan small tables though even if we add
-- indexes.
select  
 t.schemaname as schema_name,
 t.relname as table_name,
 t.seq_scan,
 t.idx_scan,
 t.seq_scan - t.idx_scan as difference
from pg_catalog.pg_stat_user_tables t
where t.seq_scan > t.idx_scan
and t.schemaname not like 'pg_temp%'
order by difference desc;
