select
 cast(d.date_id as date) as calendar_date,
 cast(d.date_id as timestamp without time zone) as calendar_dts,
 date_part('year', d.date_id) as year_of_date,
 date_part('month', d.date_id) as month_of_date,
 date_part('day', d.date_id) as day_of_date,
 date_part('quarter', d.date_id) as quarter_of_date,
 date_part('isodow', d.date_id) as iso_dow,
 case when date_part('isodow', d.date_id) in (6,7) then 1 else 0 end as is_weekend_day,
 null as is_business_day
from generate_series('2010-01-01', '2060-01-01', interval '1 day') as d(date_id)
order by calendar_date
limit 100;
