/*
These are standard Postgres functions that can be used to check whether a user has the privilege to perform an action on something.
These are not perfect since db permissions can become complicated with nesting and recursion.
*/

-- Roles:
select *
from pg_catalog.pg_roles pr
order by pr.rolname;


-- Database permissions:
select 
 pr.rolname as role_name,
 pd.datname as database_name,
 has_database_privilege(pr.rolname,pd.datname,'connect') 	as can_connect_to_database,
 has_database_privilege(pr.rolname,pd.datname,'temp') 		as can_create_temp_tables,
 has_database_privilege(pr.rolname,pd.datname,'create') 	as can_create_schemas
from pg_catalog.pg_roles pr 
cross join pg_catalog.pg_database pd
where pr.rolname not in ('pg_signal_backend','rds_replication','rds_superuser','rdsadmin','rdsrepladmin')  -- system roles and AWS RDS system roles
order by role_name, database_name;



-- Schema permissions (must be in the database in which you want to check):
select 
 pr.rolname as role_name,
 pn.nspname as schema_name,
 has_schema_privilege(pr.rolname,pn.nspname,'usage')	as can_access_objects,
 has_schema_privilege(pr.rolname,pn.nspname,'create')	as can_create_objects_in_schema
from pg_catalog.pg_roles pr 
join pg_catalog.pg_namespace pn
 on pn.nspname not in ('pg_catalog','postgres','public','information_schema')
 and pn.nspname !~ '(pg_temp|pg_toast)'
where pr.rolname not in ('pg_signal_backend','rds_replication','rds_superuser','rdsadmin','rdsrepladmin')  -- system roles and AWS RDS system roles
order by pr.rolname, pn.nspname;



-- Tables and Views permissions:
select 
 pr.rolname as role_name,
 pn.nspname as schema_name,
 pc.relname as object_name,
 has_table_privilege(pr.rolname, pn.nspname || '.' || pc.relname, 'select')	as can_select,
 has_table_privilege(pr.rolname, pn.nspname || '.' || pc.relname, 'insert')	as can_insert_into,
 has_table_privilege(pr.rolname, pn.nspname || '.' || pc.relname, 'update')	as can_update,
 has_table_privilege(pr.rolname, pn.nspname || '.' || pc.relname, 'delete')	as can_delete_from
from pg_catalog.pg_roles pr 
join pg_catalog.pg_namespace pn
 on pn.nspname not in ('pg_catalog','postgres','public','information_schema')
 and pn.nspname !~ '(pg_temp|pg_toast)'
join pg_catalog.pg_class pc
	on pn.oid = pc.relnamespace
	and pc.relkind in ('r','v')	-- tables and views
	and not exists (
		-- remove inherited tables such as partitions
		select 'x'
		from pg_catalog.pg_inherits pi
		where pc.oid = pi.inhrelid
	)
where pr.rolname not in ('pg_signal_backend','rds_replication','rds_superuser','rdsadmin','rdsrepladmin')  -- system roles and AWS RDS system roles
order by role_name, schema_name, object_name;
