-- Prepared Statements
-- https://www.postgresql.org/docs/9.6/static/sql-prepare.html

-- Why use? Optimize performance by avoiding prase analysis work on future calls.
-- They only last for duration of database session.

-- i'm using a select here.  you can also use these for any select, insert, update, delete, or values statement
prepare my_sweet_plan(text) as
	select d.city_name
    from (
        values ('IL','Chicago'),('IL','Rockford'),('WI','Madison')
    ) d(state_name, city_name)
    where d.state_name = $1;
    
execute my_sweet_plan('IL');
execute my_sweet_plan('WI');

deallocate my_sweet_plan;  -- plan will be deallocated when session ends if not done explicitly
