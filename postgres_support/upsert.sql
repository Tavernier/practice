create table if not exists public.upsert_example (
    upsert_example_id int primary key,
    upsert_value varchar(100) not null,
    insert_dts timestamptz(2) default current_timestamp,
    update_dts timestamptz(2) default current_timestamp
    );
    
    
-- regular insert:
insert into public.upsert_example(upsert_example_id, upsert_value)
values (1, 'I am the first!');
    
select *
from public.upsert_example;


-- now let's try an upsert to update that row and insert a new one:

insert into public.upsert_example(upsert_example_id, upsert_value)
values 
	(1, 'I am the updated first row!'),
    (2, 'I am the second row!')
on conflict(upsert_example_id) do update  -- we refer to the conflict data using the alias excluded
	set upsert_value = excluded.upsert_value, update_dts = now();
    
    
select *
from public.upsert_example;

