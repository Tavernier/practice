select 
 n.nspname as schema_name,
 o.relname as table_name,
 i.relname as index_name,
 pi.indisvalid
from pg_catalog.pg_index pi
join pg_catalog.pg_class i
	on pi.indexrelid = i.oid
join pg_catalog.pg_class o
	on pi.indrelid = o.oid
join pg_catalog.pg_namespace n
	on o.relnamespace = n.oid
where pi.indisprimary = false	-- exclude primary
and pi.indisclustered = false	-- exclude clustered
and n.nspname not in ('pg_catalog','postgres','information_schema')
order by schema_name, table_name, index_name;
