-- Functions: plPGSQL
-- https://www.postgresql.org/docs/9.6/static/plpgsql.html

select * from pg_catalog.pg_language;				-- languages available
select * from pg_catalog.pg_extension;				-- installed extensions
select * from pg_catalog.pg_available_extensions order by name;	-- extensions available for install

-- scalar:
drop function if exists public.fe_plpgsql_scalar(int);  -- must pass signature since functions can be overloaded
create or replace function public.fe_plpgsql_scalar(x int) returns integer as $$
begin
	return x * 2;
end;
$$
immutable 						-- always returns same result with given input, cannot modify or lookup from db
returns null on null input		-- immediately quit if any arguments are null otherwise function author must handle nulls
language plpgsql;

select 
 public.fe_plpgsql_scalar(null) as unknown,
 public.fe_plpgsql_scalar(1) as two, 
 public.fe_plpgsql_scalar(2) as four;


-- result set:
drop function if exists public.fe_plpgsql_set(text);
create or replace function public.fe_plpgsql_set(p_state_name text) 
returns table (
	city_name text
)
as $$
	declare	-- must put all used variables here
    	my_variable integer := 30;  -- := is the assignment operator
        my_other_variable int := 0;
	begin
    	-- populate variables
        if my_variable > 20 then
        	my_other_variable := 1;
        elsif my_variable > 30  then
        	my_other_variable := 2;
        else
        	my_other_variable := 3;
        end if;
        
        
    
    	return query 	select d.city_name
        				from (values ('IL','Chicago'),('IL','Rockford'),('WI','Madison')) d(state_name, city_name)
                        where d.state_name = p_state_name;
    end;
$$
stable 							-- 
returns null on null input		-- immediately quit if any arguments are null otherwise function author must handle nulls
language plpgsql;


select * from public.fe_plpgsql_set('WI');
select * from public.fe_plpgsql_set('IL');

/*
need to review:
	- capturing errors
    - looping
    - best practices
*/


