-- ------------------------------------------------------------------------------------------------
-- Dates and Times
-- Reference: http://blog.untrod.com/2016/08/actually-understanding-timezones-in-postgresql.html
-- Best Practices seem to indicate always store UTC then display to user in their timezone
-- "Don't use timestamps without time zones in PostgreSQL"
-- "Use only TIMESTAMP WITH TIME ZONE fields."
-- ------------------------------------------------------------------------------------------------
select
	current_setting('TIMEZONE'),
	current_timestamp,	-- time zone given
    now(),				-- gives UTC since server is set for that
    current_timestamp at time zone 'America/Chicago'  -- converts it to a timestamp without time zone
    ;			
    
    
select *
from pg_catalog.pg_timezone_names t
where t.name = 'America/Chicago';

set time zone 'America/Chicago';
select current_timestamp, current_setting('TIMEZONE');  -- returns Chicago time since time zone is set to that for this session
set time zone default;				-- aws rds is on utc so return to that
select current_timestamp;


create temp table if not exists test_time_zone(
    blah timestamptz
);

insert into test_time_zone(blah) values (current_timestamp);

select * from test_time_zone;  		-- returned UTC
set time zone 'America/Chicago';
select * from test_time_zone;  		-- returned CST
set time zone default;

-- Postgres will correctly adjust the time given the various time zone rules that exist:
select
   '2017-01-03 10:00:00+00'::timestamptz at time zone 'America/Chicago'	-- returns CST
 , '2016-06-03 10:00:00+00'::timestamptz at time zone 'America/Chicago'	-- returns CDT
 , '2016-11-01 10:00:00+00'::timestamptz at time zone 'America/Chicago'	-- returns CST
 , '2002-11-01 10:00:00+00'::timestamptz at time zone 'America/Chicago'	-- returns CDT


-- Let's look at some datetime match (https://www.postgresql.org/docs/9.6/static/functions-datetime.html)
select
	  cast('2016-05-20 10:00:00' as timestamptz) - cast('2016-05-19 10:30:00' as timestamptz) as diff  -- returns an interval data type
    , date_part('minute', interval '23:30:00') as minutes -- gets just the minutes (does not convert the interval to total minutes
    , EXTRACT(EPOCH FROM (cast('2016-05-20 10:00:00' as timestamptz) - cast('2016-05-19 10:30:00' as timestamptz))) as seconds_between_timestamps;
    
    
-- ------------------------------------------------------------------------------------------------
-- JSON
-- Two data types exist: JSON and JSONB
-- Best Practices seem to indicate to use JSONB.
-- ------------------------------------------------------------------------------------------------

-- json stores an exact copy of the input text including key order and duplicate keys.
-- jsonb stores a binary form (slighter slower to input, but much faster to process).  supports indexing.
--    keeps last value if duplicate key is passed.  does not maintain key order.
select
	  cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as json) as json_example
    , cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as jsonb) as jsonb_example;
    
    
-- finding data in jsonb (no equivalent operators exist for json data type):
select 
	  d.jsonb_example
    , d.jsonb_example @> cast('{"state":"IL"}' as jsonb) as jsonb_example_contains_illinois
    , d.jsonb_example @> cast('{"state":"IL", "cities":[{"city":"Chicago"}]}' as jsonb) as jsonb_example_contains_chicago
from (
    values(cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as jsonb))
) d(jsonb_example);


-- json to a result set.  the goal here is to get a result set with state_name, city_name
-- reference: https://www.postgresql.org/docs/9.6/static/functions-json.html
-- the first result set will return just the state name:
select
    d.jsonb_example->'state' as state_name_jsonb,
    d.jsonb_example->>'state' as state_name_text
from (
    values 
     (cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as jsonb)),
     (cast('{"state":"WI", "cities":[{"city":"Madison"}]}' as jsonb)),
     (cast('{"state":"NY", "cities":[{"city":"Buffalo"}]}' as jsonb))
) d(jsonb_example);

-- we then expand this result set to include the city names
select
    d.jsonb_example->>'state' as state_name_text,
    jsonb_array_elements(d.jsonb_example->'cities')->>'city' as city_name_text
from (
    values 
     (cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as jsonb)),
     (cast('{"state":"WI", "cities":[{"city":"Madison"}]}' as jsonb)),
     (cast('{"state":"NY", "cities":[{"city":"Buffalo"}]}' as jsonb))
) d(jsonb_example);

-- here's another way to also get a result set.
-- the "," syntax in postgres is seen usually online, but I prefer the cross join:
select 
 r.state,
 c.city
from (
    values 
     (cast('{"state":"IL", "cities":[{"city":"Chicago"},{"city":"Rockford"}]}' as jsonb)),
     (cast('{"state":"WI", "cities":[{"city":"Madison"}]}' as jsonb)),
     (cast('{"state":"NY", "cities":[{"city":"Buffalo"}]}' as jsonb))
) d(jsonb_example)
cross join jsonb_to_record(d.jsonb_example) as r(state varchar(2), cities jsonb)
cross join jsonb_to_recordset(r.cities) c(city varchar(20));

-- now, let's create json out of a result set:
select t.*
from (
    values ('IL'),('WI'),('NI')
) d(state_name)
cross join row_to_json(d) t;



-- ------------------------------------------------------------------------------------------------
-- XML
-- Use XMLPARSE function to create whole documents or pieces of XML (e.g. content).
-- ------------------------------------------------------------------------------------------------

select d.xml_example
from (
	values 
     (xmlparse(document '<?xml version="1.0"?><state><name>IL</name><cities><city><name>Chicago</name></city><city><name>Rockford</name></city></cities></state>')),
    (xmlparse(document '<?xml version="1.0"?><state><name>WI</name><cities><city><name>Madison</name></city></cities></state>'))
) d(xml_example);

-- functions exist to get data out, but i don't feel like going over them at the moment.

