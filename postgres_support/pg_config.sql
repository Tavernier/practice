-- see compile-time config paramter values
-- available as of version 9.6.1
select * 
from pg_catalog.pg_config();


-- watch out for an age approaching two billion.
-- if that happens, the db goes to read-only mode.
select datname, age(datfrozenxid) 
from pg_catalog.pg_database 
order by age(datfrozenxid) 
desc limit 20;
