-- Last vacuum and analyze dates.
-- The query planner relies on good stats to create performant query plans.
-- If those are missing or old, we should analyze tables.
-- We may want to proactively analyze tables too and include more data when gathering stats.
select 
 t.schemaname as schema_name,
 t.relname as object_name,
 greatest(t.last_vacuum, t.last_autovacuum) as last_vacuum,
 greatest(t.last_analyze, t.last_autoanalyze) as last_analyze
from pg_catalog.pg_stat_user_tables t
where t.schemaname not like 'pg_temp%'
order by schema_name, object_name
limit 100;

-- Table usage:
select 
 t.schemaname as schema_name,
 t.relname as object_name,
 t.seq_scan,
 t.seq_tup_read,
 t.idx_scan,
 t.idx_tup_fetch,
 t.n_tup_ins,
 t.n_tup_upd,
 t.n_tup_del,
 t.n_tup_hot_upd,
 t.n_live_tup,
 t.n_dead_tup,
 t.n_mod_since_analyze
from pg_catalog.pg_stat_user_tables t
where t.schemaname not like 'pg_temp%'
order by schema_name, object_name
limit 100;

-- Index usage:
select 
 t.schemaname as schema_name,
 t.relname as object_name,
 t.indexrelname as index_name,
 t.idx_scan as index_scans,
 t.idx_tup_read as rows_returned_by_scans,
 t.idx_tup_fetch as live_rows_returned_by_scans
from pg_catalog.pg_stat_user_indexes t
limit 100;
