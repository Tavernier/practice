-- let's create a table with one million rows:
drop table if exists public.explain_test;
create table public.explain_test (
    id int primary key
);

insert into public.explain_test(id)
select t.id
from generate_series(1, 1000000, 1) t(id);

analyze public.explain_test;

select count(*) as ct from public.explain_test;

-- explain = estimated execution plan
explain
select t.id
from public.explain_test t
where t.id between 32544 and 46562;

-- explain analyze = actual execution plan
explain analyze
select t.id
from public.explain_test t
where t.id between 32544 and 46562;


-- we can get "better" stats on a column via alter table:
alter table public.explain_test
alter id set statistics 10000; -- range 0 to 10000

