------------------------------------------------------------
-- first, create our sample sets of data:
------------------------------------------------------------
drop table if exists temp_set_one;
create temp table temp_set_one as
 select d.num
 from (
  values(1),(2),(3)
 ) d(num);

drop table if exists temp_set_two;
create temp table temp_set_two as
 select d.num
 from (
  values(2),(3),(4)
 ) d(num);


------------------------------------------------------------
-- then, practice our set based operations:
------------------------------------------------------------

-- union (member of one or two or both):
select num
from temp_set_one

union

select num
from temp_set_two;


-- intersection (member of both one and two):
select num
from temp_set_one

intersect

select num
from temp_set_two;


-- except aka set difference (all members of one that are not members of two):
select num
from temp_set_one

except

select num
from temp_set_two;


-- symmetric difference (member is in one of the sets but not both).
-- it is "the set difference (aka except) of the union and the intersection:"
(
 -- first, get all values:
 select num
 from temp_set_one

 union

 select num
 from temp_set_two
)

except

(
 -- then subtract out shared values:
 select num
 from temp_set_one

 intersect

 select num
 from temp_set_two
);



-- cartesian product (all possible ordered pairs):
select t1.num, t2.num
from temp_set_one t1
cross join temp_set_two t2;


-- powerset (all possible subsets of a set)
-- no built-in functionality for this.