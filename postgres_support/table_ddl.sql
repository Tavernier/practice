create table if not exists public.countries (
    country_id serial,
    country_name varchar(100) not null,
    constraint pk_public_countries primary key(country_id)
    );
    
create table if not exists public.states (
    state_id serial,
    country_id int not null references public.countries(country_id),
    state_name varchar(100) not null,
    constraint pk_public_states primary key (state_id)
    );
    
    
-- You can specify your own ID value even though it is defined as a serial:
insert into public.countries(country_id, country_name)
values
 (1, 'United States'),
 (20, 'Canada');
 
-- However, you could conflict with the sequence value since that was not incremented.
-- The first insert attempt here will fail since ID 1 is next in the sequence, but it already exists in the table.
-- The next attempt will succeed because the sequence value advanced even though the first insert failed.
insert into public.countries(country_name)
values ('India');
 
select * from public.countries;

-- This will fail because there is no country with an ID of 3000:
insert into public.states(country_id, state_name)
values
 (1, 'Indiana'),
 (3000, 'Calm');
 
-- This will succeed of course:
insert into public.states(country_id, state_name)
values (1, 'Indiana');

select *
from public.states;

select c.country_name, s.state_name
from public.countries c
join public.states s
	on c.country_id = s.country_id;