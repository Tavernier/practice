-- quick check:
select psa.pid as blocked_pid, unnest(pg_blocking_pids(psa.pid)) as blocking_pid
from pg_catalog.pg_stat_activity psa;

-- WARNING: confirm with whoever developed the code running the queries that is okay
-- to kill them.

------------------------------------------------------------------------------------------
-- Step 1: What's being blocked?
------------------------------------------------------------------------------------------
select 
 -- process that wants lock:
 acb.pid as blocked_pid,
 acb.usename, 
 acb.client_addr,
 acb.query_start,
 acb.xact_start,
 acb.state,
 acb.query,
 plb.locktype as blocked_locktype,
 plb.granted,
 -- lock information:
 pl.locktype as granted_locktype,
 plb.granted,
 pl.mode,
 o.relname as object_name,
 o.reltype,
 -- process with lock:
 ac.pid as blocking_pid,
 ac.usename, 
 ac.client_addr,
 ac.query_start,
 ac.xact_start,
 ac.state,
 ac.query
from pg_catalog.pg_locks pl
join pg_catalog.pg_stat_activity ac
	on pl.pid = ac.pid
join pg_catalog.pg_class o
	on pl.relation = o.oid
join pg_catalog.pg_locks plb
	on pl.database = plb.database
    and pl.relation = plb.relation
    and plb.granted is false
join pg_catalog.pg_stat_activity acb
	on plb.pid = acb.pid
where pl.granted is true
order by acb.pid, ac.pid;


------------------------------------------------------------------------------------------
-- Step 2: What is the blocking process?
------------------------------------------------------------------------------------------
-- Take the "blocking_pid" value from the query above and see what it's doing:
select *
from pg_catalog.pg_stat_activity
where pid = blocking_pid;


------------------------------------------------------------------------------------------
-- Step 3: Cancel Blocking Process if Appropriate
------------------------------------------------------------------------------------------
-- first try pg_cancel_backend:
select pg_cancel_backend(blocking_pid);

-- then terminate if that does not work:
select pg_terminate_backend(blocking_pid);

-- never kill a postgres process from the command line!


------------------------------------------------------------------------------------------
-- Step 4: Verify Process is No Longer Waiting
------------------------------------------------------------------------------------------
select * 
from pg_catalog.pg_stat_activity
where pid = blocked_pid
order by query_start;
 