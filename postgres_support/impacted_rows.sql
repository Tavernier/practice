drop table if exists returning_example;
create temp table if not exists returning_example (
    id int primary key,
    row_insert_dts timestamptz default current_timestamp
);

-- we can even return data for default values:
insert into returning_example(id)
select t.id
from generate_series(1, 10, 1) t(id)
returning id, row_insert_dts;

-- get deleted IDs:
delete from returning_example
where id between 4 and 6
returning id;

-- values 4, 5, and 6 will not return since they were deleted above:
update returning_example
set row_insert_dts = current_timestamp
where id between 2 and 8
returning id;
