-- Create from a result set:
create temp table my_temp_table as 
 select 1 as id, 'jon' as first_name;

select *
from my_temp_table;



-- Create then populate:
create temp table my_second_temp_table (
 id int,
 first_name text
);

insert into my_second_temp_table(id, first_name)
values (1, 'jon');

select *
from my_second_temp_table;