select 
 t.datname, t.pid, t.usename, 
 t.state, t.query,
 case
  when t.state = 'active' then 'running'
  when t.state is not null then 'last query to run'
 end as query_status,
 t.backend_start, current_timestamp - t.backend_start as backend_duration, 
 t.query_start, current_timestamp - t.query_start as query_duration
from pg_catalog.pg_stat_activity t
where t.usename not in ('rdsadmin')
order by t.usename, query_duration desc;

select 
 ps.name, ps.setting, ps.boot_val, 
 case when ps.setting = ps.boot_val then false else true end as current_different_from_boot, 
 ps.short_desc
from pg_catalog.pg_settings ps
where ps.name in (
 'track_activities',	-- get stats on currently executing queries
 'track_counts',		-- collect stats on db activity.  the autovacuum daemon needs the info
 'track_functions',		-- track function call stats
 'track_io_timing'		-- timing of database calls
)
order by ps.name;

select *
from pg_stat_statements pss
where pss.usename like '%whatever%'
limit 10;
