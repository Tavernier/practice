-- Tables:
select 
	pn.nspname as schema_name,	-- schema name
	pc.relname as object_name,	-- object name
	pc.oid,
	pc.relowner,
	pc.relpages,	-- size on disk in 8K pages (estimate)
	pc.relpages * 8 as relpages_KB,
	round(pc.relpages * 8.0 / 1024, 1) as relpages_MB,
    round(pc.relpages * 8.0 / 1024 / 1024, 1) as relpages_GB,
	pc.reltuples,	-- row count (estimate)
	pc.relkind,	-- type of object
	case pc.relkind
	 when 'r' then 'table'
	 when 'i' then 'index'
	 when 'S' then 'sequence'
	 when 'v' then 'view'
	 when 'c' then 'composite type'
	 when 't' then 'TOAST value'
	 when 'o' then 'internal append-optimized segment files'
	 when 'u' then 'uncataloged temp heap table'
	end as relkind_dsc
from pg_catalog.pg_namespace pn	-- schema
join pg_catalog.pg_class pc		-- tables, views, indexes, etc..
	on pn.oid = pc.relnamespace
where pn.nspname not in ('pg_catalog','postgres','information_schema')
and pc.relkind = 'r'			-- restrict to tables
and not exists (
	-- remove inherited tables such as partitions
	select 'x'
	from pg_catalog.pg_inherits pi
	where pc.oid = pi.inhrelid
)
order by pn.nspname, pc.relname;



-- Tables and Columns:
select 
	pn.nspname as schema_name,			-- schema name
	pc.relname as object_name,			-- object name
	pa.attname as column_name,			-- attribute name
	pa.attnum as column_order,
	pa.attisdropped,
	pt.typname,			-- data type
	pt.typlen			-- bytes
from pg_catalog.pg_namespace pn	-- schema
join pg_catalog.pg_class pc		-- tables, views, indexes, etc..
	on pn.oid = pc.relnamespace
join pg_catalog.pg_attribute pa	-- attributes
	on pc.oid = pa.attrelid
join pg_catalog.pg_type pt
	on pa.atttypid = pt.oid
	and pa.attnum > 0		-- eliminate system attributes
where pn.nspname not in ('pg_catalog','postgres','information_schema')
and pn.nspname not like 'pg_temp%'
and pc.relkind = 'r'			-- restrict to tables
and not exists (
	-- remove inherited tables such as partitions
	select 'x'
	from pg_catalog.pg_inherits pi
	where pc.oid = pi.inhrelid
)
order by pn.nspname, pc.relname, pa.attnum;
