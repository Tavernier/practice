drop table if exists dupe_delete_test;
create temp table if not exists dupe_delete_test (
     id int,
     city varchar(100)
    );
    
-- enter a dupe row for chicago:
insert into dupe_delete_test(id, city)
values (1, 'Chicago'),(2, 'Chicago'),(3, 'Madison');


select *
from dupe_delete_test;


with dupes as (
	select id, city, row_number() over(partition by city order by id) as rn
    from dupe_delete_test
)

delete from dupe_delete_test
where id in (
    select id
    from dupes where rn > 1
);

select *
from dupe_delete_test;