
-- A function without parameters:
create or replace function public.get_cities() 
returns table (
    city_name varchar(20)
)
as $$
	select *
    from (values('Chicago'),('Madison')) d(city);
    $$
    language sql;
    
    
select *
from public.get_cities();



-- Same function overloaded with a parameters:
create or replace function public.get_cities(
	city_name varchar(20)
) 
returns table (
    city_name varchar(20)
)
as $$
	select *
    from (values('Chicago'),('Madison')) d(city)
    where d.city = get_cities.city_name;
    $$
    language sql;
    
    
select * from public.get_cities('Chicago');
select * from public.get_cities('Madison');

/*
AWS RDS supports these languages:
PL/pgSQL
PL/Tcl
PL/Perl
PL/V8 (Version 9.3.5 and later)
*/