-- These queries are also helpful for troubleshooting slow queries.
-- You can get an idea of how much data is being accesses if a query is scanning a table
-- or index.
-- If you get a schema permission error when running these queries, you will either need
-- to get access to that schema or filter it out in the where clause.

-- Tables:
select 
 t.schemaname as schema_name,
 t.tablename as table_name,
 -- "Disk space used by the specified table, excluding indexes (but including TOAST, free space map, and visibility map)"
 pg_table_size(t.schemaname || '.' || t.tablename) as table_size_bytes,
 round(pg_table_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0 , 2) as table_size_MB,
 round(pg_table_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as table_size_GB,
 -- "Total disk space used by indexes attached to the specified table"
 pg_indexes_size(t.schemaname || '.' || t.tablename) as indexes_size_bytes,
 round(pg_indexes_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0, 2) as indexes_size_MB,
 round(pg_indexes_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as indexes_size_GB,
 -- "Total disk space used by the specified table, including all indexes and TOAST data"
 pg_total_relation_size(t.schemaname || '.' || t.tablename) as total_size_bytes,
 round(pg_total_relation_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0, 2) as total_size_MB,
 round(pg_total_relation_size(t.schemaname || '.' || t.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as total_size_GB
from pg_catalog.pg_tables t
where t.schemaname not in ('pg_catalog','postgres','information_schema')
and t.schemaname not like 'pg_temp%'
order by schema_name, table_name;


-- Indexes:
select 
 i.schemaname as schema_name,
 i.tablename as table_name,
 i.indexname as index_name,
 -- "Disk space used by the specified fork ('main', 'fsm', 'vm', or 'init') of the specified table or index"
 pg_relation_size(i.schemaname || '.' || i.indexname) as index_size_bytes,
 round(pg_relation_size(i.schemaname || '.' || i.indexname) / 1024.0 / 1024.0, 2) as index_size_MB,
 round(pg_relation_size(i.schemaname || '.' || i.indexname) / 1024.0 / 1024.0 / 1024.0, 2) as index_size_GB,
 -- "Disk space used by the specified table, excluding indexes (but including TOAST, free space map, and visibility map)"
 pg_table_size(i.schemaname || '.' || i.tablename) as table_size_bytes,
 round(pg_table_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0, 2) as table_size_MB,
 round(pg_table_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as table_size_GB,
 -- "Total disk space used by indexes attached to the specified table"
 pg_indexes_size(i.schemaname || '.' || i.tablename) as indexes_size_bytes,
 round(pg_indexes_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0, 2) as indexes_size_MB,
 round(pg_indexes_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as indexes_size_GB,
 -- "Total disk space used by the specified table, including all indexes and TOAST data"
 pg_total_relation_size(i.schemaname || '.' || i.tablename) as total_size_bytes,
 round(pg_total_relation_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0, 2) as total_size_MB,
 round(pg_total_relation_size(i.schemaname || '.' || i.tablename) / 1024.0 / 1024.0 / 1024.0, 2) as total_size_GB,
 -- Index Activity
 -- Warning: this will only be accurate when statistics are being collected on index use and the index has existed long enough for good stats to exist.
 s.idx_scan,
 s.idx_tup_read,
 s.idx_tup_fetch,
 -- If the index isn't actually being used to return data, mark it as potentially useless.
 -- The logic isn't perfect as you may not want to drop a primary key index or a unique index for example.
 case 
  -- add logic to remove primary keys and unique indexes
  when coalesce(s.idx_scan,0) + coalesce(s.idx_tup_read, 0) + coalesce(s.idx_tup_fetch,0) = 0 then true
 end as potentially_useless_index
from pg_catalog.pg_indexes i
left join pg_catalog.pg_stat_all_indexes s      -- index stats
        on i.schemaname = s.schemaname
        and i.tablename = s.relname
        and i.indexname = s.indexrelname
where i.schemaname not in ('pg_catalog','postgres','information_schema')
and i.schemaname not like 'pg_temp%'
order by index_size_GB desc;
