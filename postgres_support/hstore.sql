-- HSTORE
-- great tutorial here: http://www.postgresqltutorial.com/postgresql-hstore/

create extension if not exists hstore;

drop table if exists public.hstore_example;
create table if not exists public.hstore_example (
    hstore_example_id int primary key,
    keyvalues hstore	
    );
    

insert into public.hstore_example(hstore_example_id, keyvalues)
values
 (1, '"name"=>"Jon","city"=>"Chicago"'),
 (2, '"name"=>"Garrett","city"=>"Keller"');
 
 
select  
 t.hstore_example_id, 
 t.keyvalues->'name' as name,
 t.keyvalues->'boguskey' as boguskey
from public.hstore_example t;