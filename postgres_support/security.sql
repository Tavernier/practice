-- Roles: either a single user or group of users.

create role analysts with nologin;  -- we will use this as a group so no one can actually login as analysts

create role webapp_ro with login password 'iamstarving!';		-- we will use this as a user for read-only purposes

create role david with login in role analysts;  -- add a user for david who is an analyst

grant role david to jtadmin;  -- this is the aws rds admin, but it's not a superuser so must grant the role explicitly

select t.*
from pg_catalog.pg_roles t
where t.rolname in ('analysts', 'webapp_ro', 'david', 'jtadmin')
order by t.rolname;


create schema analysts;
grant usage on schema analysts to analysts;

create view analysts.cities as
	select c.city_id, c.city_name
    from public.cities c
    where c.city_name not in ('Buffalo');
    
    
-- analysts and david will get access to the cities view with this:
grant select on all tables in schema analysts to analysts;

-- they will not have access to this view as the grant all only applies to existing objects.
-- so after a release, we should refersh security or let the analysts own the schema.
create view analysts.cities_east as
	select c.city_id, c.city_name
    from public.cities c
    where c.city_name in ('Buffalo');
	
	
revoke david from jtadmin;