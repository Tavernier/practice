-- Storing Passwors

-- Avoid:
-- 	storing plain text passwords
-- 	storing passwords with weak hash functions
-- 	not using a salt

-- input should be: random string (salt) + password --> hash password --> store hash password

create extension if not exists pgcrypto;

-- creating the salt
select gen_salt('bf') as salt_string;  -- bf = blowfish algorithm.  generates a random salt on each call

select crypt('my_password', 'my_salt_string');

drop table if exists public.storing_safe_passwords;
create table if not exists public.storing_safe_passwords (
    user_id int primary key,
    password_hash text not null
    );

insert into public.storing_safe_passwords(user_id, password_hash)
values (1, crypt('my_password', gen_salt('bf',8)));


select 
 t.user_id,
 crypt('my_wordpass', t.password_hash) = t.password_hash as incorrect_password,
 crypt('my_password', t.password_hash) = t.password_hash as correct_password
from public.storing_safe_passwords t;

-- interesting:
--  we need not store the salt as it's already in the hash and pgcrypto knows that
--  we call crypte again with the user-entered-password and the existing hash to see whether it matches the stored hash
