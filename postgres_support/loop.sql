-- Looping

-- Let's practice loooping.
-- We'll start with a simple loop then implement a loop delete.


-- --------------------------------------------------------------------------------------------------------------------
-- simple loop:
-- --------------------------------------------------------------------------------------------------------------------
drop function if exists public.fe_loop(int);  -- must pass signature since functions can be overloaded
create or replace function public.fe_loop(p_max_loops int) returns integer as $$
declare
	final_answer int := 0;
begin
	for i in 1..p_max_loops loop
    	final_answer := final_answer + i;
    end loop;
    return final_answer;
end;
$$
returns null on null input		-- immediately quit if any arguments are null otherwise function author must handle nulls
language plpgsql;

select
	public.fe_loop(1) as one,
    public.fe_loop(5) as fifteen;


-- --------------------------------------------------------------------------------------------------------------------
-- delete loop
-- --------------------------------------------------------------------------------------------------------------------

-- first, create a table with 100 rows:
drop table if exists public.loop_delete_example;
create table if not exists public.loop_delete_example (
	id int primary key    
);

insert into public.loop_delete_example(id)
select t.id
from generate_series(1, 100) t(id);

analyze public.loop_delete_example;




drop function if exists public.fe_loop_delete();
-- we could think of adding these params: rows_per_delete_batch, delay_between_deletes, 
create or replace function public.fe_loop_delete() returns integer as $$
declare
	continue_deleting_rows boolean := true;
    max_loops int := 100; -- quit at this level no matter what to avoid infinite loop
    loops_executed int := 0;
    deleted_rows int := 0;
    total_deleted_rows int := 0;
begin
	-- we'll hold the batch of IDs to delete here:
	drop table if exists temp_ids_to_delete;
    create temp table temp_ids_to_delete(id int);      
        
	-- now, let's loop delete until no more rows are deleted
    while continue_deleting_rows loop
    	loops_executed := loops_executed + 1;
        
        if loops_executed >= max_loops then
        	continue_deleting_rows := false;
        end if;
        
        truncate table temp_ids_to_delete;
        insert into temp_ids_to_delete(id)
        select t.id
        from public.loop_delete_example t
        where t.id between 25 and 74
        limit 10;
        
        delete from public.loop_delete_example
        where exists (
            select 1
            from temp_ids_to_delete t
            where loop_delete_example.id = t.id
        );
        
        GET DIAGNOSTICS deleted_rows = ROW_COUNT;
        
        total_deleted_rows := total_deleted_rows + deleted_rows;
        
        if deleted_rows = 0 then
        	continue_deleting_rows := false;
            exit;
        else
        	perform pg_sleep(.5);
        end if;
        
    end loop;

    return total_deleted_rows;
end;
$$
returns null on null input
language plpgsql;


select public.fe_loop_delete();

select count(*) from public.loop_delete_example;

