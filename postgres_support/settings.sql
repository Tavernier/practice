select 
 ps.name, ps.setting, ps.boot_val, 
 case when ps.setting = ps.boot_val then false else true end as current_different_from_boot, 
 ps.short_desc
from pg_catalog.pg_settings ps
where ps.name in (
 'track_activities',
 'track_counts',
 'track_functions',
 'track_io_timing'
)
order by ps.name;
