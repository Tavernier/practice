-- Index Maintenance

-- Postgres docs recommend rebuilding indexes in some situations.
--  rebuild b-tree indexes when most, but not all keys in a range are deleted
-- 	old indexes or indexes that seem larger than they should be

-- Concerns
--  reindex requires an exclusive table lock.
--  workaround is to build a new index concurrently then perform a drop/rename

drop table if exists public.index_maintenance_exmaple;

create table if not exists public.index_maintenance_exmaple (
    	id1 int primary key,
    	id2 int
    );
    
insert into public.index_maintenance_exmaple(id1, id2)
select t.id, t.id * 2
from generate_series(1,100000) t(id);

analyze public.index_maintenance_exmaple;


-- let's pretend this is an important index:
drop index if exists ix_index_maintenance_exmaple_id2;
create index ix_index_maintenance_exmaple_id2 on public.index_maintenance_exmaple(id2) with(fillfactor=80);

-- assume index is small, let's just reindex:
reindex index ix_index_maintenance_exmaple_id2;

-- let's assume this is a large index and we can't afford an exclusive lock on the table to rebuild it:
drop index if exists ix_index_maintenance_exmaple_id2_new;
create index concurrently ix_index_maintenance_exmaple_id2_new		-- concurrently = allow writes on table while index is built
	on public.index_maintenance_exmaple(id2) with(fillfactor=80);
begin;
	drop index if exists ix_index_maintenance_exmaple_id2;
    alter index ix_index_maintenance_exmaple_id2_new rename to ix_index_maintenance_exmaple_id2;
commit;

