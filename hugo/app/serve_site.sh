#! /usr/bin/env bash

SITE_TO_START=$1

pushd "/app/${SITE_TO_START}"

hugo serve \
--buildDrafts \
--bind 0.0.0.0

popd
