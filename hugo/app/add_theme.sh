#! /usr/bin/env bash

set -euxo pipefail

download_theme(){
    wget \
    https://github.com/budparr/gohugo-theme-ananke/archive/master.zip \
    -O /tmp/ananke.zip
}

install_theme(){
    rm -rf /app/quickstart/themes/ \
    && mkdir -p /app/quickstart/themes/ \
    && unzip \
        /tmp/ananke.zip \
        -o \
        -d /app/quickstart/themes/ \
    && mv \
        /app/quickstart/themes/gohugo-theme-ananke-master/ \
        /app/quickstart/themes/ananke \
    && echo 'theme = "ananke"' >> /app/quickstart/config.toml 
}

main(){
    download_theme
    install_theme
}

main
