# Hugo Static Site Generator

## Helper

1. Run `./launch_hugo.sh` to shell into a running container in which hugo and git cli are installed.
1. The local `./app/` path will be mounted to `/app`, which will be the working directory.
1. Start with `hugo server -D --bind=0.0.0.0`  and visit http://localhost:1313

## Quickstart

Hugo has a [quickstart](https://gohugo.io/getting-started/quick-start/) available.  Let's go through that.