#! /usr/bin/env bash

set -euxo pipefail

build_hugo_helper(){
    docker build . --tag hugo-helper
}

shell_into_container(){
    docker run \
        -it \
        --rm \
        -p 127.0.0.1:1313:1313 \
        -v "$(pwd)/app":/app \
        hugo-helper \
        sh
}

main(){
    build_hugo_helper
    shell_into_container
}

main
