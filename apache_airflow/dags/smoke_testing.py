"""
Airflow Smoke Testing

This DAG can be run to verify basic Airflow functionality.
"""

from datetime import datetime, timedelta
import os
import textwrap

from airflow import DAG, configuration
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable


dag = DAG(
    'smoke_testing',
    schedule_interval="@once",
    max_active_runs=1,
    catchup=False,
    default_args={
        'owner': 'airflow',
        'depends_on_past': False,
        'start_date': datetime(2018, 1, 1),
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 3,
        'retry_delay': timedelta(seconds=30),
        'execution_timeout': timedelta(minutes=10)
    }
)

################################################################################
# Global Start and End
################################################################################

t_global_start = BashOperator(
    task_id="global_start",
    bash_command="sleep 1",
    dag=dag
)

t_global_end = BashOperator(
    task_id="global_end",
    bash_command="sleep 1",
    dag=dag
)

for i in range(5):
    task = BashOperator(
        task_id=f"task_{i}",
        bash_command="sleep 1",
        dag=dag
    )
    task.set_upstream(t_global_start)

    for x in range(3):
        task_x = BashOperator(
            task_id=f"task_{i}_{x}",
            bash_command="sleep 1",
            dag=dag
        )
        task_x.set_upstream(task)
        task_x.set_downstream(t_global_end)
