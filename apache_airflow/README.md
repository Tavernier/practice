# Apache Airflow

I use Airflow at work and mostly love it as a workflow scheduling and coordination tool.  Airflow sits on a decent sized box, which has met needs for about two years now and counting.

So, I wanted to play around with horizontally scaling Airflow via Celery.  That's the purpose of this exercise.

## Launching

Run `./up.sh`.  This will run `docker-compose`, which will start these components in the background:

1. Standup and Initialize Airflow's Postgres database.
1. Launch [Airflow's Web UI](http://localhost:8080/admin/) and Scheduler components.
1. Launch Celery's Broker (redis) and [Web UI](http://localhost:5555/) (flower).
1. Launch two Workers.

## Using

1. Visit [Airflow's Web UI](http://localhost:8080/admin/).
1. Enable the [smoke_testing DAG](./dags/smoke_testing.py).
1. Verify all tasks complete.

## Destroying

Run `./down.sh` to destroy everything.

## Known Issues

Sometimes Airflow's web and scheduler components launch faster than Airflow's database can be built.  These crash and need to be restarted. 
