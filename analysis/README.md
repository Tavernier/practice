# Data Analysis

## Overview

- Call `docker-compose up --build` to launch a Jupyter notebook environment.
- The `./notebooks/` path will be mounted to the running container.
- Quit or call `docker-compose down` to exit.

## References

- [Jupyter Image Reference](https://github.com/jupyter/docker-stacks)
- [Jupyter Stacks Reference](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#core-stacks)
- [Compose YAML Reference](https://docs.docker.com/compose/compose-file/)
- [Docker Compose CLI Reference](https://docs.docker.com/compose/reference/overview/)
