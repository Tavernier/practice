#! /usr/bin/env python3
"""Messy code to play around with dictionaries.

https://docs.python.org/3.6/tutorial/datastructures.html#dictionaries
"""

import helpers

# we'll use this dictionary to practice methods and operators
settings = {
    'city': 'Chicago',
    'food': [
        'hot dogs',
        'italian beef'
    ],
    'sights': [
        {'name': 'the bean'}
    ],
    'weather': {
        'temp': 'warm',
        'skies': 'sunny'
    }
}

####################################################################
# Methods
####################################################################

helpers.print_header('Iterating over Key, Values')
for key, value in settings.items():
    print(key, value)

helpers.print_header('Iterating over Keys')
for key in settings.keys():
    print(key)

helpers.print_header('Iterating over Values')
for item in settings.items():
    print(item)


# check for existence of key
assert 'city' in settings

# grab a key's value and use a default when key not found
assert settings.get('score', 10) == 10

####################################################################
# Operators
####################################################################
