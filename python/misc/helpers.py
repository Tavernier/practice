#! /usr/bin/env python3
"""Helper functions for goofing around with python."""

def print_header(header_text):
    """Print a header to stdout."""
    print('-' * 80)
    print('-', header_text)
    print('-' * 80)
