#! /usr/bin/env python3
"""Messy code to play around with lists.

References
https://docs.python.org/3.6/tutorial/datastructures.html
"""

# i'll use this list to goof around.
cities = ["Chicago", "Madison"]

####################################################################
# Methods
####################################################################

cities.append("Buffalo")
assert cities == ["Chicago", "Madison", "Buffalo"]

cities.extend(["Atlanta", "Bozeman"])
assert cities == ["Chicago", "Madison", "Buffalo", "Atlanta", "Bozeman"]

cities.remove("Buffalo")
assert cities == ["Chicago", "Madison", "Atlanta", "Bozeman"]

popped_city = cities.pop(1)
assert cities == ["Chicago", "Atlanta", "Bozeman"]
assert popped_city == "Madison"

cities.insert(1, "Madison")
assert cities == ["Chicago", "Madison", "Atlanta", "Bozeman"]

assert cities.count("Madison") == 1

####################################################################
# Operators
####################################################################

assert ["Chicago"] + ["Madison"] == ["Chicago", "Madison"]
assert ["Chicago"] * 3 == ["Chicago", "Chicago", "Chicago"]

assert "Chicago" in ["Chicago", "Madison"]
assert "Bozeman" not in ["Chicago", "Madison"]

cities = ["Chicago", "Madison", "Buffalo", "Atlanta", "Bozeman"]
del cities[1:3]
assert cities == ["Chicago", "Atlanta", "Bozeman"]

####################################################################
# Miscellaneous
####################################################################

def uppercase(city_name):
    return city_name.upper()

cities = ["Chicago", "Madison", "Atlanta"]

assert list(map(uppercase, cities)) == ["CHICAGO", "MADISON", "ATLANTA"]

assert [x.upper() for x in cities] == ["CHICAGO", "MADISON", "ATLANTA"]

assert list(map(lambda x: x.upper(), cities)) == ["CHICAGO", "MADISON", "ATLANTA"]
