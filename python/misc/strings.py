#! /usr/bin/env python3
"""Messy code to play around with strings.

References
https://docs.python.org/3.7/library/string.html
https://docs.python.org/3.7/library/stdtypes.html#string-methods
"""


####################################################################
# Methods
####################################################################
city = "Chicago"

assert city.lower() == "chicago"
assert city.upper() == "CHICAGO"

# only the first letter
assert "chicago illinois".capitalize() == "Chicago illinois"
assert "chicago illinois".title() == "Chicago Illinois"

# best for string comparisons
assert "Chicago Illinois".casefold() == "chicago illinois"

assert city.center(20,"*") == "******Chicago*******"
assert city.ljust(10, "*") == "Chicago***"
assert city.rjust(10, "*") == "***Chicago"

assert "abcd cdedef efgh ghdeij de".count("de") == 4

assert "settings.yaml".endswith(".yaml")
assert "settings.yaml".startswith("settings.")

assert "friend" in "Howdy there friend, how are you today?".casefold()

assert "Chicago,IL,USA".split(",") == ["Chicago", "IL", "USA"]
assert "Chicago\nIL\nUSA".splitlines() == ["Chicago", "IL", "USA"]


####################################################################
# Operators
####################################################################

assert "Chicago" + "Illinois" == "ChicagoIllinois"
assert "Hello" * 3 == "HelloHelloHello"
assert "Chicago" or "Madison" == "Chicago"
assert "Chicago" and "Madison" == "Madison"
assert None or "Madison" == "Madison"


####################################################################
# Regular Expressions
####################################################################

# The re module is in the standard library.
# Python's documentation also mentions the third-party regex library.
# That library is better due to more features and unicode support.

import re

data = "file_name_20181222_67832178.json"
exp = r'(\d{8})'

# search finds the first match
match = re.search(exp, data)
assert match.groups() == ('20181222',)

# match only starts matching at the beginning of the string
# fullmatch requires the entire string to match
assert re.match(exp, data) is None
assert re.fullmatch(exp, data) is None

assert re.findall(exp, data) == ['20181222', '67832178']
assert [x.group(0) for x in re.finditer(exp, data)] == ['20181222', '67832178']




####################################################################
# Formatting Options
####################################################################
from string import Template

city = "Chicago"
expected_greeting = "Welcome to Chicago!"

assert f"Welcome to {city}!" == expected_greeting

# many options exist for formatting strings.
# see: https://docs.python.org/3.7/library/string.html#formatstrings
assert "Welcome to {0}!".format(city) == expected_greeting

assert "Welcome to {city_name}!".format(city_name=city) == expected_greeting

greeting_template = Template("Welcome to ${city_name}!")
assert greeting_template.substitute(city_name=city) == expected_greeting

