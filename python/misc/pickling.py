#! /usr/bin/env python3

import pickle
import os

settings = {
    'city': 'Chicago',
    'weather': 'F'
}

def save_object(python_object, filename):
    with open(filename, 'wb') as f:
        pickle.dump(python_object, f)

    return os.path.isfile(filename)

def load_object(filename):
    python_object = None

    with open(filename, 'rb') as f:
        python_object = pickle.load(f)

    return python_object


print("Saving object: ", save_object(settings, '/tmp/settings.pck'))

loaded_settings = load_object('/tmp/settings.pck')

print("Loaded object:", loaded_settings)
print("Original object: ", settings)

print("Loaded matches Original: ", loaded_settings == settings)

