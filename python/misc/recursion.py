#! /usr/bin/env python3

schema = {
    "fields":[ 
        {
            "name": "id",
            "type": "integer"
        },
        {
            "name": "geo",
            "type": "record",
            "fields": [
                {
                    "name": "city",
                    "type": "string"
                },
                {
                    "name": "state",
                    "type": "string"
                }
            ]
        }
    ]
}

def flatten_schema(schema):

    fields = schema.get('fields', None)

    if fields:
        flatten_schema(fields)

    print(schema['name'])


flatten_schema(schema)