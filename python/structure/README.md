# Python Project Structure

My goal here is to structure a Python project to include its code, tests, and documentation.

## Guides

1. [Structuring Your Project](https://docs.python-guide.org/writing/structure/)
    - This is my starting guide.  I'll start here then look elsewhere as needed.
1. [Continuous Integration](https://realpython.com/python-continuous-integration/)
    - Includes examples with a linter (flake8) and coverage (pytest-cov).

### Sphinx Documentation

[Sphinx](http://www.sphinx-doc.org/en/master/index.html) seems to be the standard for generating documentation.  So, let's go with that.

There's enough of a learning curve here that I'll document steps I took to add and configure Sphinx here.

1. Create a `./docs/` directory and switch to it.
1. Run `sphinx-quickstart` and answer its questions.
    - I accepted the defaults except for...
        - Project name: Greetings
        - Author: me
        - autodoc: yes
        - Create Windows bat: no
1. Commit to git.
    - I wanted to capture what exists now so I can better track what I need to change to get something usable from here.
1. Add a quickstart document to the master document, `./docs/index.rst`.
    - I also created the actual document in `./docs/usage/quickstart.rst`.
1. In docs, I rank `make html`.
    - The build completed successfully.
    - Neato!  The static pages work: `open _build/html/index.html`
1. I modified `conf.py` to import greetings so autodoc would work as expected.
    - This enabled autodoc of functions such as: `.. autofunction:: greetings.friends.say_hello`
    - Sphinx's [autodoc documentation](http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#module-sphinx.ext.autodoc) lists directives and whatnot.

## Notable Repos

- [Requests](https://github.com/requests/requests)
    - I like this project's flat structure.

## Thoughts

- This is the first time I've seen `Makefile`.  I'm unsure what `.PHONY` accomplishes and need to read more there.
- I dislike `flake8`'s suggestion of empty space on lines between lines of python code.  This is common.  Is that pep8?  Should I disable that check or alter Visual Studio Code's behavior?
