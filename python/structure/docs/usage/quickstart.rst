.. _quickstart:

Quickstart
==========

This is the quickstart document.

Greeting a Friend
-----------------

.. code-block:: python
    
    import greetings.friends

Available Code
^^^^^^^^^^^^^^

.. automodule:: greetings.friends
   :members:


Greeting a Foe
--------------

.. code-block:: python

    import greetings.foes

Available Code
^^^^^^^^^^^^^^

.. automodule:: greetings.foes
   :members:
