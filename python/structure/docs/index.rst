.. Greetings documentation master file, created by
   sphinx-quickstart on Wed Nov 28 09:07:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Greetings's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   usage/quickstart


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
