#! /usr/bin/env python3
"""Friendly greetings live in this file."""


def say_hello(name: str) -> str:
    """Greet our friend.

    Parameters:
        name: Your friend's given name.

    Returns:
        string: A friendly greeting as a string.
    """
    return f"Hello {name}! How are you?"


def praise(name: str) -> str:
    """Congratulate the person on a job well done.

    Parameters:
        name: Your friend's given name.

    Returns:
        string: A statement praising our friend.
    """
    return f"Well done, {name}!"
