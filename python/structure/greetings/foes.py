#! /usr/bin/env python3
"""Negative greetings live in this file."""


def say_goodbye(name: str = None) -> str:
    """Indicate we must leave.

    Parameters:
        name: The person's first name.

    Returns:
        string: A safe statement to get us out of here.
    """

    if name:
        return f"Oh look at the time, {name}. I must be on my way."

    return "Sorry, no thank you."
