#! /usr/bin/env python3
"""Test unfriendly greetings."""

import unittest

from greetings import friends


class TestFriends(unittest.TestCase):
    """Test friendy functions."""

    def test_hello_known(self):
        """Test hello when we know the person's name."""

        self.assertEqual(
            "Hello Garrett! How are you?",
            friends.say_hello('Garrett')
        )

    def test_praise_known(self):
        """Test praise when we know the person's name."""

        self.assertEqual(
            "Well done, Garrett!",
            friends.praise('Garrett')
        )
