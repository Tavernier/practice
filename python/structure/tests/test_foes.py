#! /usr/bin/env python3
"""Test unfriendly greetings."""

import unittest

import greetings.foes


class TestFoes(unittest.TestCase):
    """Tests for functions within foes."""

    def test_goodbye_known_name(self):
        """Test goodbye when we know the person's name."""

        self.assertEqual(
            "Oh look at the time, Willard. I must be on my way.",
            greetings.foes.say_goodbye("Willard")
        )

    def test_goodbye_stranger(self):
        """Test goodbye when we do not know the person."""

        self.assertEqual(
            "Sorry, no thank you.",
            greetings.foes.say_goodbye()
        )
