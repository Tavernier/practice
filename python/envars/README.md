# Environment Variables

I wanted to play around with setting environment variables via Python.  The use case here is using something like Google KMS to encrypt secrets and surface those as environment variables to the running application, which might make local development easier too.

