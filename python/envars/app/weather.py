#! /usr/bin/env python3
"""Get a wether forecast for a city."""

import os

import settings

def get_weather_forecast(city: str, state: str):
    print(f"Weather for {city} and {state}.")

if __name__ == "__main__":
    city = os.environ.get("APP_CITY")
    state = os.environ.get("APP_STATE")
    get_weather_forecast(city, state)
