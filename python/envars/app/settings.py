#! /usr/bin/env python3
"""Set environment variables from a dictionary."""

import os
import sys

settings = {
    "environment": {
        "APP_CITY": "Chicago",
        "APP_STATE": "IL"
    }
}

for key, value in settings.get("environment", {}).items():

    if os.environ.get(key, None):
        sys.stderr.write(f"Overwriting {key}.\n")

    os.environ[key] = value
