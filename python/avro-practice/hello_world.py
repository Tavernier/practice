#! /usr/bin/env python3
"""Avro Practice

See: https://avro.apache.org/docs/current/gettingstartedpython.html
"""

import json
import os

import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter

class AvroReaderWriter(object):
    def __init__(self, schema_filename):
        self.schema_filename = schema_filename
        self.schema = self._schema
    
    @property
    def _schema(self):
        with open(self.schema_filename, "rb") as f:
            return avro.schema.Parse(f.read())

    def write_to_disk(self, users, avro_filename):
        with DataFileWriter(open(avro_filename, "wb"), DatumWriter(), self.schema) as writer:
            for user in users:
                writer.append(user)

        return os.path.exists(avro_filename)

    def read_from_disk(self, avro_filename):
        return DataFileReader(open(avro_filename, "rb"), DatumReader())

if __name__ == "__main__":
    users = AvroReaderWriter('./user.avsc')

    users.write_to_disk(
        [
            {"name": "Alyssa", "favorite_number": 256},
            {"name": "Ben", "favorite_number": 7, "favorite_color": "red"}
        ],
        './users.avro'
    )

    for user in users.read_from_disk('./users.avro'):
        print(user)
        print(type(user))
        if isinstance(user, dict):
            print(json.dumps(user))
