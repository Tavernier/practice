# Combining Settings

I often need to combine multiple files to get a final configuration for doing something such as running a Docker container.  This approach is "good enough" for now.  It combines dictionaries up to one level.

## Other Approaches

I tried these...

- `dict.update(other_dict)` nukes nested dictionaries.
- Python's ConfigParser does not support nested sections.

## Future Enhancement

- Work with any level of nesting (i.e. recursion).