#! /usr/bin/env python3
"""Test combinesettings.py"""

import unittest
import combinesettings

class TestCombinations(unittest.TestCase):
    """Test the ability to merge dictionaries together."""

    def setUp(self):
        self.default_globals = {
            'environment_variables': {
                'DB_HOST': 'localhost',
                'DB_PORT': '5432'
            },
            'cpu': '1',
            'memory': '1Gi',
            'image': 'mydocker:v1'
        }

        self.default_job = {
            'environment_variables': {
                'SPLIT': '200000'
            },
            'cpu': '3',
            'command': ['my_script.sh', 'one', 'two']
        }

        self.environment_globals = {
            'environment_variables': {
                'DB_HOST': 'staging.db.host'
            }
        }

        self.environment_job = {
            'image': 'mydocker:v2',
            'command': ['my_script.sh', 'alpha', 'beta'],
            'logger': 'stdout'
        }

    def test_two_dictionaries(self):
        """Test whether combining two dictionaries works as expected."""
        self.assertDictEqual(
            combinesettings.combine_two_dictionaries(
                self.default_globals,
                self.default_job
            ),
            {
                'environment_variables': {
                    'DB_HOST': 'localhost',
                    'DB_PORT': '5432',
                    'SPLIT': '200000'
                },
                'cpu': '3',
                'memory': '1Gi',
                'image': 'mydocker:v1',
                'command': ['my_script.sh', 'one', 'two']
            }
        )

    def test_2n_combinations(self):
        """Test whether combining 2n dictionaries works as expected."""
        self.assertDictEqual(
            combinesettings.combine_dictionaries(
                self.default_globals,
                self.default_job
            ),
            {
                'environment_variables': {
                    'DB_HOST': 'localhost',
                    'DB_PORT': '5432',
                    'SPLIT': '200000'
                },
                'cpu': '3',
                'memory': '1Gi',
                'image': 'mydocker:v1',
                'command': ['my_script.sh', 'one', 'two']
            }
        )

    def test_4n_combinations(self):
        """Test whether combining 4n dictionaries works as expected."""
        self.assertDictEqual(
            combinesettings.combine_dictionaries(
                self.default_globals,
                self.default_job,
                self.environment_globals,
                self.environment_job
            ),
            {
                'environment_variables': {
                    'DB_HOST': 'staging.db.host',
                    'DB_PORT': '5432',
                    'SPLIT': '200000'
                },
                'cpu': '3',
                'memory': '1Gi',
                'image': 'mydocker:v2',
                'command': ['my_script.sh', 'alpha', 'beta'],
                'logger': 'stdout'
            }
        )

    def test_value_error(self):
        """Confirm we cannot try to merge a string with a dictionary."""

        with self.assertRaises(ValueError):
            combinesettings.combine_two_dictionaries('howdy', {'city': 'Chicago'})

if __name__ == "__main__":
    unittest.main()
