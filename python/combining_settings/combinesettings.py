#! /usr/bin/env python3
"""Functions to combine dictionaries together with the last dictionary winning."""

def combine_dictionaries(*arg):
    """Combine N dictionaries.  The order matters.  The last dictionary wins."""
    final_dictionary = {}

    for dictionary in arg:
        final_dictionary = combine_two_dictionaries(final_dictionary, dictionary)

    return final_dictionary

def combine_two_dictionaries(defaults, overrides):
    """Combines two dictionaries up to one level only."""

    if not isinstance(defaults, dict):
        raise ValueError('Defaults value is not a dictionary!')

    if not isinstance(overrides, dict):
        raise ValueError('Overrides value is not a dictionary!')

    # first, override keys that exist in defaults
    # with keys that exist in overrides.
    for key, value in defaults.items():
        if isinstance(value, dict):
            defaults[key].update(
                overrides.get(key, value)
            )
            continue
        defaults[key] = overrides.get(key, value)

    # then, add keys from overrides that did not exist
    # in defaults
    for key, value in overrides.items():
        if key not in defaults:
            defaults[key] = value

    return defaults
