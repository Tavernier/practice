#! /usr/bin/env python3

import requests

def get_greetings():

    # returns a list of strings
    greetings = requests.get("http://www.mocky.io/v2/5d07fa3134000059005d95d8")

    return greetings.json()


if __name__ == "__main__":

    print("Available greetings:", get_greetings())
