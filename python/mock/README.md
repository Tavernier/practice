# Mocking

The purpose of this example is to better understand mocking and I'll use [Real Python's guide](https://realpython.com/python-mock-library/) to do so ([this article too](https://realpython.com/testing-third-party-apis-with-mocks/)).

## Code

The `greetings.py` file has a function that returns a list of string greetings.  The data is fetched from an external source.  Let's say we always expect this function to return three greetings.  How can mock be used to avoid making the external call to get the list of greetings?

