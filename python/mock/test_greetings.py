#! /usr/bin/env python3

import unittest
from unittest.mock import patch

import greetings

class TestGreetings(unittest.TestCase):

    # The patch will replace the requests.get method
    # and pass it as mock_get.  I need to add an object
    # that has a json() method since that's what I call
    # in the code itself.
    @patch('greetings.requests.get')
    def test_three_greetings(self, mock_get):

        mock_get.return_value.json.return_value = ["one", "two", "three"]

        # no http call will be made due to the mocking
        content = greetings.get_greetings()

        # show that the mock values are used
        print(content)

        return self.assertEqual(len(content), 3)


if __name__ == "__main__":
    unittest.main()
