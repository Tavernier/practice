#!/usr/bin/env python3

from pyspark.sql import SparkSession
import pyspark.sql.functions as sf


def heading(title, level=1):
    """Print a title with dashes above and below the title."""
    dashes = int(80 / level)

    print('-' * dashes)
    print(title)
    print('-' * dashes)


####################################################################################
# Establish Spark Sesssion and Load Data
####################################################################################

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("Analysis") \
    .getOrCreate()

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.csv

# i created a file with 1,000,000 fake orders and one with 10,000 orders
# orders.csv.gz = a million orders
# orders_10000.csv.gz = 10,000 orders
# order_id : string (might load as int)
# brand_name : string
# quantity : int

# Spark automagically detects gzip.  No need to specify compression here.
df = spark.read.csv(
    './input/orders_10000.csv.gz',
    schema="order_id STRING, brand_name STRING, quantity INT",
    header=True
)


####################################################################################
# Get to Know the Data
####################################################################################

heading('Get to Know the Data')

print("Showing Schema")
df.printSchema()

print("Column names")
print(df.columns, '\n')

print("Schema objects")
print(df.schema, '\n')

print("Showing Head")
df.show(n=10)

print("Summary Stats including Five Number Summary")
df.summary().show()

print("Basic Description")
df.describe().show()


####################################################################################
# Analysis via DataFrame
####################################################################################

# The goal here is to understand aggregations and such via DataFrame operations.
# Later on, I'll attempt the same with SQL in Spark.  The orders data set is
# pretty simple so I'm not going to explore joins and set operations at this point.

heading('Analysis via DataFrame')

# selecting just a few columns names
# note: avoid head() here as that returns a list
df.select(['brand_name', 'order_id']).show(10)

# top N by X
df.sort('quantity', ascending=False).show(10)

# order by multiple attributes
df.sort(['quantity', 'order_id'], ascending=[False, True]).show(10)

# unique values

# single column aggregation, single aggregation
# for available aggs: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.GroupedData
df.groupby('brand_name').agg(
    {'quantity': 'mean'}
).show()

# single column aggregation, multiple aggregation


# multiple column aggregation, single aggregation

# filtering before aggregation
# where() is an alias for filter()
df.where("brand_name in ('Target', 'Best Buy')").groupby('brand_name').agg(
    {'quantity': 'mean'}
).show()

# filtering after aggregation

# multiple column aggregation

# adding a new column
# also see https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame.selectExpr
# but, that does not return a nice column name

# i could not get this string aggregation to work
# the second param must be a column expression.
# i also tried sf.concat('R', df['order_id'])
# also tried python string formatting
df.withColumn('external_order_id', 'R' + df['order_id']).show(10)

# adding many new columns

# transforming data

####################################################################################
# Analysis via SQL
####################################################################################

# Let's see how SQL works within Spark.  I'm unsure on what flavor of SQL is 
# supported or where to find documentation.

# see: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame.createOrReplaceTempView
# "The lifetime of this temporary table is tied to the SparkSession that was used to create this DataFrame."
df.createOrReplaceTempView("orders")

# selecting just a few columns names
spark.sql("select order_id, brand_name from orders").show(10)

# top N by X
spark.sql("select * from orders order by quantity desc").show(10)

# order by multiple attributes
spark.sql("select * from orders order by quantity desc, order_id").show(10)

# unique values
spark.sql("select distinct brand_name from orders order by 1").show(10)

# single column aggregation, single aggregation
spark.sql("select brand_name, avg(quantity) as quantity_avg from orders group by brand_name order by 1").show(10)

# single column aggregation, multiple aggregation
spark.sql("select brand_name, avg(quantity) as quantity_avg, max(quantity) as quantity_max from orders group by brand_name order by 1").show(10)

# multiple column aggregation, single aggregation

# filtering before aggregation
spark.sql("select brand_name, avg(quantity) as quantity_avg from orders where brand_name in ('Target', 'Best Buy') group by brand_name order by 1").show(10)

# filtering after aggregation
spark.sql("select brand_name, avg(quantity) as quantity_avg from orders group by brand_name having avg(quantity) > 17.3 order by 1").show(10)

# adding a new column
spark.sql("select *, concat('R', order_id) as public_order_id from orders").show(10)

# adding many new columns
spark.sql("select order_id, brand_name, concat('R', order_id) as public_order_id, lower(replace(brand_name, ' ', '_')) as brand_slug from orders").show(10)

####################################################################################
# Good Bye Spark
####################################################################################
spark.stop()
