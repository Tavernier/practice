#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, TimestampType

from helpers import show_dataframe_info

####################################################################################
# Establish Spark Sesssion
####################################################################################

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("PartitionExample") \
    .getOrCreate()


####################################################################################
# Read hand-created CSV
####################################################################################

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.csv
df = spark.read.csv(
    './input/orders.csv.gz',
    schema="order_id STRING, brand_name STRING, quantity INT",
    header=True
)

show_dataframe_info(df, 'Raw Orders Data')


####################################################################################
# Write to Disk
####################################################################################

# Paritioned by Brand Name
# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.csv
df.write.partitionBy("brand_name").csv(
    './output/orders_partitioned.csv',
    mode='overwrite',
    header=True
)


####################################################################################
# Read what we just wrote
####################################################################################

# now that we have data partitioned by brand_name, how can we read just one 
# particular brand and not hit the disk for other brands?
df2 = spark.read.csv(
    './output/orders_partitioned.csv/brand_name=Amazon',
    schema="order_id STRING, quantity INT",  # the schema changed in the file!
    header=True
)

show_dataframe_info(df2, 'Amazon Data')

# now that we have data partitioned by brand_name, how can we read just one 
# particular brand and not hit the disk for other brands?
df3 = spark.read.csv(
    [
        './output/orders_partitioned.csv/brand_name=Amazon',
        './output/orders_partitioned.csv/brand_name=Best Buy'
    ],
    schema="order_id STRING, quantity INT",  # the schema changed in the file!
    header=True
)

show_dataframe_info(df3, 'Amazon and Best Buy Data')


# Spark seems to automatically add the partition here.
df_all = spark.read.csv(
    './output/orders_partitioned.csv',
    schema="order_id STRING, quantity INT",  # the schema changed in the file!
    header=True
)

show_dataframe_info(df_all, 'All Partitioned Data')

spark.stop()
