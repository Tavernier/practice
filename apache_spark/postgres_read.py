#!/usr/bin/env python3

from pyspark import SparkConf
from pyspark.sql import SparkSession

from helpers import db_connections

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadPostgres") \
    .getOrCreate()

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.jdbc

df = spark.read.jdbc(
    url=db_connections['postgres']['url'],
    table='jt_people',
    properties=db_connections['postgres']['properties']
)

df.show()
df.printSchema()

# we can run an arbitrary query like this
df2 = spark.read.jdbc(
    url=db_connections['postgres']['url'],
    table='(select * from jt_people where age > 38) as t',
    properties=db_connections['postgres']['properties']
)

df2.show()
df2.printSchema()

spark.stop()
