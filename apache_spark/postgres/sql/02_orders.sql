drop table if exists jt_orders;

create table jt_orders(
    order_id int,
    order_number varchar(20),
    constraint pk_orders primary key (order_id)
);

insert into jt_orders(order_id, order_number)
select i as order_id, 'R' || lpad(cast(i as varchar), 10, '0') as order_number
from (
    select i
    from generate_series(1, 10000000) i
) t;
