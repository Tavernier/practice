#! /usr/bin/env bash

set -eu

source ./setup.sh

# name of python file to submit
job_file_to_submit=$1

# send to spark-submit
# see spark-submit --help for options
# i cannot get the override to actually work yet
"$SPARK_INSTALL_PATH"/spark/bin/spark-submit \
    --conf "spark.executor.extraJavaOptions=-Dlog4j.configuration=file:$(pwd)/log4j.properties" \
    --conf "spark.driver.extraJavaOptions=-Dlog4j.configuration=file:$(pwd)/log4j.properties" \
    --jars "$(pwd)/drivers/postgresql-42.2.4.jar,$(pwd)/drivers/mysql-connector-java-8.0.11.jar" \
    --master local[4] \
    "$job_file_to_submit"
