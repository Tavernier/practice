#!/usr/bin/env python3

"""Spark Streaming SQL Example

Stolen from: https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html

Steps:
1. Start netcat data server via: nc -lk 9999
2. Submit this spark job
3. Type sentences into netcat session
4. View output of Spark job in console
"""

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split

spark = SparkSession \
    .builder \
    .appName("StructuredNetworkWordCount") \
    .getOrCreate()

# Create DataFrame representing the stream of input lines from connection to localhost:9999
lines = spark \
    .readStream \
    .format("socket") \
    .option("host", "localhost") \
    .option("port", 9999) \
    .load()

# Split the lines into words
words = lines.select(
   explode(
       split(lines.value, " ")
   ).alias("word")
)

# Generate running word count
wordCounts = words.groupBy("word").count()

print("wordCounts type: ", type(wordCounts))

 # Start running the query that prints the running counts to the console
query = wordCounts \
    .writeStream \
    .outputMode("complete") \
    .format("console") \
    .start()

print("query type: ", type(wordCounts))

query.awaitTermination()
