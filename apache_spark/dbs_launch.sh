#! /usr/bin/env bash

set -e
set -x

# I use this script to launch test Postgres and MySQL instances.
# The expectation here is the images launch with all needed data 
# and permissions for my Spark testing.

# Postgres will launch on port 25432
# MySQL will launch on port 13306

# Postgres creds are postgres/blah
# MySQL creds are root/blah

build_postgres() {
    # i add custom scripts to the image so need to build my own
    pushd ./postgres
    docker build . --tag jon-postgres-spark
    popd
}

launch_postgres(){
    # launch postgres on port 25432
    docker run \
        --name postgres-spark-testing \
        -e POSTGRES_PASSWORD=blah \
        -d \
        -p 25432:5432 \
        --rm 'jon-postgres-spark'
}

build_mysql() {
    # i add custom scripts to the image so need to build my own
    pushd ./mysql
    docker build . --tag jon-mysql-spark
    popd
}

launch_mysql(){
    docker run \
        --name mysql-spark-testing \
        -e MYSQL_ROOT_PASSWORD=blah \
        -d \
        -p 13306:3306 \
        --rm 'jon-mysql-spark'
}

main(){
    build_postgres
    build_mysql

    launch_postgres
    launch_mysql
}

main
