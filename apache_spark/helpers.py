#!/usr/bin/env python3

import os

from google.cloud import bigquery

##########################################################################################
# DB connection info for test dbs run locally via dbs_launch.sh
##########################################################################################
db_connections = dict()

db_connections['mysql'] = {
    'url': 'jdbc:mysql://localhost:13306/jt_test',
    'properties': {'user': 'root', 'password': 'blah',  'driver': 'com.mysql.cj.jdbc.Driver'}
}

db_connections['postgres'] = {
    'url': 'jdbc:postgresql://localhost:25432/postgres',
    'properties': {'user': 'postgres', 'password': 'blah',  'driver': 'org.postgresql.Driver'}
}

# see https://www.simba.com/products/BigQuery/doc/JDBC_InstallGuide/content/jdbc/bq/authenticating/useraccount.htm
db_connections['bigquery'] = {
    'project_id': 'fluted-bot-152217',
    'private_key': '/secrets/apache-spark-jon.json'
    
}


def get_bq_results(query):

    client = bigquery.Client(
        project='fluted-bot-152217'
    )

    query_job = client.query(
        query
    )  # API request - starts the query

    for row in query_job:
        yield row
    

def show_dataframe_info(dataframe, title=None):

    if title:
        print('-'*80, f'\n{title}')
    
    print('-'*40, '\nDescription')
    dataframe.describe().show()

    print('-'*40, '\nHead')
    dataframe.show(10)

    print('-'*40, '\nSchema')
    dataframe.printSchema()