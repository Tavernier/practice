# Apache Spark

We are launching Apache Spark.  I want to learn more about using it.

## Background Information

* Spark "is a unified analytics engine for large-scale data processing."
* Write applications quickly in Java, Scala, Python, R, and SQL.
* Current version is Spark 2.3.1, released on June 8, 2018.  Today is July 19th.
* Combine SQL, streaming, and complex analytics.
* Spark runs on Hadoop, Apache Mesos, Kubernetes, standalone, or in the cloud. It can access diverse data sources.

## To Do

* Learn about standard libraries (e.g. `from pyspark.sql.functions import *`)
* Learn about Spark DataFrame object (is it similar to Pandas?)
* How do we submit jobs to a remote Spark cluster?
    * Assume we submit jobs via Apache Airflow, on one k8s POD, to Apache Spark, on another k8s POD.

## Quick Start Guide

I will work through this guide: https://spark.apache.org/docs/latest/quick-start.html

Starting in Spark 2, the main data structure is the Dataset.  Previously, it was the Resilient Distributed Dataset (RDD).  RDDs are replaced by Datasets so I'll work with those.

### Getting Spark

> To follow along with this guide, first download a packaged release of Spark from the Spark website. Since we won’t be using HDFS, you can download a package for any version of Hadoop.

So, I downloaded: http://mirrors.sonic.net/apache/spark/spark-2.3.1/spark-2.3.1-bin-hadoop2.7.tgz

```bash
# prep temp directory
mkdir -p /tmp/spark
cd /tmp/spark

# download and unpackage
wget http://mirrors.sonic.net/apache/spark/spark-2.3.1/spark-2.3.1-bin-hadoop2.7.tgz
tar -xvzf spark-2.3.1-bin-hadoop2.7.tgz

cd spark-2.3.1-bin-hadoop2.7
```

Also, run `./setup.sh` to help Spark connect to localhost.  This fixed the `Service 'sparkDriver' could not bind` issue.

See: https://stackoverflow.com/questions/40522545/why-does-spark-not-run-locally-while-it-should-be-possible-according-the-documen

### Spark Shell

* Spark’s shell provides a simple way to learn the API, as well as a powerful tool to analyze data interactively.
* It's available in Scala and Python.  I'll go with Python.

```bash
cd /tmp/spark/spark-2.3.1-bin-hadoop2.7

./bin/pyspark
```

I got in:

```
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /__ / .__/\_,_/_/ /_/\_\   version 2.3.1
      /_/

Using Python version 3.6.5 (default, Apr 26 2018 08:42:37)
SparkSession available as 'spark'.
```

Loading the ReadMe file into a Dataset, which is a silly exercise, but nonetheless:

```python
textFile = spark.read.text("README.md")
textFile.count()  # Number of rows in this DataFrame
textFile.first()  # First row in this DataFrame

type(textFile) # <class 'pyspark.sql.dataframe.DataFrame'>

# create a new DataFrame
linesWithSpark = textFile.filter(textFile.value.contains("Spark"))
linesWithSpark.count() # 20

# transformations and actions can be chained together
textFile.filter(textFile.value.contains("Spark")).count()
```

#### More Dataset Operations

> Dataset actions and transformations can be used for more complex computations. Let’s say we want to find the line with the most words:

```python
from pyspark.sql.functions import *

# returns [Row(max(numWords)=22)]
textFile.select(size(split(textFile.value, "\s+")).name("numWords")).agg(max(col("numWords"))).collect()
```

About that line of code:

> This first maps a line to an integer value and aliases it as “numWords”, creating a new DataFrame. agg is called on that DataFrame to find the largest word count. The arguments to select and agg are both Column, we can use df.colName to get a column from a DataFrame. We can also import pyspark.sql.functions, which provides a lot of convenient functions to build a new Column from an old one.

#### Caching

Spark also supports pulling data sets into a cluster-wide in-memory cache. This is very useful when data is accessed repeatedly, such as when querying a small “hot” dataset or when running an iterative algorithm like PageRank.

```python

# we still have this set from above:
linesWithSpark.count()

# mark it as cached
linesWithSpark.cache()
```

### Self-Contained Applications

* Spark supports applications written in Python, Scala, and Java.
* The Python API is named PySpark.

The quick start guide shows this file, `SimpleApp.py`:

```python
"""SimpleApp.py"""
from pyspark.sql import SparkSession

logFile = "YOUR_SPARK_HOME/README.md"  # Should be some file on your system
spark = SparkSession.builder.appName("SimpleApp").getOrCreate()
logData = spark.read.text(logFile).cache()

numAs = logData.filter(logData.value.contains('a')).count()
numBs = logData.filter(logData.value.contains('b')).count()

print("Lines with a: %i, lines with b: %i" % (numAs, numBs))

spark.stop()
```

That file will:

* Create a spark session.
* Read the ReadMe text file from disk and cache it in the cluster.  Why cache it?
* Count the number of lines with the letter `a`.
* Count the number of lines with the letter `b`.
* Print the results.
* Stop the spark session / app.

Let's setup a virtualenv for installation practice.  I also set this venv as Visual Studio Code's interpreter for this example.

```python
mkvirtualenv apache_spar
pip3 install pyspark
```

Then run our SimpleApp.py:

```
./SimpleApp.py 
2018-07-19 09:10:09 WARN  NativeCodeLoader:62 - Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
Setting default log level to "WARN".
To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).
2018-07-19 09:10:10 WARN  Utils:66 - Service 'SparkUI' could not bind on port 4040. Attempting port 4041.
Lines with a: 61, lines with b: 30
```

#### Running the Application

Since I created a venv with pyspark, I was able to start the app on the command line simply by executing the script via `./SimpleApp.py`.

The quick start guide also states you can submit the application via the `bin/spark-submit` script.  Let's try that.  I started a new shell to break out of my venv.  My workdir has `./SimpleApp.py` so let's submit that.

```
/tmp/spark/spark-2.3.1-bin-hadoop2.7/bin/spark-submit \
  --master local[4] \
  ./SimpleApp.py
```

Submitting the app appears to have worked.  About two billion log lines printed to stdout.  Here is the head of that output, the lines around the applications print statement, and the tail of the output.

```
2018-07-19 09:17:35 INFO  SparkContext:54 - Running Spark version 2.3.1
2018-07-19 09:17:35 INFO  SparkContext:54 - Submitted application: SimpleApp
2018-07-19 09:17:35 INFO  SecurityManager:54 - Changing view acls to: jon.tavernier
2018-07-19 09:17:35 INFO  SecurityManager:54 - Changing modify acls to: jon.tavernier
2018-07-19 09:17:35 INFO  SecurityManager:54 - Changing view acls groups to: 
2018-07-19 09:17:35 INFO  SecurityManager:54 - Changing modify acls groups to: 
...
2018-07-19 09:17:39 INFO  DAGScheduler:54 - Job 1 finished: count at NativeMethodAccessorImpl.java:0, took 0.039564 s
Lines with a: 61, lines with b: 30
2018-07-19 09:17:39 INFO  AbstractConnector:318 - Stopped Spark@16ac5e53{HTTP/1.1,[http/1.1]}{0.0.0.0:4041}
...
2018-07-19 09:17:39 INFO  SparkContext:54 - Successfully stopped SparkContext
2018-07-19 09:17:40 INFO  ShutdownHookManager:54 - Shutdown hook called
2018-07-19 09:17:40 INFO  ShutdownHookManager:54 - Deleting directory /private/var/folders/0z/v12bznk537d7yd_y6zhcxky43zc49g/T/spark-462b4239-1d4c-4078-9be3-9d7adf59d46e
2018-07-19 09:17:40 INFO  ShutdownHookManager:54 - Deleting directory /private/var/folders/0z/v12bznk537d7yd_y6zhcxky43zc49g/T/spark-11b6cd88-9d36-42dc-8e42-1b5edcd7bcf8
2018-07-19 09:17:40 INFO  ShutdownHookManager:54 - Deleting directory /private/var/folders/0z/v12bznk537d7yd_y6zhcxky43zc49g/T/spark-462b4239-1d4c-4078-9be3-9d7adf59d46e/pyspark-dd0d60f6-f0d6-49fa-b30b-20868d907e10
```

This kind of looks like Apache Airflow!

## Summary

I downloaded Spark, launched its interactive shell, and ran a simple application.  Everything worked as expected.  

The quick start guide recommends these next steps:

* For an in-depth overview of the API, start with the RDD programming guide and the SQL programming guide, or see “Programming Guides” menu for other components.
    * Note: RDD has been replaced with Dataset so this might be an outdated recommendation.
    * Here's the SQL, DataFrame programming guide instead: https://spark.apache.org/docs/latest/sql-programming-guide.html
* For running applications on a cluster, head to the deployment overview.
* Finally, Spark includes several samples in the examples directory (Scala, Java, Python, R). You can run them as follows:

```
# For Scala and Java, use run-example:
./bin/run-example SparkPi

# For Python examples, use spark-submit directly:
./bin/spark-submit examples/src/main/python/pi.py

# For R examples, use spark-submit directly:
./bin/spark-submit examples/src/main/r/dataframe.R
```