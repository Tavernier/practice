# Spark SQL, DataFrames, and Datasets

I am following this guide: https://spark.apache.org/docs/latest/sql-programming-guide.html

Apache indicates RDDs are old news and Datasets are the new thing.  So, I'll learn Datasets.  

## Overview

* Spark SQL is a Spark module for *structured data processing*.
* Both SQL and the Dataset API interact with Spark SQL.  The same execution engine is used for both.

* A Dataset is a distributed collection of data.
    *  The Dataset API is available in Scala and Java, but not Python yet!
* A DataFrame is a Dataset organized into named columns.  So, it is a distributed collection of data organized into named columns.
    * It is conceptually equivalent to a table in a relational database or a data frame in R/Python.
    * The DataFrame API is available in Scala, Java, Python, and R.

## Programming

The entry point into all functionality in Spark is the SparkSession class. 
Example:

```python
from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL basic example") \
    .config("spark.some.config.option", "some-value") \
    .getOrCreate()
```

### DataFrames

DataFrames can be created from an existing RDD, from a Hive table, or from Spark data sources.

Spark data sources include: parquet, json, etc.  Can we query relational databases and BigQuery?  See `spark.read`.

Let's launch the interactive shell:

```
cd /tmp/spark/spark-2.3.1-bin-hadoop2.7
./bin/pyspak
```

We can see this creates a "SparkSession available as 'spark'."  We can then run through the example of creating a data frame from a JSON file:

```
>>> df = spark.read.json("examples/src/main/resources/people.json")
2018-07-19 09:57:35 WARN  ObjectStore:568 - Failed to get database global_temp, returning NoSuchObjectException
>>> df.head()
Row(age=None, name='Michael')
>>> df.show()
+----+-------+
| age|   name|
+----+-------+
|null|Michael|
|  30|   Andy|
|  19| Justin|
+----+-------+
```

#### DataFrame Operations (aka Untyped Dataset Operations)

* Access columns using `df.column_name` or `df['column_name']`.
    * The indexing method is recommended since it is future proof and will not conflict with method names: `df['column_name']`
* DataFrame documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame
* Functions for manipulating data: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#module-pyspark.sql.functions

Using the DataFrame from above:

```
df.printSchema()
root
 |-- age: long (nullable = true)
 |-- name: string (nullable = true)


# Select only the "name" column
df.select("name").show()
+-------+
|   name|
+-------+
|Michael|
|   Andy|
| Justin|
+-------+


# Select everybody, but increment the age by 1
df.select(df['name'], df['age'] + 1).show()
+-------+---------+
|   name|(age + 1)|
+-------+---------+
|Michael|     null|
|   Andy|       31|
| Justin|       20|
+-------+---------+

df.select(df['name'], df['age'] + 1).alias(['name', 'new_age']).show()

# Select people older than 21
df.filter(df['age'] > 21).show()
+---+----+
|age|name|
+---+----+
| 30|Andy|
+---+----+


# Count people by age
df.groupBy("age").count().show()
+----+-----+
| age|count|
+----+-----+
|  19|    1|
|null|    1|
|  30|    1|
+----+-----+

# get first N rows
df.show(2)
+----+-------+
| age|   name|
+----+-------+
|null|Michael|
|  30|   Andy|
+----+-------+
only showing top 2 rows

```

### Running SQL

* Temporary views in Spark SQL are session-scoped and will disappear if the session that creates it terminates.
* Create a global temporary view when you want a view that is shared among all sessions and kept alive until the Spark application terminates.

```
# Register the DataFrame as a SQL temporary view
df.createOrReplaceTempView("people")

sqlDF = spark.sql("SELECT * FROM people")
sqlDF.show()

+----+-------+
| age|   name|
+----+-------+
|null|Michael|
|  30|   Andy|
|  19| Justin|
+----+-------+

spark.sql("SELECT * FROM people where age > 20").show()
+---+----+
|age|name|
+---+----+
| 30|Andy|
+---+----+


# global temp view exmaple
df.createGlobalTempView("people")
spark.sql("SELECT * FROM global_temp.people").show()
+----+-------+
| age|   name|
+----+-------+
|null|Michael|
|  30|   Andy|
|  19| Justin|
+----+-------+
```

Queries can be executed directly against files too:

```python
df = spark.sql("SELECT * FROM parquet.`examples/src/main/resources/users.parquet`")
```

## Data In

* Parquet files
* ORC files
* JSON files, newline delimieted.  One object on each line.
* Hive tables
* JDBC to other databases
    * https://spark.apache.org/docs/latest/sql-programming-guide.html#jdbc-to-other-databases

## Data Out

* Temporary SQL in memory views
* Parquet files
* Hive persistent tables
    * the DataFrame will be written to disk
    * Persistent tables will still exist even after your Spark program has restarted, as long as you maintain your connection to the same metastore.
* See `dataframe.write` that returns a `DataFrameWriter`: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter

## Performance Notes

* Data can be stored in a partitioned format:
    * https://spark.apache.org/docs/latest/sql-programming-guide.html#partition-discovery
* Data can be cached in memory:
    * https://spark.apache.org/docs/latest/sql-programming-guide.html#caching-data-in-memory
* You can ask Spark to broadcast some tables:
    * https://spark.apache.org/docs/latest/sql-programming-guide.html#broadcast-hint-for-sql-queries

## Q & A

### What is `spark.sparkContext` vs. `spark.read`?

```python
sc = spark.sparkContext
lines = sc.textFile("examples/src/main/resources/people.txt")
```

vs.

```python
df = spark.read.json("examples/src/main/resources/people.json")
```

`textFile` is for RDS while `read` is for DataFrame.

See: https://stackoverflow.com/questions/50570381/reading-files-from-s3-bucket-to-pyspark-dataframe-boto3

### What data types are supported in Spark SQL?

See: https://spark.apache.org/docs/latest/sql-programming-guide.html#data-types

### How do we read/write with AWS S3?

Reading:

See: https://stackoverflow.com/questions/50570381/reading-files-from-s3-bucket-to-pyspark-dataframe-boto3

Writing:

Note: writing direclty is unsafe.
See: https://stackoverflow.com/questions/46464065/writing-spark-dataframe-as-parquet-to-s3-without-creating-a-temporary-folder

### How do we read/write with Google BigQuery?

I have failed with the Simba JDBC error.  For some reason, the column names get double quoted in the submitted query.  So, BigQuery returns the actual column names as values and the not the real values themselves.  I see no configuration option to modify this behavior.  This seems like a known issue too: https://stackoverflow.com/questions/47020379/bigquery-simba-jdbc-error-with-spark

Might want to explore: https://cloud.google.com/dataproc/docs/concepts/connectors/bigquery

We could go Pandas DataFrame from BigQuery to SparkSQL DataFrame, but that assumes the Pandas DataFrame will be smallish or fit into whatever memory is available.

To write data into BigQuery, we could:

* Convert SparkSQL DataFrame to Pandas DataFrame then write that to BigQuery, but that assumes the Pandas DataFrame will be smallish or fit into whatever memory is available.
* Write Spark DataFrame to disk and load those file(s) to BigQuery.  We might need to upload to GCS first.

### How do we read/write with Postgres or MySQL?

See `./postgres_read.py` and `./postgres_write.py`.

See `./mysql_read.py` and `./mysql_write.py`.

### How do we read/write json?

See `./json_read.py` and `./json_write.py`.

* This does not produce a single file! Spark is not meant to do this since it's a distributed data tool.
* Path = "the path in any Hadoop supported file system"

### How do we read/write nested json structures?


### How do we read/write csv?

See `./csv_read.py` and `./csv_write.py`.

* This does not produce a single CSV! Spark is not meant to do this since it's a distributed data tool.
    * See https://stackoverflow.com/questions/47053760/how-to-write-data-as-single-normal-csv-file-in-spark
* Path = "the path in any Hadoop supported file system"

### How do we read/write Avro?


### How does pyspark handle Python's yield?

Can we yield to a pyspark Dataframe and parallelize?


### How does Spark handle large files?

Assume I have a 3 GB file on disk as received by a vendor.  Do I need that much memory to work with that file or can Spark stream / read only certain lines at a time?

Does `spark.read.load(filename)` pull the entire file into memory?

See: https://stackoverflow.com/questions/46638901/how-spark-read-a-large-file-petabye-when-file-can-not-be-fit-in-sparks-main-m


### Does data need to be stored in Hadoop?

Where can data be stored?

### Should we learn RDDs?  Seems like they are replaced with Datasets and DataFrames.

Praj recommended learning DataFrames, but note that Datasets and RDDs still serve a purpose.  He provided this article: https://databricks.com/blog/2016/07/14/a-tale-of-three-apache-spark-apis-rdds-dataframes-and-datasets.html

### How does Spark perform work in parallel?

Spark's DataFrame is distributed (ref: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame)

The `foreach` method will apply a function to each row: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame.foreach

Q1: Let's say we have a giant newline delimited JSON file and we want to perform work on each JSON blob.  Does Spark just automagically parallelize that work?

df.read.json = DataFrame
df.foreach(f)?

How do we know the operation succeeded for all rows?


Q2: Let's say we get a list of IDs from a Postgres query.  How do we parallelize the work for each ID?

spark.read.jdbc = DataFrame
df.foreach(f)?

### How does Spark partition data?

I saw some documentation related to partitions.  How does this work?

See: http://www.cs.sfu.ca/CourseCentral/732/ggbaker/content/spark-sql.html#partition
See: https://spark.apache.org/docs/latest/sql-programming-guide.html#partition-discovery

See `partition_example.py`.

That script loads the `orders.csv.gz` file then writes it partitioned by brand_name.

The files on disk then look like:

```
orders.csv/
├── _SUCCESS
├── brand_name=Amazon
│   └── part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
├── brand_name=Best\ Buy
│   └── part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
├── brand_name=Foot\ Locker
│   └── part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
├── brand_name=McDonalds
│   └── part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
└── brand_name=Target
    └── part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
```

Here's information about the Amazon partition:

```bash
head part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv 
order_id,quantity
4,23
6,28
7,22
8,29
12,20
19,8
31,4
34,5
40,6

wc -l part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv 
  199962 part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv

ls -lth
total 3744
-rw-r--r--  1 jon.tavernier  staff   1.8M Jul 23 09:00 part-00000-605e71ea-8d0b-4e42-b619-da7c3cf9a6b4.c000.csv
```

What's interesting there is the brand_name has been removed from the file.  That will save a ton of space in this example.

If we read multiple partitions, however, how do we know from which partition the data originated?  I do not see that value coming back to the DataFrame when reading partitioned files.

Additional fun things to know:

* Multiple partition levels are supported.
* Bucketing is an option too: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.bucketBy

### What's the difference between `bucketBy` and `partitionBy`?

* bucketBy
    * "Buckets the output by the given columns.If specified, the output is laid out on the file system similar to Hive’s bucketing scheme."
    * The input here is the number of buckets to create and the name of one or more columns.
    * So, this may be more flexible than `partitionBy`.  We could break down a column that has many unique values.
    * Ref: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.bucketBy
* partitionBy
    * "Partitions the output by the given columns on the file system."
    * The input here is one or more column names.
    * Ref: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.partitionBy

### What's the downside of creating views of DataFrames and running SQL against those?

Instead of performing operationson DataFrames.

### What flavor of SQL does Spark run?

I cannot find a link to documentation.  Hive SQL?

### What's the preferred way to submit jobs in production?

Via `spark-submit`?
Just calling the python executable?
