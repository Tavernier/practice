#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, TimestampType


# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadJSON") \
    .getOrCreate()

# the file must be newline delimited json blobs
# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.json

# this file has a schema:
#   name: string
#   birth_year: int
#   age: float
#   last_seen: datetime

# i could not get read.json to import last_seen as a timestamp
# and we cannot specify just that field in the schema, so we 
# must specify everything

schema = StructType([
    StructField("last_seen", TimestampType(), True)
])

# let's read and specify a schema.
# i'm using the DDL string variety here.
# could also pass a StructType as seen above, which requires defining all attributes.
df = spark.read.json(
    './input/people.json',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP"
)

df.show()

df.printSchema()

spark.stop()
