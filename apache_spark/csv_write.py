#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, TimestampType

####################################################################################
# Establish Spark Sesssion
####################################################################################

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("WriteCSV") \
    .getOrCreate()


####################################################################################
# Read hand-created CSV
####################################################################################

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.csv
df = spark.read.csv(
    './input/people.csv',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP",
    header=True
)

df.show()

df.printSchema()


####################################################################################
# Write to CSV
####################################################################################

# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.csv
df.write.csv(
    './output/people.csv',
    mode='overwrite',
    header=True
)


####################################################################################
# Read what we just wrote
####################################################################################

df2 = spark.read.csv(
    './output/people.csv',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP",
    header=True
)

df2.show()

df2.printSchema()

spark.stop()
