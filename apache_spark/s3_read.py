#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, TimestampType

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadJSON") \
    .getOrCreate()

# the file must be newline delimited json blobs
# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.json
schema = StructType([
    StructField("name", StringType(), True)
])

# https://hadoop.apache.org/docs/r2.8.0/hadoop-aws/tools/hadoop-aws/index.html#S3A
df = spark.read.json(
    's3a://need_a_bucket_here',
    #compression='gzip',
    schema=schema
)

df.show()

df.printSchema()

spark.stop()
