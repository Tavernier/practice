#!/usr/bin/env bash

set -eux

recreate_temp(){
    rm -rf /tmp/spark
    mkdir -p /tmp/spark
}

download_unpackage_spark(){
    pushd /tmp/spark

    curl \
    --output spark-2.3.2-bin-hadoop2.7.tgz \
    'https://archive.apache.org/dist/spark/spark-2.3.2/spark-2.3.2-bin-hadoop2.7.tgz' \
    && tar -xvzf spark-2.3.2-bin-hadoop2.7.tgz \
    && ln -sf /tmp/spark/spark-2.3.2-bin-hadoop2.7 /tmp/spark/spark

    popd
}

show_spark(){
    echo "Spark install at /tmp/spark"
    ls /tmp/spark/spark
}

main(){
    recreate_temp
    download_unpackage_spark
    show_spark
}

main
