#!/usr/bin/env python3

from pyspark import SparkConf
from pyspark.sql import SparkSession

from helpers import db_connections

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadMySQL") \
    .getOrCreate()

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.jdbc

df = spark.read.jdbc(
    url=db_connections['mysql']['url'],
    table='jt_people',
    properties=db_connections['mysql']['properties']
)

df.show()
df.printSchema()


# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.jdbc
# note the statement about Spark bringing down the data store due to partitions

# bonus: the table need not exist beforehand
# i didn't explore how to accomplish fancier stuff (add index, PK, etc.)
df.write.jdbc(
    url=db_connections['mysql']['url'],
    table='jt_people_output',
    mode='overwrite',  # default is to error if already exists
    properties=db_connections['mysql']['properties']
)

df2 = spark.read.jdbc(
    url=db_connections['mysql']['url'],
    table='jt_people_output',
    properties=db_connections['mysql']['properties']
)

df2.show()
df2.printSchema()

spark.stop()
