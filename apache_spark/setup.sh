#! /usr/bin/env bash

export SPARK_LOCAL_IP="127.0.0.1"
export GOOGLE_APPLICATION_CREDENTIALS="/secrets/apache-spark-jon.json"
export SPARK_INSTALL_PATH='/tmp/spark'
