#!/usr/bin/env python3

from pyspark.sql import SparkSession

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("HelloWorld") \
    .getOrCreate()

print("I am spark!")

spark.stop()
