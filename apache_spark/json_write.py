#!/usr/bin/env python3

from pyspark.sql import SparkSession

####################################################################################
# Establish Spark Sesssion
####################################################################################

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("WriteJSON") \
    .getOrCreate()



####################################################################################
# Read hand-created Data
####################################################################################
# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.json
df = spark.read.json(
    './input/people.json',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP"
)

df.show()

df.printSchema()


####################################################################################
# Write to Disk
####################################################################################

# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.json

df.write.json(
    './output/people.json',
    mode='overwrite'
)

####################################################################################
# Read what we just wrote
####################################################################################

df2 = spark.read.json(
    './output/people.json',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP"
)

df2.show()

df2.printSchema()

spark.stop()
