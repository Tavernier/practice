#!/usr/bin/env python3

import pandas as pd

from pyspark import SparkConf
from pyspark.sql import SparkSession

from helpers import db_connections

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadBigQuery") \
    .getOrCreate()

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.jdbc

# the public shakespeare table is 6 MB and ~164,000 rows
# i cannot get this to work.  the BQ query runs correct per GCP logs, but i get errors in Spark: 
#   java.sql.SQLDataException: [Simba][JDBC](10140) Error converting value to long.
# these all fail:
#   (select 1 as ct)
#   `bigquery-public-data.samples.shakespeare`
# bigquery logs:
#  "SELECT * FROM `bigquery-public-data.samples.shakespeare` WHERE 1=0"
#   wtf, why the 1=0?
#   SELECT "word","word_count","corpus","corpus_date" FROM (select * from `bigquery-public-data.samples.shakespeare`)
#   wtf, that just returns those strings.
# i'm really confused and unsure what i'm doing wrong. the same approach works for other jdbc drivers.

# this works, but look at the result set.  it confirms the query gets completely screwed by the time it gets to BigQuery.
# df = spark.read \
    # .format("jdbc") \
    # .option("url", db_connections['bigquery']['url']) \
    # .option("driver", db_connections['bigquery']['properties']['driver']) \
    # .option("dbtable", '(select 1 as ct)') \
    # .option("customSchema", "ct STRING") \
    # .load()

# df.show()

# DataFrame actual output:
# +---+                                                                           
# | ct|
# +---+
# | ct|
# +---+

# DataFrame expected output:
# +---+                                                                           
# | ct|
# +---+
# |  1|
# +---+

# what the heck?
# oh, someone else saw this: https://stackoverflow.com/questions/47020379/bigquery-simba-jdbc-error-with-spark
# damn!

# df.printSchema()

# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession.createDataFrame
# approach 2: get data then transform to DataFrame
# this works, but I don't yet know enough about Spark to know limitations here
pd_df = pd.read_gbq(
    'select * from `bigquery-public-data.samples.shakespeare` limit 1000',
    project_id=db_connections['bigquery']['project_id'],
    dialect='standard',
    private_key=db_connections['bigquery']['private_key']
)

df2 = spark.createDataFrame(pd_df)

df2.show(10)

df2.printSchema()

spark.stop()
