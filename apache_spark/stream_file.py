#!/usr/bin/env python3

"""Spark Streaming SQL Example

Stolen from: https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html

I modified this to look at file input.

Steps:
1. Submit this spark job
2. cp ./input/sentences_01.csv ./input/stream
3. View output of Spark job in console
4. cp ./input/sentences_02.csv ./input/stream
5. View output of Spark job in console
"""

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split

spark = SparkSession \
    .builder \
    .appName("StreamFileExample") \
    .getOrCreate()

# Create DataFrame representing the stream of input lines from connection to localhost:9999
lines = spark \
    .readStream \
    .csv(
        "./input/stream",
        schema="value STRING"
    )

# Split the lines into words
words = lines.select(
   explode(
       split(lines.value, " ")
   ).alias("word")
)

# Generate running word count
wordCounts = words.groupBy("word").count()

print("wordCounts type: ", type(wordCounts))

 # Start running the query that prints the running counts to the console
query = wordCounts \
    .writeStream \
    .outputMode("complete") \
    .format("console") \
    .start()

print("query type: ", type(wordCounts))

query.awaitTermination()
