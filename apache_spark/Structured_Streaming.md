# Structured Streaming

After going through the basic Spark tutorial and the Spark SQL tutorial, I next moved on to the [Structured Streaming tutorial](https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html).

## Hello World

I followed the guide to:

1. Start a netcat data server on localhost 9999
    - `nc -lk 9999`
2. Submit a Spark SQL Streaming job that listens to this port
    - See `./stream_example.py`
3. Type sentences into the netcat session
4. Inspect the console output of the running Spark job

## Concepts

Let's dive into the tutorial and concepts.

> The key idea in Structured Streaming is to treat a live data stream as a table that is being continuously appended. You will express your streaming computation as standard batch-like query as on a static table, and Spark runs it as an incremental query on the unbounded input table. 

* Input = Unbounded table of rows.
* Query = A query on the input that will produce a Result Table.
* Output = What gets written to external storage, can be one of three modes:
    1. Complete Mode
        - Entire Result Table is written.
    2. Append Mode
        - Only new rows appended in Result Table will be written.
        - Only suitable where previous Result Table rows will remain unchanged.
    3. Update Mode
        - Only updated rows in the Result Table will be written.

## Hello World Code

Let's look at `./stream_example.py` starting after the imports.

Nothing new here.  We still establish a SparkSession just like we do in "normal" jobs.

```python
spark = SparkSession \
    .builder \
    .appName("StructuredNetworkWordCount") \
    .getOrCreate()
```

> This lines DataFrame represents an unbounded table containing the streaming text data. This table contains one column of strings named “value”, and each line in the streaming text data becomes a row in the table. Note, that this is not currently receiving any data as we are just setting up the transformation, and have not yet started it. Next, we have used two built-in SQL functions - split and explode, to split each line into multiple rows with a word each. In addition, we use the function alias to name the new column as “word”. Finally, we have defined the wordCounts DataFrame by grouping by the unique values in the Dataset and counting them. Note that this is a streaming DataFrame which represents the running word counts of the stream.

More information:

- The `readStream` method returns a [DataStreamReader](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession.readStream) object, [doc ref](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession.readStream).
- The DataStreamReader's `format` method specifies the input data source format, [doc ref](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.streaming.DataStreamReader.format).
- The DataStreamReader's `option` method "adds an input option for the underlying data source."
- The DataStreamReader's `load` method loads the data and returns a DataFrame.
- The `lines.select` is a [DataFrame](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrame) method that returns another DataFrame, which I've covered elsewhere so let's skp that.
- `words.groupBy("word").count()` also returns a DataFrame.

```python
# Create DataFrame representing the stream of input lines from connection to localhost:9999
lines = spark \
    .readStream \
    .format("socket") \
    .option("host", "localhost") \
    .option("port", 9999) \
    .load()

# Split the lines into words
words = lines.select(
   explode(
       split(lines.value, " ")
   ).alias("word")
)

# Generate running word count
wordCounts = words.groupBy("word").count()
```

> All that is left is to actually start receiving data and computing the counts. To do this, we set it up to print the complete set of counts (specified by outputMode("complete")) to the console every time they are updated. And then start the streaming computation using start().

More information:

- `wordCounts` is a DataFrame.
- `writeStream` returns a [DataStreamWriter](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.streaming.DataStreamWriter)
- `outputMode` sets "how data of a streaming DataFrame/Dataset is written to a streaming sink."  Supported modes are complete, append, or update.
- `format` sets the "underlying output data source."
- `start()` "streams the contents of the DataFrame to a data source. The data source is specified by the format and a set of options. "
- `query` is also a Spark DataFrame, but awaitTermination is defined on the [StreamingQuery class](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.streaming.StreamingQuery).
- `awaitTermination()` will wait either for `query.stop()` or an exception (e.g. KeyboardInterrupt), [doc ref](https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.streaming.StreamingQuery.awaitTermination).

```python
 # Start running the query that prints the running counts to the console
query = wordCounts \
    .writeStream \
    .outputMode("complete") \
    .format("console") \
    .start()

# quit with ctrl+c
query.awaitTermination()
```