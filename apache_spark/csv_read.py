#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, TimestampType


# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ReadCSV") \
    .getOrCreate()

# doc: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameReader.csv

# this file has a schema:
#   name: string
#   birth_year: int
#   age: float
#   last_seen: datetime

# let's read and specify a schema.
# i'm using the DDL string variety here.
# could also pass a StructType as seen above, which requires defining all attributes.
df = spark.read.csv(
    './input/people.csv',
    schema="name STRING, birth_year INT, age DOUBLE, last_seen TIMESTAMP",
    header=True
)

df.show()

df.printSchema()

spark.stop()
