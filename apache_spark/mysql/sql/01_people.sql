create schema jt_test;

use jt_test;

drop table if exists jt_people;

create table jt_people (
    name text,
    birth_year int,
    age float,
    last_seen timestamp
);

insert into jt_people(name, birth_year, age, last_seen)
values 
	('Jon', 1980, 38.5, '2018-07-19 14:00:00'),
	('Garrett', 1980, 38.3, '2018-07-10 14:00:00');

select *
from jt_people;
