#! /usr/bin/env python3
"""WebSocket Example

Code stolen from: https://websockets.readthedocs.io/en/3.0/intro.html
"""

import logging
import os
import time

import asyncio
import websockets

CONNECTED_CLIENTS = set()

async def hello(websocket, path):
    CONNECTED_CLIENTS.add(websocket)
    logging.info(f"connected clients {len(CONNECTED_CLIENTS)}")
    while True:
        message = await websocket.recv()
        logging.info(f"received message: {message}")
        for client in CONNECTED_CLIENTS:
            await client.send(message)
        logging.info(f"sent message: {message}")
        time.sleep(.1)


if __name__ == "__main__":
    logging.basicConfig(level="INFO")
    start_server = websockets.serve(hello, '0.0.0.0', int(os.getenv("PORT", 8085)))
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
