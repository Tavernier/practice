# Terraform

> Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

## Basics

- Define infrastructure as code.
- Create an Execution Plan, which shows what Terraform will do.
- [Terraform Configuration](https://www.terraform.io/docs/configuration/index.html): A set of files used to describe infrastructure.
    - Terraform loads all files ending in `.tf` in a directory.  Files are loaded in alphabetical order.  Files are appended together not merged!  Override files can be used to merge though.
    - [Functions and whatnot](https://www.terraform.io/docs/configuration/interpolation.html) are supported within the configuration files.
    - The syntax for defining a resource is `resource "resource_type" "your_resource_name" {...}`.
    - Dependencies are supported. Both implicit and explicit.
        - Implicit: Refer to one resource in another.
        - Explicit: Use `depends_on` to list out required resources.
    - Provisioners can be defined and run on resource creation or destroy.
        - Not a replacement for configuration management.
    - Variables are supported.
        - [Input](https://www.terraform.io/intro/getting-started/variables.html) (e.g. access keys, aws region) variables exist to support parameterizing your configurations.
        - Variables can be set on the command line, from a file, via environment variables (strings only), via default values, etc.
        - Output variables are supported too.
            - These values are meant for human consumption.
            - "Output variables are a way to tell Terraform what data is important. This data is outputted when apply is called, and can be queried using the terraform output command."
    - Modules can be defined too.
        - Terraform has a [registry with ready-to-use modules](https://registry.terraform.io/).
        - "Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. Modules are used to create reusable components, improve organization, and to treat pieces of infrastructure as a black box."
        - I'll revisit this later after I better understand the basics.
- Terrform keeps track of state in a file.  This file is critical.
    - This state file can be kept remotely, in S3 for example, too.
- [Terraform Providers](https://www.terraform.io/docs/providers/index.html) manage resource creation, destruction, etc.
    - Lots of providers are available such as [AWS](https://www.terraform.io/docs/providers/aws/index.html), [GCP](https://www.terraform.io/docs/providers/google/index.html), etc.

## Workflow

1. Create or modify your [Terraform Configuration](https://www.terraform.io/docs/configuration/index.html).
1. Run `terraform init`.
    - This "initializes various local settings and data that will be used by subsequent commands."
    - If we are managing AWS resources, for example, Terraform will download its AWS plugin here.
1. Run `terraform apply` to inspect changes Terraform will make.
    - `+` means Terraform will create the resource.
    - `-/+` means Terraform will destroy and recreate the resource.  It's not an update!
    - `~` means Terraform will update the resource.
    - `-` means Terraform will destroy the resource.
1. Accept / Deny `terraform apply` prompt to move forward with changes.
1. Use `terraform destroy` when you want to nuke everything.

The [Terraform CLI](https://www.terraform.io/docs/commands/index.html) offers many commands.

## Docker

Hashicorp publishes a light [Docker Image](https://hub.docker.com/r/hashicorp/terraform/).  This includes just the terraform binary, which is good enough.  The full version is overkill for my purposes.

```bash
# hello world
docker run hashicorp/terraform:light version

# mount this dir in container and play around with terraform
docker run \
--interactive \
--mount type=bind,src=$(pwd),dst=/app \
--tty \
--workdir /app/ \
--entrypoint sh \
hashicorp/terraform:light
```

