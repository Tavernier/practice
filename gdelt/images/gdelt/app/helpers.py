#! /usr/bin/env python3

import json

class Schema(object):
    def __init__(self, schema_filename):
        self.schema_filename = schema_filename
        self.schema = self._load_schema_file()

    @property
    def field_names(self):
        """Return a list of the field names only."""
        return [x['name'] for x in self.schema]

    @property
    def panda_data_types(self):
        return self._get_data_types('pandas_data_type')

    def _get_data_types(self, data_type):
        return [x['data_type'] for x in self.schema if x.get(data_type, None)]

    def _load_schema_file(self):
        contents = None
        with open(self.schema_filename, 'r') as f:
            contents = json.load(f)
        return contents
