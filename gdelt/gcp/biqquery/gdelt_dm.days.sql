select
    -- key
    sqldate, 

    -- values
    count(globaleventid) as total_events,
    approx_count_distinct(eventcode) as distinct_events,

    sum(nummentions) as total_mentions,
    sum(numsources) as total_sources,
    sum(numarticles) as total_articles,
    avg(goldsteinscale) as goldsteinscale_avg,

    approx_count_distinct(actor1code) as actor1_occurrences,
    approx_count_distinct(actor2code) as actor2_occurrences,

    'gdelt_dm' as data_source_database,
    'events' as data_source_table
from gdelt_dm.events
group by sqldate
;
