#! /usr/bin/env bash

set -euxo pipefail

rebuild_date_partitioned_table(){
    table_to_create=$1
    partition_field=$2

    sql_filename="./${table_to_create}.sql"

    echo "Building ${table_to_create} with ${sql_filename}"

    # table builds in BigQuery are atomic and non-blocking.
    # provided your result set has no breaking schema changes
    # and does not goof up the data, this is safe to run while
    # others read from the table.
    # max_rows = 0 just means return zero rows to this client.
    cat ${sql_filename} | bq --headless query \
        --nouse_legacy_sql \
        --max_rows=0 \
        --replace \
        --destination_table="${table_to_create}" \
        --time_partitioning_field=${partition_field} \
        --time_partitioning_type DAY
}

rebuild_table(){
    table_to_create=$1

    sql_filename="./${table_to_create}.sql"

    echo "Building ${table_to_create} with ${sql_filename}"

    # table builds in BigQuery are atomic and non-blocking.
    # provided your result set has no breaking schema changes
    # and does not goof up the data, this is safe to run while
    # others read from the table.
    # max_rows = 0 just means return zero rows to this client.
    cat ${sql_filename} | bq --headless query \
        --nouse_legacy_sql \
        --max_rows=0 \
        --replace \
        --destination_table="${table_to_create}"
}

main(){
    rebuild_date_partitioned_table gdelt_dm.events SQLDATE_PARTITION
    rebuild_table gdelt_dm.days
    rebuild_table gdelt_dm.daily_top_events
    rebuild_table gdelt_dm.actors
    rebuild_table gdelt_dm.event_codes
    rebuild_table gdelt_dm.hosts
}

main
