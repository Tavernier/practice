select 
    -- key
    eventcode, 
    -- values
    count(*) as total_events,
    avg(goldsteinscale) as goldsteinscale_avg,
    sum(nummentions) as total_mentions,
    sum(numsources) as total_sources,
    sum(numarticles) as total_articles,
    min(sqldate) as earliest_event,
    max(sqldate) as latest_event,
      -- show our work
  'gdelt_dm' as data_source_database,
  'events' as data_source_table
from gdelt_dm.events 
group by
    eventcode
;
