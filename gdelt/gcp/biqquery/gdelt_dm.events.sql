select
  date_trunc(safe.parse_date('%Y%m%d', cast(sqldate as string)), month) as SQLDATE_PARTITION,
  safe.parse_date('%Y%m%d', cast(sqldate as string)) as SQLDATE,
  safe.parse_date('%Y%m%d', cast(DATEADDED as string)) as DATEADDED,
  st_geogpoint(Actor1Geo_Long, Actor1Geo_Lat) as Actor1Geo,
  st_geogpoint(Actor2Geo_Long, Actor2Geo_Lat) as Actor2Geo,
  st_geogpoint(ActionGeo_Long, ActionGeo_Lat) as ActionGeo,
  * except(SQLDATE, DATEADDED)
from `gdelt-bq.full.events`
;
