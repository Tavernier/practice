select
    --key
    sqldate,
    eventcode,

    -- values
    event_rank,
    total_events,
    distinct_actor1s,
    distinct_actor2s,

    -- show our work
    'gdelt_dm' as data_source_database,
    'events' as data_source_table
from (
    select
        -- key
        sqldate,
        eventcode,
        row_number() over(
            partition by sqldate
            order by total_events desc
        ) as event_rank,

        total_events,
        distinct_actor1s,
        distinct_actor2s
    from (
        select
            -- key
            sqldate,
            eventcode,
            
            -- values
            count(globaleventid) as total_events,
            approx_count_distinct(actor1code) as distinct_actor1s,
            approx_count_distinct(actor2code) as distinct_actor2s
        from gdelt_dm.events
        group by
            sqldate,
            eventcode
    ) d
) fd
where fd.event_rank <= 5
;
