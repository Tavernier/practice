# Working with a Largish Data Set

My goal is to work with a largish data set in a variety of different technologies for analytical purposes such as Athena, BigQuery, Spark, etc.  I want to turn a largish, slow data set into a more performant data set.

The "GDELT database is 100% free and open."  GDELT = Global Database of Events, Language and Tone.  As of January 2019, it's about 180 GB uncompressed and stored in tab separated files.

## Background Information

### Metadata

- CSV files are available on S3: `s3://gdelt-open-data/events/`
- As of today, January 11th, 2019:
    - Total Objects: 2221
    - Total Size: 177.8 Gi
    - via: `aws s3 ls --human-readable --summarize s3://gdelt-open-data/events/`
- Each file is about 70 MB.
- Files do not have a header.
- File delimiter is the tab character.
- GDELT publishes a guide on the [file format](http://data.gdeltproject.org/documentation/GDELT-Data_Format_Codebook.pdf).


### Summary

Shown below is a Conceptual ERD and the Architecture I played with.  Mind the notes.  I'm using BigQuery maybe as an equivalent of Snowflake to show the capabilities of that type of technology.  Also, the same transformed data exists many times because I wanted to play around with transforming the data using a variety of tools (Apache Spark via EMR, Athena CTAS Queries, BigQuery Queries, etc.).

Anywho, here's the visual overview, which I created in [draw.io](https://draw.io) (see `./docs/draw_io_diagram.xml`):

![Summary](./docs/summary.png "Summary")



### Sample

The `./source_data/get_sample.sh` script will download one file to `/tmp/gdelt/sample` from S3.

### Attributes

I stored the field names and their source data type in `./source_data/schema.json`.  I will add technology specific data types when needed.

## Target Datasets

We have the source data.  I want to make that source data available in a performant way.  I also want to create some curated data sets such as aggregate information, top N per day, etc.

I plan to create these data sets:

- events: Copy of the raw data, but stored in Parquet and partitioned for performance.
- daily_stats: For each day, aggregate some information such as total events, etc.
- daily_events: Show information about the top 5 events per day.
- actors: Each actor1code will have a single row in this table.  Aggregate information will appear.  Let's also try to show the top 3 actor2s associated with the actor1 in question.

## Technology Explorations

In no particular order...

### AWS

#### Athena

Athena is Presto as a service.  I went through the process of creating source and transformed data.  I found Athena to be performant and a great tool for processing large data sets.

##### Creating Data

1. Create the databases.
    - `gdelt_ods`: holds raw source data
    - `gdelt_dm`: holds transformed data sets
1. Create ODS events table.
    - `gdelt_ods.events`
        - Surfaces raw GDELT events.
        - Each query scans 177 GB.
        - I'm suprised by this since I thought Athena would take advantage of S3 Select here and only scan the needed bytes.
1. Create DM events table.
    - `gdelt_dm.events`
        - Created via CTAS to build a Parquet table partitioned by year.
        - CTAS tables are limited to 100 partitions so I could not partition by day here.
        - Query took 9 minutes and processed 177 GB.  ~330 MB per second of processing.
        - The output Parquet files weight in at 26 GB.
        - `aws s3 ls --summarize --human-readable --recursive s3://data.jontav.com/athena/dm/gdelt/events/`
1. Create "easy" DM tables.
    - Now that we have a partitioned, column store table, let's create some additional aggregated data sets.
    - `gdelt_dm.days`
        - A few attributes are aggregated by day.
        - The result is a tiny table that performs well should we have the need for daily stats.
    - `gdelt_dm.daily_top_events`
        - Shows the Top 5 Events per day and some basic information.
        - Query took 14 seconds to run over all data and scanned 1.48 GB.
        - The resulting table is tiny that performs well.
    - `gdelt_dm.event_codes`
        - One row per eventcode.
    - `gdelt_dm.hosts`
        - A few attributes are aggregated by sourceurl host.
1. Create "harder" DM tables.
    - `gdelt_dm.actors`
        - Key: `actor_code`
        - For each actor, let's show total events, actor1 event totalss, actor2 event totals, total recent events, and bundle in the top 3 Actor 2s based on the number of events between Actor 1 and Actor 2.
        - The result again is a tiny, performant table.

##### Documentation

- [Functions and Operators](https://docs.aws.amazon.com/athena/latest/ug/functions-operators-reference-section.html)
- [DDL](https://docs.aws.amazon.com/athena/latest/ug/language-reference.html)
    
##### More Learning

- How are tables replaced?  Let's say I have a DM.  Can I overwrite it with the results of a new table?
    - The `alter table set location` may work.
    - Same with switching partitions.
- How can I drop and recreate a CTAS table?
    - I dropped the table in SQL and attempted to recreate it via CTAS.  Athena complained that the S3 location was already taken.  I manually wiped that out.
    - Using `purge` in `drop table` did not do it either.

#### EMR: Notebooks

[EMR Notebooks](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-managed-notebooks.html) is Jupyter as a service.  "An Amazon EMR cluster is required to execute the code and queries within an EMR notebook."  Amazon introduced this in November 2018.  Let's take it for a spin.

I followed [Amazon's guide](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-managed-notebooks-service-role.html) on creating the necessary policies and role.

Here's what I did next...

1. Launched a small EMR Cluster with Spark.
    - Wait for it to launch and show a running state.
1. Created a Notebook and attached it to this running cluster.
    - Default Security Groups
    - EMR_Notebooks_DefaultRole
    - S3 Location for notebooks: `s3://data.jontav.com/emr_notebooks/`
1. Waited for the Notebook to attached to the cluster.
    - It was availble in "local" mode before then.
    - It took all of 20 seconds or so for this Notebook to launch and attach to the running cluster.
1. Clicked Open
    - Bingo, I'm in a running Notebook.

From within my test notebook, I was able to query data sets in S3 with minimal effort.  I saved the [output of my test notebook in html](./aws/notebooks/JonNotebook.html) (no option for markdown).

#### EMR: Presto

Athena is Presto-as-a-service.  So, I'm going to lower the priority of exploring Presto on EMR.  Athena does have some limitations both with concurrency and Presto functionality, which may be cause for exploring Presto itself.

#### EMR: Spark

EMR Spark is spark-as-a-service.  How anyone ever figures out the syntax for working with Hadoop-like tools is beyond me.  I encountered a few challenges too:

- File Not Found errors on `/stderr` and `/stdout`.
    - This appears to be a [known problem](https://forums.aws.amazon.com/thread.jspa?messageID=878824&tstart=0) and fixed in 5.21.0, which I do not see yet as an option.
    - [StackOverflow](https://stackoverflow.com/questions/53140852/cant-get-a-sparkcontext-in-new-aws-emr-cluster) has some info too.
    - Turns out, the job still runs as expected though.
- A cluster seemed to have bombed out completely.
    - Error: `A job flow that is shutting down, terminated, or finished may not be modified.`.
    - The cluser shows `Cluster ready after last step completed.`.
    - So, everything looks okay.  I must be missing a layer.  I nuked the cluster and created a new one.
- Slow
    - I created a 5.20.0 cluster with 4 m3.xlarge instances.
    - I need to learn how to optimize Spark I guess.
    - Step Settings
        - num-executors: 15
        - executor-cores: 2
        - executor-memory: 1g
    - I guess I need to learn how to optimize Spark settings now.  Barf.
    - The "application" has been running for 20 minutes so far and is nowhere near done.  The Spark app reads the GDELT S3 files and writes it back out as Parquet files partitioned by year.
    - 80% of the data has yet to be processed.
    - So, 20 minutes processed 31 GB = 26.88 MB per second. I managed to standup and use all this fancy technology to get speeds slower than my laptop.  I need to learn how to optimize this.

I could have imported the CSVs into Google BigQuery and been working with the data within minutes.

Let's work on the slowness. [This post](https://spoddutur.github.io/spark-notes/distribution_of_executors_cores_and_memory_for_spark_application.html) seems like a nice guide.  [Cloudera has a nice post too](https://blog.cloudera.com/blog/2015/03/how-to-tune-your-apache-spark-jobs-part-2/).

* Example Cluster
    - 10 Nodes
    - 16 Cores per Node
    - 64GB RAM per Node
* Tiny Executors = 1 executor per core.
    - num-executors = nodes * cores per node = 160
    - executor-cores = 1
    - executor-memory = 64GB / 16 executors on the node = 4 GB
    - This is a bad approach per that article.
* Large Executors
    - num-executors = 10 = one per node and we have 10 nodes
    - executor-cores = 16 = all cores of a node are assigned to its one executor
    - executor-memory = 64GB / 1 executor on the node = 64 GB
    - This is also a bad approach per the article.
* Balanced Approach
    - executor-cores = 5
        - It's unclear to me how this was picked.
    - num-executors = 29
        - 16 cores per node - 1 core for other stuff = 15 cores left per node.
        - 15 cores on a node * 10 nodes = 150 cores available.
        - 150 cores / 5 executor cores = 30 executors.
        - 30 executors - 1 for ApplicationManager = 29
    - executor-memory = 18 GB
        - 30 executors per node / 10 nodes = 3.
        - 64 GB / 3 = 21 GB - 7% for heap overhead = 18 GB.

Let's try this again.

* My Cluster
    - 3 nodes of `m4.2xlarge`
    - 8 cores per node
    - 32 GB per node
- My Approach
    - executor-cores = 4
    - num-executors = 4
        - 8 core nodes - 1 for other stuff = 7 cores left per node.
        - 7 cores on a node * 3 nodes = 21 cores available.
        - 21 cores / 4 executor cores = 5 executors.
        - 5 executors - 1 for ApplicationManager = 4.
    - executor-memory = 10 GB
        - 5 executors per node / 3 nodes = 1.6
        - 18 GB / 1.6 = 11 GB - 7% = 10 GB
- Results
    - 177 GB gdelt files processed into Parquet.
        - Total Objects: 5999
        - Total Size: 34.9 GiB
    - 32 minutes so 94 MB per second processed.
    - That's a lot better, but still pretty slow.  How come the throughput is so bad here?
    - What's a bummer is the Parquet files are so small.  Each file is about 20 MB.  The recommended size for Athena and other analytical tools is 256 MB to 1024 MB.  [Amazon has a note on this.](https://aws.amazon.com/premiumsupport/knowledge-center/emr-concatenate-parquet-files/)

##### SQL Against Parquet

Now that I have the data in a columnar Paruqet format, I ran a couple Spark SQL queries against them.

```sql
-- see ./aws/spark/jobs/aggregate_dm.py
-- Application ran for 1.8 minutes.
-- Certainly not suitable for interactive queries, but pretty good.
-- S3 Total Objects: 201
-- S3 Total Size: 293.0 KiB
select eventcode, year, count(*) as ct 
from events 
group by eventcode, year
```
   

```sql
-- see ./aws/spark/jobs/top_events.py
-- Application ran for 1.9 minutes.
-- Certainly not suitable for interactive queries, but pretty good.
-- S3 Total Objects: 201
-- S3 Total Size: 344.7 KiB
select day, eventcode, event_rank
from (
    select
        eventcode, day, ct,
        row_number() over(partition by day order by ct desc) as event_rank
    from (
        select eventcode, day, count(*) as ct
        from events
        group by eventcode, day
    ) d
) a
where event_rank <= 5
```

##### Larger Parquet File Sizes

Athena performs great with larger Parquet files.  The problem is pyspark seems to write very small Parquet files.  Let's try to get larger files written to S3.

I started with [this StackOverflow](https://stackoverflow.com/questions/44808415/spark-parquet-partitioning-large-number-of-files) post then branched out from there.  I was able to get one large file as seen in `./aws/spark/jobs/larger_parquet.py`.

#### Glue

Glue is ETL as a service.  So far, I'm not liking it so I'm going to stop investigating it for now and look at other technologies. AWS seems to be pushing this hard though (e.g. its integration with Athena).

First, I'll kick the tires manually then automate what I can:

- I created a Crawler and pointed it to `s3://gdelt-open-data/events/`
- The Crawler correctly identified the files and number of columns.
- It created a table with generic column names, which are not present in the source data.
- I added column names.  I changed `IsRootEvent` to `boolean` from `integer`.
- At a quick glance, I see little support for Glue in Terraform at this time.  Glue is new however.

I already dislike the development environment:

- Python 2.7 only.
- Must be compatible with Apache Spark 2.2.1.
- Libraries that rely on C extensions, such as the pandas Python Data Analysis Library, are not yet supported.

I would rather build a Docker Image and use AWS Batch to have full control over what want to do. Local testing is easier too.  Also, Athena can write result sets as Parquet.

#### QuickSight

QuickSight is BI as a service that [connects to external data](https://docs.aws.amazon.com/quicksight/latest/user/supported-data-sources.html).  I signed up my AWS account for QuickSight creating the account name `jonquicksightpoc` and authorized access to Athena (see Athena section above). 

##### Analysis

First, I want to immediately analyze my data in Athena. 

I created a new Data Source.  QuickSight lets me choose an Athena table or even write a custom query.  I picked the `gdelt_dm.daily_events` data set.  QuickSight asked me whether to import the data to SPICE, I have 1 GB available due to signing up for the Personal vs. Enterprise plan, or directly query Athena.  Sure, let's import to SPICE.  All 73,105 imported.

Side note: My initial import failed.  I granted QuickSight access to Athena.  I had to go back and grant QuickSight access to my S3 bucket as well.

I created a visual to show even code counts per day.  Warning: I am not an analyst or dataviz expert.  I just wanted to play around with the data.

I then added `gdelt_dm.events` as a Direct Query.  I also added 2017 and 2018 into SPICE.  I attempted to import all of GDELT, but the query timed out.

In summary...

- Imported to Spice
    - `gdelt_dm.actors`
    - `gdelt_dm.daily_top_events`
    - `gdelt_dm.days`
    - `gdelt_dm.event_codes`
    - `gdelt_dm.hosts`
- Direct Query
    - `gdelt_dm.events`

##### SPICE

QuickSight's SPICE storage engine. Limited to 25 GB. "You can improve the performance of database data sets by importing the data into SPICE instead of using a direct query to the database. All data sets that aren't based on database data sources must use SPICE."

So, let's see how SPICE works with Athena.  I get only 1GB since I signed up for a personal account.

#### S3

- Source Data: `s3://gdelt-open-data/events/`
- Transformed Data: `s3://data.jontav.com/dm/gdelt/events`
    - I'll convert the source data to Parquet.  Parquet files will be about 10% of the original file.
    - Each source file is relatively small (under 100 MB).
    - A stretch goal is compacting those into larger Parquet files, say 500 MB.
    - Data will be partitioned by day (e.g. `s3://data.jontav.com/dm/gdelt/events/Day=2017-04-12`).

#### S3 Select

I used [S3 Select within EMR Notebooks](https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-spark-s3select.html) and believe it worked as advertised.  See its usage in [my example notebook](./aws/notebooks/JoneNotebook.html).

The source data was 364 S3 objects weighing in at 22.2 GB.  I believe S3 Select worked as advertised based on the query response I saw.

### GCP

#### BigQuery

Google makes the full GDELT data set available in BigQuery.  So, I will skip the cloud-to-cloud copy via [`gsutil cp`](https://cloud.google.com/storage/docs/gsutil/commands/cp) and data load via [`bq load`](https://cloud.google.com/bigquery/docs/bq-command-line-tool#loading_data).

GDELT data is available in the [`gdelt-bq:full.events`](https://bigquery.cloud.google.com/table/gdelt-bq:full.events?tab=details) table.  The table holds 555,125,381 rows and is 206 GB.  They also build a partitioned table, but let's pretend that does not exist.  The exercise here is to load GDELT data into BigQuery and make it more usable.

The `gdelt-bq:full.events` source table is:

- Unpartitioned
- Stores days as integers
- etc.

So, lets...

- Create a copy of this table that is day partitioned and has the proper data types.
    - Partition by month so if the `SQLDATE` value is 2018-10-22 it gets put in the 2018-10-01 partition.  Folks would need to know this when querying the table.
- Build data marts off of that.
    - I will build the same data mart tables I build in AWS.

The code in [`./gcp/bigquery`](./gcp/bigquery) builds this data mart tables.  Run `./gcp/bigquery/build_dms.sh` to build the data marts.  This assumes you have the gcloud SDK installed, a service account authenticated, etc..

Here's the output of the build script (I added line breaks and removed debug lines):

```bash
Building gdelt_dm.events with ./gdelt_dm.events.sql
Waiting on bqjob_r19a7fa859f9d0863_0000016856f24db1_1 ... (532s) Current status: DONE

Building gdelt_dm.days with ./gdelt_dm.days.sql
Waiting on bqjob_r6d171b11aff193a4_0000016856fa7c90_1 ... (3s) Current status: DONE

Building gdelt_dm.daily_top_events with ./gdelt_dm.daily_top_events.sql
Waiting on bqjob_r28ffe76ff617183e_0000016856fa997b_1 ... (4s) Current status: DONE

Building gdelt_dm.actors with ./gdelt_dm.actors.sql
Waiting on bqjob_r780159966614faf_0000016856fabaca_1 ... (4s) Current status: DONE

Building gdelt_dm.event_codes with ./gdelt_dm.event_codes.sql
Waiting on bqjob_rb26d94c1b702790_0000016856fadc3a_1 ... (2s) Current status: DONE

Building gdelt_dm.hosts with ./gdelt_dm.hosts.sql
Waiting on bqjob_r1929285686bc4b1d_0000016856faf3c7_1 ... (4s) Current status: DONE
```

Here's a summary of what was built:

|Table|Seconds to Run|MB Processed|MB per Second|Cost|
|---|---|---|---|---|
|events|532|211,289|397|$1.01|
|days|3|33,018|11,006|$0.16|
|daily_top_events|4|16,071|4,018|$0.08|
|actors|4|17,640|4,410|$0.08|
|event_codes|3|23,851|7,950|$0.11|
|hosts|4|55,607|13,902|$0.27|

BigQuery is the best analytial data store on the planet.

## Data Studio

> Unlock the power of your data with interactive dashboards and beautiful reports that inspire smarter business decisions. It's easy and free.

I like the sound of that.  I granted Data Studio access to my BigQuery project.

Challenges

- BigQuery geo points were not recognized.  I had to create a text field in Data Studio which was "lat,long".
- Data Studio does not understand date partitioned tables.  It thinks `gdelt_dm.events` is ingestion-time partitioned.
- When running a report, it seems Data Studio waits for all components to render before showing any components.  So, an individual chart's query may complete in two seconds, but it will not appear until the slowest query on another chart has finished.

I created an "Exploring GDELT Events" report.  I prefer this interface over AWS's QuickSight.  Data Studio made it easy to add...

- Charts.
- Filter everything by the same date range.
    - The filter is pushed down to BigQuery. I can see the date partitioned being used.
- Edit vs. View


## Adhoc Queries

Now that we have equivalent data sets in both BigQuery and Athena, let's run some adhoc queries and see how they perform.

I found both technologies to be performant.  Both returned results in about the same time.

### What are the most important events as defined by the avgtone value?

```sql
-- Athena SQL
-- 3.64 seconds, Data scanned: 1.45 GB
select
  day,
  avg(avgtone) as avgtone_score
from gdelt_dm.events
where day < date('2019-01-01')
group by
  day
order by
  avgtone_score desc
limit 10;

-- BigQuery Standard SQL
-- 3.3s elapsed, 12.4 GB processed
select
  sqldate,
  avg(avgtone) as avgtone_score
from `gdelt_dm.events` 
where SQLDATE_PARTITION < '2019-01-01'
group by
  sqldate
order by
  avgtone_score desc
limit 10;
```

### How does an actor's mentions change over time in February and March 2018?

```sql
-- Athena SQL
-- 18.84 seconds, Data scanned: 167.09 MB
select
  Actor1Code,
  day,
  NumMentions,  
  try(
    (NumMentions - lag(NumMentions) over(partition by Actor1Code order by day, GLOBALEVENTID))
    /
    cast(lag(NumMentions) over(partition by Actor1Code order by day, GLOBALEVENTID) as double)
  ) as NumMentions_change
from gdelt_dm.events
where day between date('2018-02-01') and date('2018-03-01')
and year = 2018
and Actor1Code is not null
order by 
  globaleventid;

-- BigQuery Standard SQL
-- 21.1s elapsed, 364 MB processed
select
  Actor1Code,
  SQLDATE,
  NumMentions,  
  safe_divide(
    NumMentions - lag(NumMentions) over(partition by Actor1Code order by SQLDATE, GLOBALEVENTID), 
    lag(NumMentions) over(partition by Actor1Code order by SQLDATE, GLOBALEVENTID)
  ) as NumMentions_change
from `gdelt_dm.events` 
where SQLDATE_PARTITION in ('2018-02-01', '2018-03-01')
and Actor1Code is not null
order by 
  globaleventid;
```


### What actors have the highest score (minimum two events)?

These queries run against our `actors` DM table, which aggregates information for each actor.

```sql
-- Athena SQL
-- Run time: 1.63 seconds, Data scanned: 409.84 KB
select 
    actor_code,
    actor1_goldsteinscale_avg,
    actor1_latest_event,
    actor1_total_events
from gdelt_dm.actors
where actor1_total_events > 2
order by
    actor1_goldsteinscale_avg desc
limit 25;

-- BigQuery Standard SQL
-- 0.9s elapsed, 1.01 MB processed
select 
    actor_code,
    actor1_goldsteinscale_avg,
    actor1_latest_event,
    actor1_total_events
from gdelt_dm.actors
where actor1_total_events > 2
order by
    actor1_goldsteinscale_avg desc
limit 25;
```


### What actors found in December 2018 are also found in December 2017?

I am purposely performing a suboptimal query here by adding a function to the day attribute.

```sql
-- Athena SQL
-- 5.99 seconds, Data scanned: 556.65 MB
select
  distinct actor1code
from gdelt_dm.events
where date_trunc('month', day) = date('2018-12-01')
and actor1code is not null

intersect distinct

select
  distinct actor1code
from gdelt_dm.events
where date_trunc('month', day) = date('2017-12-01')
and actor1code is not null
;


-- BigQuery SQL
-- 2.6s elapsed, 6.78 GB processed
select
  distinct actor1code
from `gdelt_dm.events` 
where date_trunc(sqldate, month) = '2018-12-01'
and actor1code is not null

intersect distinct

select
  distinct actor1code
from `gdelt_dm.events` 
where date_trunc(sqldate, month) = '2017-12-01'
and actor1code is not null
;
```

### What are some details for just a few actors?

I avoided entire table scans here (i.e. select *) and picked just a few columns.  Neither data set is sorted or bucketed by Actor1Code.  These runtimes impress me.

I avoided sorting here and assumed that would occur on the client.

```sql
-- Athena SQL
-- Run time: 4.51 seconds, Data scanned: 157.21 MB
select
   globaleventid,
   day,
   actor1code,
   actor2code,
   eventcode,
   avgtone,
   goldsteinscale,
   actiongeo_lat,
   actiongeo_long
from gdelt_dm.events
where actor1code in (
  'AUTGOVMILELI',
  'IGOGMB',
  'UISCHRPRO'
)
;

-- BigQuery Standard 
-- 2.9s elapsed, 32.1 GB processed
select
   globaleventid,
   sqldate,
   actor1code,
   actor2code,
   eventcode,
   avgtone,
   goldsteinscale,
   actiongeo_lat,
   actiongeo_long
from gdelt_dm.events
where actor1code in (
  'AUTGOVMILELI',
  'IGOGMB',
  'UISCHRPRO'
)
;
```

### By year, how many events occurred within 30 miles of Chicago?

I think I'm using the geo functions correctly here.

```sql
-- Athena SQL
-- NA. I don't think Athena a geography function I can use.
-- I need to do some more research.  Presto documents
-- great_circle_distance returning the kilometers between two
-- points.  This function is unavailable in Athena.
-- More here: https://docs.aws.amazon.com/athena/latest/ug/geospatial-functions-list.html
select
  extract(year from day) as year,
  count(*) as ct
from gdelt_dm.events
where day < date('2019-01-01')
and great_circle_distance(
  actiongeo_lat, actiongeo_long,
  41.881832, -87.623177
) / 1000 < 48280 -- in meters, which is 30 miles.
group by
  extract(year from day)
order by
  year desc
;

-- BigQuery Standard SQL
-- 10.6s elapsed, 12.3 GB processed
select
  extract(year from sqldate) as year,
  count(*) as ct
from gdelt_dm.events
where sqldate < '2019-01-01'
and ST_DISTANCE(
  ST_GEOGPOINT(actiongeo_long, actiongeo_lat),
  ST_GEOGPOINT(-87.623177, 41.881832)
) < 48280 -- in meters, which is 30 miles.
group by
  year
order by
  year desc
;
```