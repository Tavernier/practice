from pyspark.sql import SparkSession

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("AggregateDM") \
        .getOrCreate()

    df = spark.read.parquet(
        's3://data.jontav.com/spark_data/gdelt/events/'
    )

    df.createOrReplaceTempView("events")

    summary = spark.sql("select eventcode, year, count(*) as ct from events group by eventcode, year")

    summary.write.parquet(
        's3://data.jontav.com/spark_data/gdelt/stats'
    )

    spark.stop()
