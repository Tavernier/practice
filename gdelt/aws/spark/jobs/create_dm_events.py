from pyspark.sql import SparkSession

def get_gdelt_ddl_schema():
    """Returns the GDELT schema for reading in Spark."""
    fields = [
        "GlobalEventID INT",
        "Day INT",
        "MonthYear INT",
        "YEAR INT",
        "FractionDate DOUBLE",
        "Actor1Code STRING",
        "Actor1Name STRING",
        "Actor1CountryCode STRING",
        "Actor1KnownGroupCode STRING",
        "Actor1EthnicCode STRING",
        "Actor1Religion1Code STRING",
        "Actor1Religion2Code STRING",
        "Actor1Type1Code STRING",
        "Actor1Type2Code STRING",
        "Actor1Type3Code STRING",
        "Actor2Code STRING",
        "Actor2Name STRING",
        "Actor2CountryCode STRING",
        "Actor2KnownGroupCode STRING",
        "Actor2EthnicCode STRING",
        "Actor2Religion1Code STRING",
        "Actor2Religion2Code STRING",
        "Actor2Type1Code STRING",
        "Actor2Type2Code STRING",
        "Actor2Type3Code STRING",
        "IsRootEvent INT",
        "EventCode STRING",
        "EventBaseCode STRING",
        "EventRootCode STRING",
        "QuadClass STRING",
        "GoldsteinScale STRING",
        "NumMentions STRING",
        "NumSources STRING",
        "NumArticles STRING",
        "AvgTone STRING",
        "Actor1Geo_Type STRING",
        "Actor1Geo_Fullname STRING",
        "Actor1Geo_CountryCode STRING",
        "Actor1Geo_ADM1Code STRING",
        "Actor1Geo_Lat STRING",
        "Actor1Geo_Long STRING",
        "Actor1Geo_FeatureID STRING",
        "Actor2Geo_Type STRING",
        "Actor2Geo_Fullname STRING",
        "Actor2Geo_CountryCode STRING",
        "Actor2Geo_ADM1Code STRING",
        "Actor2Geo_Lat STRING",
        "Actor2Geo_Long STRING",
        "Actor2Geo_FeatureID STRING",
        "ActionGeo_Type STRING",
        "ActionGeo_Fullname STRING",
        "ActionGeo_CountryCode STRING",
        "ActionGeo_ADM1Code STRING",
        "ActionGeo_Lat STRING",
        "ActionGeo_Long STRING",
        "ActionGeo_FeatureID STRING",
        "DATEADDED INT",
        "SOURCEURL STRING"
    ]
    return ",".join(fields)

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("CreateEventsDM") \
        .getOrCreate()

    df = spark.read.csv(
        's3://gdelt-open-data/events/',
        schema=get_gdelt_ddl_schema(),
        sep="\t"
    )

    df.write.partitionBy('YEAR').parquet(
        's3://data.jontav.com/spark_data/gdelt/events',
        partitionBy=['YEAR']
    )

    spark.stop()
