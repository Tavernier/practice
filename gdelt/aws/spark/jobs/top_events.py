from pyspark.sql import SparkSession

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("TopEvents") \
        .getOrCreate()

    df = spark.read.parquet(
        's3://data.jontav.com/spark_data/gdelt/events/'
    )

    df.createOrReplaceTempView("events")

    summary = spark.sql("""
    select day, eventcode, event_rank
    from (
        select
            eventcode, day, ct,
            row_number() over(partition by day order by ct desc) as event_rank
        from (
            select eventcode, day, count(*) as ct
            from events
            group by eventcode, day
        ) d
    ) a
    where event_rank <= 5
    """)

    summary.write.parquet(
        's3://data.jontav.com/spark_data/gdelt/top_events'
    )

    spark.stop()
