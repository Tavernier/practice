from pyspark.sql import SparkSession

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("HelloWorld") \
        .getOrCreate()

    # how the heck to people know this?
    # spark.sparkContext._jvm.org.apache.log4j is totally obvious.
    log4jLogger = spark.sparkContext._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger(__name__)

    logger.error('Hello, world!')
    logger.info('Hello information.')
    logger.warn('Warning, warning.')

    # the log entry is:
    # 2019-01-12 20:31:35 WARN  __main__:? - Warning, warning.
    # fiin what the ? should be.  i hate java.

    spark.stop()
