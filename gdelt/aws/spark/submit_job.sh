#! /usr/bin/env bash

set -euxo pipefail

EMR_CLUSTER_ID=$1
JOB_JSON_FILE=$2

sync_jobs_to_s3(){
    aws s3 sync \
        ./jobs/ \
        s3://data.jontav.com/spark/jobs/
    sleep 2
}

submit_a_job(){
    aws emr add-steps \
        --cluster-id ${EMR_CLUSTER_ID} \
        --steps "file://${JOB_JSON_FILE}"
}

main(){
    sync_jobs_to_s3
    submit_a_job
}

main
