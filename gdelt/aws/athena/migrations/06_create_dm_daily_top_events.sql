/*
Table: gdelt_dm.daily_top_events

For each day, this table holds the top 5
events as determined by the count of events each
day.
*/
create table gdelt_dm.daily_top_events
with (
    external_location = 's3://data.jontav.com/athena/dm/gdelt/daily_top_events'
) as
select
    --key
    day,
    eventcode,

    -- values
    event_rank,
    total_events,
    distinct_actor1s,
    distinct_actor2s,

    -- show our work
    'gdelt_dm' as data_source_database,
    'events' as data_source_table
from (
    select
        -- key
        day,
        eventcode,
        row_number() over(
            partition by day
            order by total_events desc
        ) as event_rank,

        total_events,
        distinct_actor1s,
        distinct_actor2s
    from (
        select
            -- key
            day,
            eventcode,
            
            -- values
            count(globaleventid) as total_events,
            approx_distinct(actor1code) as distinct_actor1s,
            approx_distinct(actor2code) as distinct_actor2s
        from gdelt_dm.events
        group by
            day,
            eventcode
    ) d
) fd
where fd.event_rank <= 5;
