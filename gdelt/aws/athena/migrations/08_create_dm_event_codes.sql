/*
Table: gdelt_dm.event_codes

Let's create a summary table for each event.
*/
create table gdelt_dm.event_codes
with (
    external_location = 's3://data.jontav.com/athena/dm/gdelt/event_codes'
) as
select 
    -- key
    eventcode, 
    -- values
    count(*) as total_events,
    avg(goldsteinscale) as goldsteinscale_avg,
    sum(nummentions) as total_mentions,
    sum(numsources) as total_sources,
    sum(numarticles) as total_articles,
    min(day) as earliest_event,
    max(day) as latest_event,
      -- show our work
  'gdelt_dm' as data_source_database,
  'events' as data_source_table
from gdelt_dm.events 
group by
    eventcode
;
