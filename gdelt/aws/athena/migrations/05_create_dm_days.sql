/*
Table: gdelt_dm.days

This table holds various aggregations calculated
at the day level.
*/
create table gdelt_dm.days
with (
    external_location = 's3://data.jontav.com/athena/dm/gdelt/days'
) as
select
    -- key
    day, 

    -- values
    count(globaleventid) as total_events,
    approx_distinct(eventcode) as distinct_events,

    sum(nummentions) as total_mentions,
    sum(numsources) as total_sources,
    sum(numarticles) as total_articles,
    avg(goldsteinscale) as goldsteinscale_avg,

    approx_distinct(actor1code) as actor1_occurrences,
    approx_distinct(actor2code) as actor2_occurrences,

    'gdelt_dm' as data_source_database,
    'events' as data_source_table
from gdelt_dm.events
group by day;
