/*
Table: gdelt_dm.hosts

Let's create a summary table for source url web host.
*/
create table gdelt_dm.hosts
with (
    external_location = 's3://data.jontav.com/athena/dm/gdelt/hosts'
) as
select 
    -- key
    url_extract_host(sourceurl) as host, 
    
    -- values
    count(*) as total_events,
    avg(goldsteinscale) as goldsteinscale_avg,
    sum(nummentions) as total_mentions,
    sum(numsources) as total_sources,
    sum(numarticles) as total_articles,
    min(day) as earliest_event,
    max(day) as latest_event,
      -- show our work
  'gdelt_dm' as data_source_database,
  'events' as data_source_table
from gdelt_dm.events
where sourceurl <> ''
group by
    url_extract_host(sourceurl)
;
