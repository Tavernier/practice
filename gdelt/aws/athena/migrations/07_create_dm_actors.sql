/*
Table: gdelt_dm.actors

Each actor appears once in this table.
Aggregate information, such as total events, is calculated.
The top 3 actors with which the actor in question targets
are also present.
*/
create table gdelt_dm.actors
with (
    external_location = 's3://data.jontav.com/athena/dm/gdelt/actors'
) as
select
 -- key
 actors.actor_code,
 
 -- values
 actor1.actor1_total_events,
 actor1.actor1_goldsteinscale_avg,
 actor1.actor1_earliest_event,
 actor1.actor1_latest_event,
 
 actor2.actor2_total_events,
 actor2.actor2_goldsteinscale_avg,
 actor2.actor2_earliest_event,
 actor2.actor2_latest_event,
 
 coalesce(actor1.actor1_total_events, 0) + coalesce(actor2.actor2_total_events, 0) as total_events,
 greatest(actor1.actor1_latest_event, actor2.actor2_latest_event) as latest_event,
 greatest(actor1.actor1_earliest_event, actor2.actor2_earliest_event) as earliest_event,
 
 top_actor2s.actor2s,
    
  -- show our work
  'gdelt_dm' as data_source_database,
  'events' as data_source_table
from (
  select distinct actor1code as actor_code
  from gdelt_dm.events
  where actor1code is not null

  union distinct

  select distinct actor2code as actor_code
  from gdelt_dm.events
  where actor2code is not null
) actors
left join (
  select 
    actor1code as actor_code, 
    count(*) as actor1_total_events,
    avg(goldsteinscale) as actor1_goldsteinscale_avg,
    min(day) as actor1_earliest_event,
    max(day) as actor1_latest_event
  from gdelt_dm.events 
  group by actor1code
) actor1
  on actors.actor_code = actor1.actor_code
left join (
  select 
    actor2code as actor_code, 
    count(*) as actor2_total_events,
    avg(goldsteinscale) as actor2_goldsteinscale_avg,
    min(day) as actor2_earliest_event,
    max(day) as actor2_latest_event
  from gdelt_dm.events 
  group by actor2code
) actor2
  on actors.actor_code = actor2.actor_code
left join (
    select
        --key
        actor1code,

        array_agg(
            cast(
              row(actor2code, actor2_rank, total_events, earliest_contact, latest_contact)
              as row(actor2code varchar(100), actor2_rank integer, total_events integer, earliest_contact date, latest_contact date)
            )
        ) as actor2s
    from (
        select
            -- key
            actor1code, actor2code,
            row_number() over(
                partition by actor1code
                order by total_events desc
            ) as actor2_rank,
            --values
            total_events,
            earliest_contact,
            latest_contact
        from(
            select
                actor1code, actor2code,
                count(globaleventid) as total_events,
                min(day) as earliest_contact,
                max(day) as latest_contact
            from gdelt_dm.events
            where actor1code is not null and actor2code is not null
            and actor1code <> '' and actor2code <> ''
            group by
                actor1code, actor2code
        ) d
    ) d
    where actor2_rank <= 3
    group by
        actor1code
) top_actor2s
    on actors.actor_code = top_actor2s.actor1code
;
