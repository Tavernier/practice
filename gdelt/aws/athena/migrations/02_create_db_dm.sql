create database if not exists gdelt_dm
    comment 'Transformed data.'
    location 's3://data.jontav.com/athena/dm/gdelt';