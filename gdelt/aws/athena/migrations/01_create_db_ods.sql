create database if not exists ods
    comment 'Original source data.'
    location 's3://data.jontav.com/athena/ods';
