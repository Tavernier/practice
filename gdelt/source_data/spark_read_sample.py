from pyspark.sql import SparkSession

import helpers

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("AggregateGdelt") \
    .getOrCreate()

df = spark.read.csv(
    '/tmp/gdelt/sample/20190105.export.csv',
    schema=helpers.get_gdelt_ddl_schema(),
    sep="\t"
)

df.show()

df.createOrReplaceTempView("events")

spark.sql("""
select
    Day,
    EventCode,
    rn as EventRank
from (
    select
        Day,
        EventCode,
        ct,
        row_number() over(
            partition by Day
            order by ct desc, EventCode
        ) as rn
    from (
        select 
            Day,
            EventCode, 
            count(*) as ct 
        from events 
        group by
            Day,
            EventCode
    ) d
) fd
where rn <= 5
""").show(10)

spark.sql("""
select 
    Day, 
    count(*) as ct 
from events 
group by
    Day
""").show(10)

spark.stop()
