from pyspark.sql import SparkSession

import helpers

# Class documentation: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# A SparkSession is required. 
spark = SparkSession \
    .builder \
    .appName("ParquetCreator") \
    .getOrCreate()

# what the hell?  all rows load as null with no errors 
# or logs to indicate why that is the case.  why do 
# people like spark?
df = spark.read.csv(
    '/tmp/gdelt/sample/20190105.export.csv',
    schema=helpers.get_gdelt_ddl_schema(),
    sep="\t"
)

df.show()

print("Creating Unpartitioned Set")
df.write.parquet('/tmp/gdelt/dm/events')

print("Creating Partitioned Set")
df.write.partitionBy('YEAR').parquet(
    '/tmp/gdelt/dm/events_partitioned',
    partitionBy=['YEAR']
)

spark.stop()
