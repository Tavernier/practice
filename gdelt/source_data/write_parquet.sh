#! /usr/bin/env bash

set euxo pipefail

prep_temp_space(){
    mkdir -p /tmp/gdelt/dm
    rm -rf /tmp/gdelt/dm
}

create_parquet(){
    python3 ./spark_create_parquet.py
}

main(){
    prep_temp_space
    create_parquet
}

main
