#! /usr/bin/env bash

set -euxo pipefail

prep_temp_space(){
    mkdir -p /tmp/gdelt/sample
    rm -f /tmp/gdelt/sample/*
}

download_file(){
    aws s3 cp \
        s3://gdelt-open-data/events/20190105.export.csv \
        /tmp/gdelt/sample
}

main(){
    prep_temp_space
    download_file
}

main
