#! /usr/bin/env bash

set -euxo pipefail

build_base_image(){
    pushd ./images/base
    docker build . --tag jt_base
    popd
}

copy_source_schema_down(){
    cp -f \
        ./source_data/schema.json \
        ./images/gdelt/app/schemas/gdelt.json
}

build_gdelt_image(){
    pushd ./images/base
    docker build . --tag jt_gdelt
    popd
}

main(){
    build_base_image

    copy_source_schema_down
    build_gdelt_image
}

main
