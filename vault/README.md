# Hashicorp Vault

My goal here is to understand basic input / output with Hashicorp's Vault product.  I'm hard-coding a token for this purpose, which is horrible to do in any real server.  Vault is running under development mode here.

## Usage

Hello world...I want to, at the very least, understand storing and getting key values pairs from Vault.  I think the most likely use case will be reading secret values at application runtime.

1. Call `./up.sh` to launch Vault on localhost 8200.
2. Login at `http://localhost:8200/ui` with token `a1b2c3`.
3. Create secrets.
    - Use the Web UI.
    - Use the CLI via `./put_secrets.sh`.
4. Read secrets.
    - Make sure you create secrets (see above).
    - Use the Web UI to see the secrets.
    - Use the CLI via `./get_secrets.sh`.
    - Use Python via `./get_secrets.py`.

## Takeaways

Super easy to use and perform a hello world example.  I wonder what most folks use for authentication in the real world.
