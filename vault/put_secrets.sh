#! /usr/bin/env bash

main(){
    # hello world for storing a single key value secret to a path
    docker exec vault_vault_1 vault kv put \
        secret/hello \
        foo=world

    # a secret can hold many key value pairs
    docker exec vault_vault_1 vault kv put \
        secret/creds \
        user=bugs_bunny \
        pass=carr0ts!
}

main
