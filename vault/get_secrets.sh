#! /usr/bin/env bash

main(){
    # hello world for getting a single key value secret
    docker exec vault_vault_1 vault kv get secret/hello

    # a secret can hold many key value pairs
    docker exec vault_vault_1 vault kv get secret/creds 
}

main
