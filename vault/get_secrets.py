#! /usr/bin/env python3

import hvac

CLIENT = hvac.Client(token='a1b2c3')

def list_secrets():
    return CLIENT.secrets.kv.v2.list_secrets(path='')['data']['keys']

def read_secret(path):
    """Returns the most recent version."""
    return CLIENT.secrets.kv.v2.read_secret_version(path=path)['data']['data']

if __name__ == "__main__":
    print(list_secrets())
    print(read_secret('hello'))
    print(read_secret('creds'))
