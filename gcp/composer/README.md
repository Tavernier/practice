# Cloud Composer

I have used Apache Airflow.  Google offers Apache Airflow as-a-service via its Cloud Composer service.  I have not used Cloud Composer so the exercise here is for me to get more comfortable with running Airflow via Cloud Composer.

Fun fact: This [Cloud Composer: Qwik Start lab](https://www.qwiklabs.com/focuses/2456?locale=en&parent=catalog) is one credit and lasts 45 minutes.  Qwiklabs will create a temp GCP project and destroy all GCP resources when the lab is over.

## Managing the Cluster

1. How to launch cluster programmatically?
    - for poc testing, gcloud can be used: gcloud composer environments create. this can then be automated elsewhere.
    - reference: https://cloud.google.com/composer/docs/how-to/managing/creating
    - See example in commands below.
    - notable options:
        - `machine-type`: default is `n1-standard-1`. i think three `n1-highcpu-4` would be a good start.
        - `disk-size`: default is 100 GB. used for the node VMs. min is 20 GB.  Where is this space accessible?
            - "DAGs and plugins: By default, Cloud Composer provisions 100 GB capacity for your environment, the dags/ folder, and the plugins/ folder."
            - 100 GB is a ton for just code files.
    - notable missing options:
        - I see no way to set the GCS bucket at cluster creation time.  So, have to grab that after cluster is created.
1. How are Airflow config options set?
    - at cluster creation time, the `airflow-configs` option can be used.
    - some properties are blocked: https://cloud.google.com/composer/docs/concepts/airflow-configurations
    - after the cluster is running, the `update-airflow-configs` option can be used, but it will cause the cluster to enter an updating state.
        - reference: https://cloud.google.com/composer/docs/how-to/managing/updating#updating_environments
        - changing env vars is probably the equivalent of nuking the pods and relaunching with new env values.
1. What Airflow config options should we set?
    - Look into catchup, max overall task instances, max task instances per DAG.
1. How are secrets controlled and set?
    - Let's say we have a sensitive value we wish to expose as an environment variable in Airfow.
1. How are environment variables set?
    - at cluster creation time, the `env-variables` option can be used.  See the example above.
    - after the cluster is running, the `update-env-variables` option can be used.
        - reference: https://cloud.google.com/sdk/gcloud/reference/beta/composer/environments/update
1. How are Airflow variables set?
    - The Airflow Web UI is accessible so variables can be set there.
    - See example below for setting via `gcloud` cli.
1. What functionality is beta vs released?
    - Need to look into...
1. What GCP components are created?
    - Kubernetes Engine
        - GCP auto creates a k8s cluster for each Composer environment we create.
    - Google Cloud Storage
        - GCP auto creates a GCS bucket for each Composer environment we create.
    - Networking
        - No clue.  I will investiage at some other time.
1. What is used for the Airflow database?
    - Airflow itself stores data about task instances, DAG runs, DAGs metadata, variables, etc.  Where is that data kept?
    - Looks like SQLite within Airflow itself: `sqlite:////home/airflow/gcs/airflow.db`
1. How do I access the Airflow Web UI?
    - GCP Console > Composer > Environment > Airflow web UI link.
    - Reference: https://cloud.google.com/composer/docs/how-to/accessing/airflow-web-interface
1. How are upgrades handled?
    - Need to look into.
1. What's the cluster lifecycle? I think it will be:
    1. Create Cluster
    1. [Sync DAGs](https://cloud.google.com/sdk/gcloud/reference/composer/environments/storage/dags/) and [Plugins](https://cloud.google.com/sdk/gcloud/reference/composer/environments/storage/plugins/) to Cluster's DAGs GCS Location
    1. Profit
1. Where does code and logs live?
    - [Google documents the GCS and local folders](https://cloud.google.com/composer/docs/concepts/cloud-storage#folders_in_the_storage_name_bucket).
    - What's interesting is that the "data/ folder and logs/ folder are not subject to capacity limits."
    - "Cloud Composer synchronizes the dags/ and plugins/ folders uni-directionally by copying locally and synchronizes data/ and logs/ folders bi-directionally by using Cloud Storage FUSE."
    - Regarding logs:
        - The Airflow Web UI displays these.
        - Stackdriver has these too.

### Example Commands

```bash
# get some help
gcloud help composer environments create

# create a cluster
# this takes between 15 and 20 minutes.
gcloud composer environments create jon-airflow-poc \
    --location=us-central1 \
    --zone=us-central1-a \
    --machine-type=n1-highcpu-4 \
    --python-version=3 \
    --node-count 3 \
    --env-variables='CITY=Chicago'

# set an Airflow variable (not an envar)
gcloud composer environments run jon-airflow-poc \
     --location us-central1 variables -- \
     --set 'DAG_GIT_VERSION' 'BlAH'

# this will cause the cluser to enter an update state so 
# probably want to not use or use sparingly.
gcloud beta composer environments update jon-airflow-poc \
     --location us-central1 \
     --update-env-variables='CITY=Buffalo'

# import DAGs
# this is a copy only so we may be better off performing a gsutil
# sync in case we want to delete remote files cause they were 
# delete from source.
# i was unable to get this to work.  here's what happened:
#   this command placed the dags in the GCS bucket ./dags/dags folder.
#   i expected the files to be copied just to the ./dags GCS folder.
gcloud composer environments storage dags import \
    --environment jon-airflow-poc \
    --location us-central1 \
    --source ./dags

# alternative: manually sync to gcs
DAGS_BUCKET=$(gcloud composer environments describe jon-airflow-poc \
    --location us-central1 \
    --format=json | jq -r '.config.dagGcsPrefix')

gsutil -m rsync \
    -d \
    -r \
    ./dags \
    ${DAGS_BUCKET}


# see DAGs
gcloud composer environments storage dags list \
    --environment jon-airflow-poc \
    --location us-central1
```

### Airflow Running Configuration

I launched an Airflow cluster than visited the Admin Configuration page to grab the environment's settings.

<table>
    <tr>
        <th>Section</th>
        <th>Key</th>
        <th>Value</th>
        <th>Source</th>
    </tr>
    <tr>
        <td>core</td>
        <td>airflow_home</td>
        <td class='code'>/home/airflow/gcs</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>dags_folder</td>
        <td class='code'>/home/airflow/gcs/dags</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>base_log_folder</td>
        <td class='code'>/home/airflow/gcs/logs</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>remote_log_conn_id</td>
        <td class='code'>google_cloud_default</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>encrypt_s3_logs</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>logging_level</td>
        <td class='code'>INFO</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>logging_config_class</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>log_format</td>
        <td class='code'>[%%(asctime)s] {%%(filename)s:%%(lineno)d} %%(levelname)s - %%(message)s</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>simple_log_format</td>
        <td class='code'>%%(asctime)s %%(levelname)s - %%(message)s</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>executor</td>
        <td class='code'>CeleryExecutor</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>sql_alchemy_conn</td>
        <td class='code'>sqlite:////home/airflow/gcs/airflow.db</td>
        <td>bash cmd</td>
    </tr>
    <tr>
        <td>core</td>
        <td>sql_alchemy_pool_size</td>
        <td class='code'>5</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>sql_alchemy_pool_recycle</td>
        <td class='code'>3600</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>parallelism</td>
        <td class='code'>30</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>dag_concurrency</td>
        <td class='code'>15</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>dags_are_paused_at_creation</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>non_pooled_task_slot_count</td>
        <td class='code'>128</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>max_active_runs_per_dag</td>
        <td class='code'>15</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>load_examples</td>
        <td class='code'>False</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>plugins_folder</td>
        <td class='code'>/home/airflow/gcs/plugins</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>fernet_key</td>
        <td class='code'>QJIHdWw6-zjPzARuHiYPx04nQn_TH7zXZFuaFBpnBOI=</td>
        <td>bash cmd</td>
    </tr>
    <tr>
        <td>core</td>
        <td>donot_pickle</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>dagbag_import_timeout</td>
        <td class='code'>30</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>task_runner</td>
        <td class='code'>BashTaskRunner</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>default_impersonation</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>security</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>unit_test_mode</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>task_log_reader</td>
        <td class='code'>gcs.task</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>core</td>
        <td>enable_xcom_pickling</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>killed_task_cleanup_time</td>
        <td class='code'>60</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>core</td>
        <td>remote_base_log_folder</td>
        <td class='code'>gs://us-central1-jon-airflow-poc-8abbd8e9-bucket/logs</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>cli</td>
        <td>api_client</td>
        <td class='code'>airflow.api.client.local_client</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>cli</td>
        <td>endpoint_url</td>
        <td class='code'>http://localhost:8080</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>api</td>
        <td>auth_backend</td>
        <td class='code'>airflow.api.auth.backend.default</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>operators</td>
        <td>default_owner</td>
        <td class='code'>Airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>operators</td>
        <td>default_cpus</td>
        <td class='code'>1</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>operators</td>
        <td>default_ram</td>
        <td class='code'>512</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>operators</td>
        <td>default_disk</td>
        <td class='code'>512</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>operators</td>
        <td>default_gpus</td>
        <td class='code'>0</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>base_url</td>
        <td class='code'>https://j857f1f2e0121b2a7-tp.appspot.com</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_host</td>
        <td class='code'>0.0.0.0</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_port</td>
        <td class='code'>8080</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_ssl_cert</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_ssl_key</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_worker_timeout</td>
        <td class='code'>120</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>worker_refresh_batch_size</td>
        <td class='code'>1</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>worker_refresh_interval</td>
        <td class='code'>60</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>secret_key</td>
        <td class='code'>some-random-id</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>workers</td>
        <td class='code'>3</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>worker_class</td>
        <td class='code'>sync</td>
        <td>env var</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>access_logfile</td>
        <td class='code'>-</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>error_logfile</td>
        <td class='code'>-</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>expose_config</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>authenticate</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>filter_by_owner</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>owner_mode</td>
        <td class='code'>user</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>dag_default_view</td>
        <td class='code'>tree</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>dag_orientation</td>
        <td class='code'>LR</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>demo_mode</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>log_fetch_timeout_sec</td>
        <td class='code'>5</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>hide_paused_dags_by_default</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>page_size</td>
        <td class='code'>100</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>webserver</td>
        <td>web_server_name</td>
        <td class='code'>jon-airflow-poc</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>email</td>
        <td>email_backend</td>
        <td class='code'>airflow.contrib.utils.sendgrid.send_email</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_host</td>
        <td class='code'>localhost</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_starttls</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_ssl</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_port</td>
        <td class='code'>25</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_mail_from</td>
        <td class='code'>no-reply@cloud.google.com</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_user</td>
        <td class='code'>airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>smtp</td>
        <td>smtp_password</td>
        <td class='code'>airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>celery_app_name</td>
        <td class='code'>airflow.executors.celery_executor</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>celeryd_concurrency</td>
        <td class='code'>6</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>worker_log_server_port</td>
        <td class='code'>8793</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>broker_url</td>
        <td class='code'>redis://airflow-redis-service:6379/0</td>
        <td>bash cmd</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>celery_result_backend</td>
        <td class='code'>redis://airflow-redis-service:6379/0</td>
        <td>bash cmd</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>flower_host</td>
        <td class='code'>0.0.0.0</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>flower_port</td>
        <td class='code'>5555</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>default_queue</td>
        <td class='code'>default</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>celery_config_options</td>
        <td class='code'>airflow.config_templates.default_celery.DEFAULT_CELERY_CONFIG</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>celery</td>
        <td>celery_ssl_active</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>dask</td>
        <td>cluster_address</td>
        <td class='code'>127.0.0.1:8786</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>job_heartbeat_sec</td>
        <td class='code'>5</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>scheduler_heartbeat_sec</td>
        <td class='code'>5</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>run_duration</td>
        <td class='code'>-1</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>min_file_process_interval</td>
        <td class='code'>0</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>dag_dir_list_interval</td>
        <td class='code'>100</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>print_stats_interval</td>
        <td class='code'>30</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>child_process_log_directory</td>
        <td class='code'>/home/airflow/gcs/logs/scheduler</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>scheduler_zombie_task_threshold</td>
        <td class='code'>300</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>catchup_by_default</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>max_tis_per_query</td>
        <td class='code'>0</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>statsd_on</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>statsd_host</td>
        <td class='code'>localhost</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>statsd_port</td>
        <td class='code'>8125</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>statsd_prefix</td>
        <td class='code'>airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>max_threads</td>
        <td class='code'>2</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>scheduler</td>
        <td>authenticate</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>uri</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>user_filter</td>
        <td class='code'>objectClass=*</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>user_name_attr</td>
        <td class='code'>uid</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>group_member_attr</td>
        <td class='code'>memberOf</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>superuser_filter</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>data_profiler_filter</td>
        <td class='code'></td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>bind_user</td>
        <td class='code'>cn=Manager,dc=example,dc=com</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>bind_password</td>
        <td class='code'>insecure</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>basedn</td>
        <td class='code'>dc=example,dc=com</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>cacert</td>
        <td class='code'>/etc/ca/ldap_ca.crt</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>ldap</td>
        <td>search_scope</td>
        <td class='code'>LEVEL</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>master</td>
        <td class='code'>localhost:5050</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>framework_name</td>
        <td class='code'>Airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>task_cpu</td>
        <td class='code'>1</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>task_memory</td>
        <td class='code'>256</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>checkpoint</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>mesos</td>
        <td>authenticate</td>
        <td class='code'>False</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kerberos</td>
        <td>ccache</td>
        <td class='code'>/tmp/airflow_krb5_ccache</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kerberos</td>
        <td>principal</td>
        <td class='code'>airflow</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kerberos</td>
        <td>reinit_frequency</td>
        <td class='code'>3600</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kerberos</td>
        <td>kinit_path</td>
        <td class='code'>kinit</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kerberos</td>
        <td>keytab</td>
        <td class='code'>airflow.keytab</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>github_enterprise</td>
        <td>api_rev</td>
        <td class='code'>v3</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>admin</td>
        <td>hide_sensitive_variable_fields</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
    <tr>
        <td>kubernetes</td>
        <td>in_cluster</td>
        <td class='code'>True</td>
        <td>airflow config</td>
    </tr>
</table>

# Functionality

1. How can I start a Docker container on more powerful nodes?
    - "The KubernetesPodOperator is best suited for launching pods into the GKE cluster for your Cloud Composer environment."
    - [Google recommends launching pods in a different node pool](https://cloud.google.com/composer/docs/how-to/using/using-kubernetes-pod-operator#node-pool) to avoid taking resources away from Airflow.
    - I do not understand this note: "The new node pool is shared between the Airflow processes and the KubernetesPodOperator pods: Airflow processes might leak into the new node pool."  What's the point of creating a new node pool then?  What is the impact of these processes leaking into the new node pool?
    - What is a node pool?
        - "When you create a container cluster, the number and type of nodes that you specify becomes the default node pool. Then, you can add additional custom node pools of different sizes and types to your cluster. All nodes in any given node pool are identical to one another."

# Gotchas

1. The `@once` DAG schedule is busted in GCP Cloud Composer.  Tasks will not run.


# Local Development

1. Does Google provide a Docker Image already to launch their flavor of Airflow locally?
1. How to launch Apache Airflow (aka Cloud Composer)?
    - Can a local instance be created that's "close enough" to Cloud Composer?  The idea here is to enable local development for fast iterations over code.
1. How to launch Apache Beam (aka GCP Dataflow)?
1. How to launch Apache Spark (aka GCP Dataproc)?
1. What utils exist within Cloud Composer?
    - Google [documents those comamnds](https://cloud.google.com/composer/docs/how-to/using/writing-dags#bashoperator).
    - Example: The `bq` cli is available so that's something I would want to build into my locally running Airflow instance.
