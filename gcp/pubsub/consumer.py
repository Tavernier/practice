#! /usr/bin/env python3
"""Read and acknowledge messages off the hello-world-consumer-01 subscription."""

import os
import time

from google.cloud import pubsub_v1

subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(
    os.environ['GCP_PROJECT_ID'], 'hello-world-consumer-01')

def callback(message):

    output = f"Received message: {message.data}"
    
    if message.attributes:
        output += f"  Attributes: {message.attributes}"

    print(output)

    message.ack()

# Limit the subscriber to only have ten outstanding messages at a time.
flow_control = pubsub_v1.types.FlowControl(max_messages=10)
subscriber.subscribe(
    subscription_path, callback=callback, flow_control=flow_control)

# The subscriber is non-blocking, so we must keep the main thread from
# exiting to allow it to process messages in the background.
print('Listening for messages on {}'.format(subscription_path))
while True:
    time.sleep(60)
