# GCP Pub/Sub

The goal here is to complete a "hello world" exercise of:

1. Creating a topic and publishing messages to it.
1. Creating a subscription and reading messages from it.
1. Piping the subscription to BigQuery.

I stole most of the code from Google's documentation.

## Requirements

1. A GCP project and gcloud installation is assumed.
1. Set these environment variables:
    - `GCP_PROJECT_ID`: commands will run against this project
    - `GOOGLE_APPLICATION_CREDENTIALS`: path to service account's json creds file

## Example

```bash
# create the topic
gcloud pubsub topics create hello-world

# create the subscription
gcloud pubsub subscriptions create 'hello-world-consumer-01' \
--topic 'hello-world' \
--topic-project ${GCP_PROJECT_ID}

# publish 100 messages
./publisher.py -n 100

# consume the messages
# avoid running with dataflow since i'm using the same subscription for that example
./consumer.py

# create the bigquery table
bq mk temp.cities ./cities.json

# start a streaming dataflow job to bq from pubsub
gcloud dataflow jobs run JOB_NAME \
    --gcs-location gs://dataflow-templates/latest/PubSub_Subscription_to_BigQuery \
    --parameters \
inputSubscription=projects/${GCP_PROJECT_ID}/subscriptions/hello-world-consumer-01,\
outputTableSpec=${GCP_PROJECT_ID}:temp.cities
```

## Future Questions to Answer

1. How is schema evolution handled?
    - The destination table's schema must be manually updated to match incoming messages.
    - The [PubSub_Subscription_to_BigQuery](https://cloud.google.com/dataflow/docs/guides/templates/provided-streaming#cloud-pubsub-subscription-to-bigquery) template is being used. No parameter exists to auto add fields.
1. What if the message contains fields not present in the BigQuery table?
    - In the publisher, I added a `tickets_sold` integer attribute then published some messages.
    - The Dataflow job wrote these new messages to an errors table since the BigQuery destination table did not have that attribute.
    - See `temp.cities_error_records`
    - The message data is represented in the `payloadString` as a json blob (the field is a string data type).
    - These messages are acked off the subscription.
    - I added a `tickets_sold:int64` field and messages starting writing to the table with some delay.  The job might periodically check and cache the table's metadata.
1. What if the message does not contain all the fields in the table?
    - The message will still write to the table provided the field is marked as nullable.
1. What if a message does not conform to the BigQuery table?
    - The PubSub_Subscription_to_BigQuery template automatically creates an errors table and pipes the messages there.  The messages are acknowledge off the subscription so they will not pipe again.
1. How is an insert ID specified to help de-dupe streaming BigQuery data?
    - As long as the [`insertId` attribute exists on the table](https://cloud.google.com/bigquery/streaming-data-into-bigquery#dataconsistency), BigQuery will try to de-dupe rows provided the dupes stream together within a minute.  BigQuery will not de-dupe rows streamed further apart.
1. What is a future? Google's doc refers to this.
1. How are various BigQuery data types handled?
    - Float: see `fun_factor` field.
    - Numeric: unsure. tried piping `random.random()`. gcp says `"message" : "Invalid NUMERIC value: 0.37205138487377609"`. see `popcorn_rating` field.
    - Integer: see `tickets_sold` field.
    - String: see `city` field.
    - Date: see `created_dt` field.
    - DateTime: see `local_created_at` field.
    - Timestamp: see `created_at` field.
    - Boolean: see `is_matinee` field.
    - Repeated values: see `snacks` field.
    - Records: see `clerk` field.
