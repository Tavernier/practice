#! /usr/bin/env python3
"""Publish messages to the hello-world topic."""

import argparse
from datetime import datetime
import json
import os
import random

from google.cloud import pubsub_v1

def publish_messages_to_topic(number_of_messages, topic_name='hello-world'):

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(os.environ['GCP_PROJECT_ID'], topic_name)

    for _ in range(0, number_of_messages):
        # this is the actual message data itself
        # let's say we run a movie theatre and someone buys a ticket
        created_at = datetime.utcnow()
        local_created_at = datetime.now()  # assumes not running process on utc box

        payload = {
            "city": random.choice(["Chicago", "Springfield", "Joliet", "Rockford"]),
            "state": "IL",
            "movie": random.choice(["Toy Story 4", "Avengers", "Star Wars"]),
            "tickets_sold": random.randint(1, 6),
            "local_created_at": local_created_at.isoformat(),
            "created_at": created_at.isoformat(),
            "created_dt": str(created_at.date()), # YYYY-MM-DD
            "is_matinee": random.choice([True, False]),
            "fun_factor": random.random(),
            "popcorn_rating": None,
            "snacks": random.choices(['twizzlers', 'peanut m&ms', 'popcorn', 'nachos', 'milk duds', 'skittles'], k=random.randint(1,3)),
            "clerk": {
                "last_name": random.choice(["Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller"]),
                "first_name": random.choice(["Matt", "Joe", "Jason", "Emma", "Charlotte", "Olivia", "Sarah"]),
            }
        }

        payload = json.dumps(payload)

        # message attributes are not stored in BigQuery
        publisher.publish(
            topic=topic_path,
            data=payload.encode('utf-8'),

            publisher="echo-one"
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-n',
        '--number_of_messages',
        type=int,
        help='Number of messages to publish.',
        default=10
    )

    args = parser.parse_args()

    print(f"Publishing {args.number_of_messages} messages.")

    publish_messages_to_topic(args.number_of_messages)
