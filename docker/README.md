# Docker

The purpose of this content is to understand Docker best practices particularly those around Dockerfile, Docker Compose, etc.

## Dockerfile

1. Docker's [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
1. Google's [best practices](https://cloud.google.com/solutions/best-practices-for-building-containers)
    1. They also provide best practices for [running containers](https://cloud.google.com/solutions/best-practices-for-operating-containers)
1. [Twelve-Factor App Processes](https://12factor.net/processes)
1. Use `.dockerignore` and understand build context
1. Order layers from less frequently changed to more frequently changed to take advantage of cached layers.
1. Use absolute paths when referring to paths within the image.  Use WORKDIR vs RUN cd.
1. Know that entrypoint exists and can be used to treat the image like a binary.
1. See [example for proper apt-get usage](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#run)
1. Multi-stage builds look interesting, but I need more time to understand them.


```bash
# build the image
docker build . --tag jontesting

# we are running as appuser
docker run --rm jontesting whoami

# this script is runnable because we added execute permissions in the Dockerfile
docker run --rm jontesting /app/howdy.py

# we could also run with this approach
docker run --rm jontesting python /app/howdy.py

# interactive bash session
docker run --rm -it jontesting bash
```