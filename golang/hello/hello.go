/*
Declare a main package (a package is a way to group
functions, and it's made up of all the files in the
same directory).
*/
package main

/*
Import the popular fmt package, which contains functions
for formatting text, including printing to the console.
This package is one of the standard library packages you
got when you installed Go.
See: https://pkg.go.dev/fmt

The other import is from ../greetings because I ran:
go mod edit -replace example.com/greetings=../greetings
For production, example.com/greetings should be published.
*/
import (
    "fmt"
    "log"

    "example.com/greetings"
)

/*
After adding this line, I ran this command: go mod tidy
Go will add the quote module as a requirement, as well
as a go.sum file for use in authenticating the module.
*/
import "rsc.io/quote"

/*
Implement a main function to print a message to the console.
A main function executes by default when you run the main
package.
*/
func main() {
    // Set properties of the predefined Logger, including
    // the log entry prefix and a flag to disable printing
    // the time, source file, and line number.
    log.SetPrefix("greetings: ")
    log.SetFlags(0)
    
    fmt.Println("Hello, World!")
	fmt.Println(quote.Go())

	message, err := greetings.Hello("Gladys")
    fmt.Println(message)

    // cannot use walrus to reuse same vars since walrus
    // initiates then assigns value of variable.
    message2, err2 := greetings.Hello("Homer")
    if err2 != nil {
        log.Fatal(err) // exit the linux process with an error status
    }
    fmt.Println(message2)


    // Use the new Hellos function
    // Define a slice of names to pass to Hellos.
    names := []string{"Gladys", "Samantha", "Darrin"}

    // Request greeting messages for the names.
    messages, err := greetings.Hellos(names)
    if err != nil {
        log.Fatal(err)
    }
    // Print the returned map of messages to the console.
    fmt.Println(messages)
}
