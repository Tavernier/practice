# Learning Go

- I'm going (hahah) through [Go's tutorials](https://go.dev/doc/tutorial/).
- I'm familiar with Python already but Go is new to me.
- Some of the statements below are copied directly from Go's tutorials pages.

## Go Terms

- External Package
    - found in [pkg.go.dev](https://pkg.go.dev/)
    - add to your code via `import` then run `go mod tidy`
- Standard Library
    - Go includes a lot of the out-of-the-box functionality in its [Standard Library](https://pkg.go.dev/std).
- Package vs. Module
    - A package is a directory of .go files, and it is the basic building block of a Go program. Packages help to organize code into reusable components.
    - On the other side, a module is a collection of packages with built-in dependencies and versioning.

## Go Programming Terms and Notes

- `init()`: Go automatically runs this at execution startup, after global variables have been initialized. Nice!
- Maps: like a Python Dictionary. Defined like `myMap := make(map[key-type]value-type)` such as `make(map[string]string)`.
- Slices: like a Python List. It's a dynamic array in Go and defined like `[]string`
 
## Basic Commands

- `go version`
- `go mod tidy`
- `go mod edit -replace example.com/greetings=../greetings`
- `go run .`
- `go test`

## Testing

- Ending a file with `_test.go` tells the go test command that this file contains test functions.
- Test function names have the form TestName, where Name says something about the specific test.
    - Syntax is `func TestName(t *testing.T) {...}`
    - Then use `t` to do stuff like `t.Fatalf()` to report a failure.

You can get more output with the `-v` flag on `go test`:

```shell
go test -v
=== RUN   TestHelloName
--- PASS: TestHelloName (0.00s)
=== RUN   TestHelloEmpty
--- PASS: TestHelloEmpty (0.00s)
PASS
ok  	example.com/greetings	0.101s
```

In [greetings_test.go](./greetings/greetings_test.go), I purposely broke a test (since fixed) to see the results. Here's the output:

```shell
go test
--- FAIL: TestHelloName (0.00s)
    greetings_test.go:15: Hello("Gladys") = "Great to see you, Gladys!", <nil>, want match for `\bGladysD\b`, nil
FAIL
exit status 1
FAIL	example.com/greetings	0.107s
```

## Compiling and Installing the Application

- The `go build` command compiles the packages, along with their dependencies, but it doesn't install the results.
- The `go install` command compiles and installs the packages.

### Compiling

```shell
cd ./hello

# this creates the ./hello executable in the same directory.
go build

# so, we can now run it:
./hello
```

### Installing

Run `go list -f '{{.Target}}'` to see where Go will install the program. For me, that path is "/Users/xjxt156/go/bin/hello". So, I could install it there and add that path to my `PATH` environment variable so I could run `hello` anywhere.

Alternatively, I could set the install target location by populating the variable `GOBIN` such as in this example: `go env -w GOBIN=/path/to/your/bin`.  I could then run `go install` and have access to `hello` at the command line. Let's try this option.

```shell
# /usr/local/bin is already in PATH
go env -w GOBIN=/usr/local/bin
go install
cd /tmp
hello
```
