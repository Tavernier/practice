package greetings

import (
    "errors"
    "fmt"
    "math/rand"
    "time"
)

// Hello returns a greeting for the named person.
// The second returned value is an error if one occurred.
func Hello(name string) (string, error) {
    // If no name was given, return an error with a message.
    if name == "" {
        return "", errors.New("empty name")
    }

    // If a name was received, return a value that embeds the name
    // in a greeting message.
    message := fmt.Sprintf(randomFormat(), name)
    return message, nil
}

// Hellos returns a map that associates each of the named people
// with a greeting message.
func Hellos(names []string) (map[string]string, error) {
    // A map to associate names with messages.
    // In Go, you initialize a map with this
    // syntax: make(map[key-type]value-type)
    messages := make(map[string]string)

    // Loop through the received slice of names, calling
    // the Hello function to get a message for each name.
    // the underscore is the same in python. we don't need
    // the variable. range returns the index and value.
    for _, name := range names {
        message, err := Hello(name)
        if err != nil {
            return nil, err
        }
        messages[name] = message
    }
    return messages, nil
}

// init sets initial values for variables used in the function.
// Go executes init functions automatically at program startup,
// after global variables have been initialized.
func init() {
    rand.Seed(time.Now().UnixNano())
}

// randomFormat returns one of a set of greeting messages. The returned
// message is selected at random. randomFormat starts with a lowercase
// letter, making it accessible only to code in its own package
// (in other words, it's not exported).
func randomFormat() string {
    // A slice of message formats.
    // A slice is like an array, except that its size changes
    // dynamically as you add and remove items. The slice is //
    // one of Go's most useful types.
    formats := []string{
        "Hi, %v. Welcome!",
        "Great to see you, %v!",
        "Hail, %v! Well met!",
    }

    // Return a randomly selected message format by specifying
    // a random index for the slice of formats.
    return formats[rand.Intn(len(formats))]
}