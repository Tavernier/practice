create table app.users (
    user_id serial,
    name text,
    created_at timestamptz,
    deleted_at timestamptz,
    updated_at timestamptz,
    constraint pk_users primary key (user_id)
);

insert into app.users(name, created_at, updated_at)
values 
	('Jon', '2018-07-19 14:00:00', '2018-07-19 14:00:00'),
	('Garrett', '2018-07-10 14:00:00', '2018-07-19 14:00:00');

comment on table app.users is 'People who have registered on our site.';
comment on column app.users.user_id is 'Primary Key. Meaningless value to the business.';
comment on column app.users.name is 'User name as entered by the user.';
comment on column app.users.created_at is 'Date row was created.  Not for business use.';
comment on column app.users.deleted_at is 'Date row was deleted.  Not for business use.';
comment on column app.users.updated_at is 'Date row was updated.  Not for business use.';
