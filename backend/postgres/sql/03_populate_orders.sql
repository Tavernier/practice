insert into app.orders(
	order_id,
	order_number,
	amount_paid,
	product_category
)
select 
	o.order_id,
	'P' || lpad(o.order_id::text, 9, '0') as order_number,
	cast((random() * 100 + 1) * 100 as integer) as amount_paid,
	(select array['coffee', 'donuts', 'bagels', 'tea'])[floor((random()*3))::int + 1] as product_category
from (
	select *
	from generate_series(1, 50000) order_id
) o;
