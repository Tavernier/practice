create table app.orders (
	order_id integer,
	order_number varchar(20) not null,
	amount_paid integer not null,
    product_category varchar(20) not null,
	constraint pk_orders primary key (order_id),
	constraint uq_orders_order_number unique (order_number)
);

comment on table app.orders is 'Purchases made by buyers.';
comment on column app.orders.order_id is 'Primary Key. Meaningless value to the business.';
comment on column app.orders.order_number is 'Order Number. Public ID for this order. Can share with other systems.';
comment on column app.orders.amount_paid is 'Amount paid by buyer in USD cents.';
comment on column app.orders.product_category is 'Product Category to which this purchase belongs.';
