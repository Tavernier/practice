# Backend Stuff

The purpose of these services is to standup re-usable backends. Other tools can join the same network to interact with these backends.

Create the common networks via `../create_networks.sh` prior to starting these components via `./up.sh`.
```
