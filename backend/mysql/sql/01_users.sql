use app;

create table app.users (
    user_id int auto_increment comment 'Primary Key. Meaningless value to the business.',
    name text comment 'User name as entered by the user.',
    created_at timestamp comment 'Date row was created.  Not for business use.',
    deleted_at timestamp comment 'Date row was deleted.  Not for business use.',
    updated_at timestamp comment 'Date row was updated.  Not for business use.',
    constraint pk_users primary key (user_id)
)
    comment = 'People who have registered on our site.'
;

insert into app.users(name, created_at, updated_at)
values 
	('Jon', '2018-07-19 14:00:00', '2018-07-19 14:00:00'),
	('Garrett', '2018-07-10 14:00:00', '2018-07-19 14:00:00');
