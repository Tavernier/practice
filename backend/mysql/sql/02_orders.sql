use app;

create table app.orders (
	order_id integer comment 'Primary Key. Meaningless value to the business.',
	order_number varchar(20) not null comment 'Order Number. Public ID for this order. Can share with other systems.',
	amount_paid integer not null comment 'Amount paid by buyer in USD cents.',
    product_category varchar(20) not null comment 'Product Category to which this purchase belongs.',
	constraint pk_orders primary key (order_id),
	constraint uq_orders_order_number unique (order_number)
)
    comment 'Purchases made by buyers.'
;
