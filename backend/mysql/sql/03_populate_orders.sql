insert into app.orders(
	order_id,
	order_number,
	amount_paid,
	product_category
)
select 
	o.order_id,
    concat('P', lpad(o.order_id, 9, '0')) as order_number,
    (rand() * 100 + 1) * 100 as amount_paid, -- cents, not dollars
    (select category from (select 'coffee' union all select 'donuts' union all select 'bagels') c(category) order by rand() limit 1) as category
from (
	-- generate N rows so we can create fake data.
	select row_number() over() as order_id
	from information_schema.columns c1
	cross join information_schema.columns c2
	limit 50000
) o;
