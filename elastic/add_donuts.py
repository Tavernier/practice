#! /usr/bin/env python3
"""Add a donuts-eaten index and populate it with fake data over last 24 hours."""

import argparse
import collections
from datetime import datetime, timedelta
import random
import sys

from elasticsearch import Elasticsearch

def get_random_location():
    city = collections.namedtuple("City", "name, lat, lon")
    locations = [
        city("Atlanta", 33.7490, 84.3880),
        city("Bozeman", 45.6770, 111.0429),
        city("Chicago", 41.8781, 87.6298),
        city("Detroit", 42.3314, 83.0458),
        city("Hammond", 41.5834, 87.5000),
        city("Madison", 43.0731, 89.4012)
    ]
    return random.choice(locations)

def yield_donuts(number_to_yield=10000):
    """Yield N donuts eaten logs at a random time within the last 24 hours."""
    for _ in range(number_to_yield):
        location = get_random_location()
        yield {
            "city": location.name,
            "location": {
                "lat": location.lat,
                "lon": location.lon
            },
            "donuts_eaten": random.randint(1, 50),
            "created_at": datetime.utcnow() - timedelta(seconds=random.randint(0,86400*24))
        }

def setup_index():
    """Create and configure the index mappings.

    Mappings cannot be modified after creation.
    Failing to add location as a geo_point here means
    location will be added as an object with its own
    fields.  ES will not recognize the data as a geo_point
    unless the mapping below is added prior to any data
    being added to the index in that field.

    References:
    - https://www.elastic.co/guide/en/elasticsearch/reference/6.4/geo-point.html
    - https://stackoverflow.com/questions/44435624/can-t-merge-a-non-object-mapping-with-an-object-mapping-error-in-machine-learnin
    """
    es = Elasticsearch()

    if es.indices.exists("donuts-eaten") == False:
        es.indices.create(
            index="donuts-eaten",
            body={
                "mappings": {
                    "donuts": {
                    "properties": {
                        "location": {
                        "type": "geo_point"
                        }
                    }
                    }
                }
            }
        )

def write_logs_to_es(number_of_entries=10000):
    """Create and populate a donuts-eaten index."""
    es = Elasticsearch()
    for log in yield_donuts(number_of_entries):
        es.index(index="donuts-eaten", doc_type="donuts", body=log)
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog="Donuts Eaten",
        description="Create a donuts-eaten index and populate it with fake data over the last 24 hours from now."
        )

    parser.add_argument(
        "--number_of_entries",
        "-n",
        default=10000,
        type=int,
        help="Number of fake entries to add."
        )

    args = parser.parse_args()

    sys.stderr.write(f"Adding {args.number_of_entries} donuts eaten logs.\n")
    setup_index()
    write_logs_to_es(args.number_of_entries)
    sys.stderr.write("Done!\n")
