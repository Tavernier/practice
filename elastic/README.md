# Elastic

From their website:

* So, what is the ELK Stack? "ELK" is the acronym for three open source projects: Elasticsearch, Logstash, and Kibana.
* Elasticsearch is a search and analytics engine. 
* Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch. 
* Kibana lets users visualize data with charts and graphs in Elasticsearch. 

References:

* Docker images: https://www.docker.elastic.co/

## Tutorial

Let's try standing up Elasticsearch and Kibana.  I do not think Logstash is required, but I could be wrong. As of September 2018, [Elastic's guide](https://www.elastic.co/start) shows just Elasticsearch and Kibana.

Elastic shows its Docker Images on its site and not on Docker Hub.  See: https://www.docker.elastic.co/

Let's pick some images and look at some documentation to see launch values.

* Elasticsearch
    * Image: `docker.elastic.co/elasticsearch/elasticsearch:6.4.1`
    * Doc: https://www.elastic.co/guide/en/elasticsearch/reference/6.4/docker.html
* Kibana
    * Image: `docker.elastic.co/kibana/kibana:6.4.1`
    * Doc: https://www.elastic.co/guide/en/kibana/6.4/docker.html

I will use the information there to create a Docker Compose file.

## Usage

- Standup with `docker-compose up --build`.
- Visit kibana at `http://localhost:15601`.
- Run `./hello_world.py` to create some sample data and confirm connectivity.

## Playing Around

- Run `./add_donuts.py -n 1000` to add some fake data on the donuts-eaten index.
- Once data exists in Elasticsearch, add an Index Pattern in Kibana so data can be explored.
    - Remember to refresh the Index Pattern's fields after new fiels are added to the ES index itself.
    - For locations, be sure to add the mapping type to the index before any location is added to the index itself.  Mappings cannot be changed after data is added.
        - Kibana seems to goof up this display.  I triply confirmed Chicago's lat is 41.876465 and lon is -87.621887.  Kibana places this geo point in Asia.  I am unsure what I'm missing here.  I tried the object, string, and array representations of a geo_point and accounted for the reverse order in the array representation.
