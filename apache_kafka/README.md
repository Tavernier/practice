# Apache Kafka

Let's standup Confluent's open source version and play around with it.

## Components

1. Zookeeper
    - Maintains and coordinates Brokers.
    - Docker Image: `confluentinc/cp-zookeeper`
1. Broker 01
    - I am starting with a single broker.
    - Docker Image: `confluentinc/cp-kafka`
1. Confluent Schema Registry
    - Store and retrieve Avro schemas.
    - Docker Image: `confluentinc/cp-schema-registry`
    - "It provides serializers that plug into Kafka clients that handle schema storage and retrieval for Kafka messages that are sent in the Avro format."
    - [Documentation](https://docs.confluent.io/current/schema-registry/docs/index.html)
    - [Nice Overview](https://dzone.com/articles/kafka-avro-serialization-and-the-schema-registry)
1. Confluent REST Proxy
    - Makes it easy to:
        - produce and consume messages
        - view the cluster's state
        - perform administrative actions
    - Docker Image: `confluentinc/cp-kafka-rest`
    - Requires:
        - ZooKeeper
        - Schema Registry
    - [Documentation](https://docs.confluent.io/current/kafka-rest/docs/index.html)
1. Application
    - The `./poc` image will be launched as a container so I can interact with kafka on the same compose network.
    - I'll explore interacting directly with the broker and Confluent's pieces...schema registry and whatnot.
    - The `./poc/app` files will mount to the container's `/app` path.

## Clients

1. `kafka-python`
    - I started with this client to test basic connectivity, producing, and consuming.
    - I then moved on to using Confluent's client to interact with the cluster and their components.
    - [Repo](https://github.com/dpkp/kafka-python)
1. `confluent-kafka-python`
    - [Usage](https://docs.confluent.io/current/clients/confluent-kafka-python/index.html)
    - [Repo](https://github.com/confluentinc/confluent-kafka-python)

## Helpers

- `./up.sh`: Launch the Kafka cluster and application container.
- `./down.sh`: Nuke everything.
- `./app.sh`: Shell into the running application container to play around with `./poc/app` code.

## To Do

- Add REST proxy interaction scripts (put, get data. see topics, etc.)
