#! /usr/bin/env python3
"""Submit a city to its topic."""

import os
import json

from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError

import avro_helpers

registry = os.environ['SCHEMA_REGISTRY']

consumer = AvroConsumer({
        'bootstrap.servers': os.environ['KAFKA_BROKER'],
        'schema.registry.url': f'http://{registry}',
        'group.id': 'howdy'
    }
)

consumer.subscribe(['cities'])

while True:
    try:
        msg = consumer.poll(10)

    except SerializerError as e:
        print("Message deserialization failed for {}: {}".format(msg, e))
        break

    if msg is None:
        print("No message.")
        continue

    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            print("Yikes!")
            continue
        else:
            print(msg.error())
            break

    print(msg.value())

c.close()
