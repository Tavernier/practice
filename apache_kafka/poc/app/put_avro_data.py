#! /usr/bin/env python3
"""Submit a city to its topic."""

import os
import json

from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer

import avro_helpers

registry = os.environ['SCHEMA_REGISTRY']

producer = AvroProducer(
    {
        'bootstrap.servers': os.environ['KAFKA_BROKER'],
        'schema.registry.url': f'http://{registry}'
    }
)

# i'm unsure why i need to pass the schema here
# when schema registry already has a registered schema.
producer.produce(
    topic='cities', 
    value={'id': '100', 'city': 'Chicago', 'state': 'IL'}, 
    value_schema=avro.loads(json.dumps(avro_helpers.data_schema))
)
producer.flush()
