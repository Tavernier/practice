#! /usr/bin/env python3

data_schema = {
        "namespace": "blah.kafka.test",
        "type": "record",
        "name": "city",
        "fields": [
            {
                "name": "id",
                "type": "string"
            },
            {
                "name": "city",
                "type": "string"
            },
            {
                "name": "state",
                "type": "string"
            }
        ]
    }
