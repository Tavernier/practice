#! /usr/bin/env python3
"""Register a schema in Confluent's schema registry."""

import os
import json
import requests

from confluent_kafka import avro

import avro_helpers

registry = os.environ['SCHEMA_REGISTRY']

resp = requests.post(
    f'http://{registry}/subjects/cities/versions',
    json={"schema":json.dumps(avro_helpers.data_schema)},
    headers={"Content-Type": "application/vnd.schemaregistry.v1+json"}
)

print(resp.text)
