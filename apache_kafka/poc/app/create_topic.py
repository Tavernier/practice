#! /usr/bin/env python3

import os

import kafka

client = kafka.client.KafkaClient(
    bootstrap_servers=os.environ['KAFKA_BROKER']
)

client.add_topic('test_topic')
