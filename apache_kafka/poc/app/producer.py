#! /usr/bin/env python3

import os

import kafka

# connects to kafka broker
producer = kafka.KafkaProducer(
    bootstrap_servers=os.environ['KAFKA_BROKER']
)

print("Sending messages")
for _ in range(100):
    print(".", end='')
    producer.send('test', b'some_message_bytes')

print("\nFlushing")
producer.flush()
