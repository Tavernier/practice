#! /usr/bin/env python3

import os

import kafka

consumer = kafka.KafkaConsumer(
    'test',
    bootstrap_servers=os.environ['KAFKA_BROKER'],
    auto_offset_reset='earliest',
    consumer_timeout_ms=1000
)

print("Consuming")
for msg in consumer:
    print(msg)

print("Done")
