#! /usr/bin/env python

import os

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions

DESTINATION_BUCKET_NAME = os.environ['BEAM_GCS_BUCKET']


# See https://medium.com/@brunoripa/apache-beam-a-python-example-5644ca4ed581
class Split(beam.DoFn):

    def process(self, element):
        city, state = element.split(",")

        return [{
            'city': city,
            'state': state
        }]

with beam.Pipeline(options=PipelineOptions()) as pipeline:
    print "Starting pipeline."
    
    # i'm unsure how Beam knows this file is a CSV.
    # i see no options for specifying delimiter, data types, etc.
    # https://beam.apache.org/releases/pydoc/2.9.0/apache_beam.io.textio.html
    cities = (
        pipeline 
        | 'ReadCities' >> beam.io.ReadFromText('/tmp/input_data/cities.csv')
    )

    # collections are immutable.
    # transform output new collections.  the original remains unchanged.

    # syntax will resemble:
    # [Final Output PCollection] = ([Initial Input PCollection] | [First Transform]
    #          | [Second Transform]
    #          | [Third Transform])

    # See for core transforms:
    # https://beam.apache.org/documentation/programming-guide/#core-beam-transforms


    class ChangeToUppercase(beam.DoFn):
        def process(self, element):
            return [element.upper()]

    uppercase_cities = (
        cities 
        | 'ConvertToUppercase' >> beam.ParDo(ChangeToUppercase())
    )

    # Let's write this result set to both GCS and BigQuery
    # Must have these environment variables set:
    #   BEAM_GCS_BUCKET = name of GCS bucket to which files will be written
    #   GOOGLE_APPLICATION_CREDENTIALS = absolute path to service account JSON file

    # note: since i conver
    uppercase_cities | beam.io.WriteToText(
        file_path_prefix="gs://{}/beam/cities/cities".format(DESTINATION_BUCKET_NAME),
        file_name_suffix=".csv"
    )

    # i do not like the behavior here.
    # beam waits 2.5 minutes before writing data.  i have not seen that behavior in the past.
    # i'm also guessing beam is using streaming inserts here.  the table's metadata shows zero rows,
    # but i do see data in the table.
    # Stackdriver logging shows the service account does this:
    #   list tables
    #   delete table (why? i specified CREATE_IF_NEEDED)
    #   insert table (creates the table)
    (
        uppercase_cities 
        | 'ConvertToDictionary' >> beam.ParDo(Split())
        | 'WriteToBigQuery' >> beam.io.WriteToBigQuery(
            project='fluted-bot-152217',
            dataset='temp',
            table='beam_cities',
            schema='city:string,state:string',
            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
            write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE)
    )


    print "Done."
