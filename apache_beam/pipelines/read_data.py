#! /usr/bin/env python

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions

with beam.Pipeline(options=PipelineOptions()) as pipeline:
    print "Starting pipeline."
    # i'm unsure how Beam knows this file is a CSV.
    # i see no options for specifying delimiter, data types, etc.
    # https://beam.apache.org/releases/pydoc/2.9.0/apache_beam.io.textio.html
    cities = pipeline | 'ReadCities' >> beam.io.ReadFromText('/tmp/input_data/cities.csv')
    print "Done."
