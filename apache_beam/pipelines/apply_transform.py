#! /usr/bin/env python

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions

with beam.Pipeline(options=PipelineOptions()) as pipeline:
    print "Starting pipeline."
    
    # i'm unsure how Beam knows this file is a CSV.
    # i see no options for specifying delimiter, data types, etc.
    # https://beam.apache.org/releases/pydoc/2.9.0/apache_beam.io.textio.html
    cities = pipeline | 'ReadCities' >> beam.io.ReadFromText('/tmp/input_data/cities.csv')

    # collections are immutable.
    # transform output new collections.  the original remains unchanged.

    # syntax will resemble:
    # [Final Output PCollection] = ([Initial Input PCollection] | [First Transform]
    #          | [Second Transform]
    #          | [Third Transform])

    # See for core transforms:
    # https://beam.apache.org/documentation/programming-guide/#core-beam-transforms


    class ChangeToUppercase(beam.DoFn):
        def process(self, element):
            return [element.upper()]

    uppercase_cities = cities | beam.ParDo(ChangeToUppercase())

    # we're done transforming the data so let's write out the final result set.
    uppercase_cities | beam.io.WriteToText(
        file_path_prefix='/tmp/output_data/cities',
        file_name_suffix=".csv"
    )

    print "Done."
