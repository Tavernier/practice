#! /usr/bin/env bash

build_image(){
    docker build ./image/ --tag jt_apache_beam
}

launch_and_connect(){
    docker run \
    --rm \
    -it \
    -v "$(pwd)/pipelines":/pipelines \
    -v "$(pwd)/input_data":/tmp/input_data \
    -v "$(pwd)/output_data":/tmp/output_data \
    jt_apache_beam \
    bash
}

main(){
    build_image
    launch_and_connect
}

main
