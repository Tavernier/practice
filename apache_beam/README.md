# Apache Beam

https://beam.apache.org/documentation/programming-guide/

## Concepts

I'm using the [Apache Beam Programming Guide](https://beam.apache.org/documentation/programming-guide/).

- Pipelines
    - "A Pipeline encapsulates your entire data processing task, from start to finish."
    - Must provide options when creating a pipeline.
- Collections
    - A collection is "a distributed data set that your Beam pipeline operates on."
    - Collections are the inputs and outputs for each step in the pipeline.
    - Collections can be created from external data sources or from in-memory data.
        - Bounded Collection = fixed source like a file
        - Unbounded Collection = a continuously updating source like a stream
- Transforms
    - A transform "represents a data processing operation, or a step, in your pipeline."
    - Input: one or more Collections
    - Output: zero or more Collections
    - IO Transforms = "read or write data to various external storage systems."

## Syntax

Apache Beam is the first time I have seen syntax in Python like this:

```python
lines = p | 'read' >> ReadFromText(known_args.input)
...
counts = (lines
            | 'split' >> (beam.ParDo(WordExtractingDoFn())
                          .with_output_types(unicode))
            | 'pair_with_one' >> beam.Map(lambda x: (x, 1))
            | 'group' >> beam.GroupByKey()
            | 'count' >> beam.Map(count_ones))
```

1. What the heck is `|` doing here?
    - "In Beam, `|` is a synonym for `apply`, which applies a `PTransform` to a `PCollection` to produce a new `PCollection`."
    - [Reference](https://stackoverflow.com/questions/43796046/explain-apache-beam-python-syntax)
1. What the heck is `>>` doing here?
    - "`>>` allows you to name a step for easier display in various UIs -- the string between the `|` and the `>>` is only used for these display purposes and identifying that particular application."
    - Same reference as above.
1. What visualization tools exist?

## Learnin'

### Collections

1. How does Beam know I'm working with a CSV when reading (e.g. `beam.io.ReadFromText('data.csv')`)?
    - It does not it appears.
    - See https://stackoverflow.com/questions/41170997/how-to-convert-csv-into-a-dictionary-in-apache-beam-dataflow

### Transforms

1. How can I write to BigQuery?
    - [See this guide](https://beam.apache.org/releases/pydoc/2.4.0/_modules/apache_beam/io/gcp/bigquery.html).
    - [This guide is good too.](https://beam.apache.org/documentation/io/built-in/google-bigquery/#writing-to-bigquery).
    - Beam raised a warning: "Sleeping for 150 seconds before the write as BigQuery inserts can be routed to deleted table for 2 mins after the delete and create."
        - I saw this on first creation.  I have not experienced this behavior when working with BigQuery in the past.
    - Given the limitations that exist today compared to other tools I have used, I cannot recommend Beam for the use case of simply moving bytes from a source to BigQuery.  There are better tools to do that.
1. How can I write to Google Cloud Storage?
    - The programming guide shows this example: `beam.io.WriteToText('gs://some/outputData')`
1. How can I write newline delimited JSON files?
    - 
1. How does Apache Beam in GCP work?
    - Google offers Dataflow, which is their Beam-as-a-service solution.
    - It's serverless so that's nice.
    - We can submit existing jobs to the Dataflow runner.
    - Google provides this food for thought when wondering whether to use Beam:
        - Your job is compute-intensive.
        - Your data is not tabular and you cannot use SQL to do the analysis. (If it is tabular, use BigQuery). 
        - Large portions of the job are embarrassingly parallel -- in other words, you can process different subsets of the data on different machines.
        - Your logic involves custom functions, iterations, etc...
        - The distribution of the work varies across your data subsets. 
1. For GCS, what GCP credentials and authorization is needed?
    - Writing
        - At the bucket level, I granted my service account the Storage Object Admin role.
        - I initially tried just the Storage Object Creator and Viewer roles, but received permission errors because Beam also deletes data from GCS (temporary data created during the pipeline's execution).
1. For BigQuery, what GCP credentials and authorization is needed?
    - Writing
        - At the project level, I granted my service account the [BigQuery Metadata Viewer](https://cloud.google.com/bigquery/docs/access-control#permissions_and_roles) role.
        - At the dataset level, I granted my service account the [Edit role](https://cloud.google.com/bigquery/docs/access-control#dataset-primitive-roles).


## Fun Things to Know

- Only Python 2.7 is supported as of February 2019.
- In addition to the higher level concepts of Pipelines, Collections, and Transforms, it seems that we need to also concern ourselves with source and sink concepts at a lower level.
