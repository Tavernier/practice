

/*
What is going on when replication lags behind master?

Likely culprit:

1. activity on the box is either blocking replication
2. or consuming so many resources replication slows down

i will use this guide:
https://www.percona.com/blog/2014/05/02/how-to-identify-and-cure-mysql-replication-slave-lag/

Two threads are important to replication and these existon the replica:

1. IO_THREAD = reads from master and applies changes to a local relay log
2. SQL_THREAD = reads from local relay log and applies those to the local database
    
So, what is behind? IO_THREAD or SQL_THREAD?

1. if IO_THREAD is behind, the replica doesn't even have all the data yet.
2. if SQL_THREAD is behind, replica has all the data, but can't keep up with writes.
*/

-- run this on upstream:
show master status;

-- run this on replica:
show slave status;

