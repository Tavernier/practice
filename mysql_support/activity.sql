-- https://dev.mysql.com/doc/refman/5.6/en/show-processlist.html
select 
 pl.id,			-- cnnnection identifier
 pl.user,		-- user who issues statement
 pl.host,		-- host of client issuing statement
 pl.db,			-- default db if one is selected (null otherwise)
 pl.command,	-- type of command executig
 pl.time,		-- time thread has been in its current state
 pl.time / 60 as time_minutes,
 pl.state,		-- what the thread is doing
 pl.info		-- statement executing (null if no statement executing)
from information_schema.processlist pl
where pl.command not in ('Sleep')
order by pl.time desc;
