
-- show table size information with estimated rows
select 
 t.table_schema,
 t.table_name,
 t.table_rows as estimated_rows,
 t.data_length,
 t.index_length,
 round(t.data_length / 1024 / 1024, 1) as data_size_MB,
 round(t.index_length / 1024 / 1024, 1) as index_size_MB
from information_schema.tables t
where t.table_type = 'BASE TABLE'
order by t.table_schema, t.table_name;
