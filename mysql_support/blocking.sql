SHOW FULL PROCESSLIST;

show slave status;

-- these only apply to innodb tables:

-- https://dev.mysql.com/doc/refman/5.6/en/innodb-trx-table.html
select 
 t.trx_id,
 t.trx_state, -- watch out for lock wait
 t.trx_started,
 t.trx_requested_lock_id,
 t.trx_wait_started,
 t.trx_weight,
 t.trx_mysql_thread_id,
 t.trx_query,
 t.trx_operation_state,
 t.trx_tables_in_use,
 t.trx_tables_locked,  -- how many tables this transaction is locking
 t.trx_lock_structs,
 t.trx_rows_locked		-- rows locked
from information_schema.innodb_trx t
limit 10;


-- locks requested, but not yet granted:
select *
from information_schema.innodb_locks l
limit 10;

-- one or more rows for each blocked transaction
select *
from information_schema.innodb_lock_waits w
limit 10;
