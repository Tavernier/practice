##########################################################################################
# Docker related functions and aliases
##########################################################################################

alias dc='docker-compose'
alias ..='cd ..'
alias ll='ls -l'

##########################################################################################
# git related functions and aliases
##########################################################################################
function git.nuke_merged_branches() {
    git branch | grep -v master | xargs git branch -d
}


##########################################################################################
# Global Helpers
##########################################################################################

function resolve_environment() {
    # Given a string, this function returns a full environment name.
    # E.g. "prd" returns "production"
    
    requested_env=$1

    case $requested_env in
      playground|play)
          resolved_environment='playground'
          ;;
      staging|stage|stg)
          resolved_environment='staging'
          ;;
      production|prod|prd)
          resolved_environment='production'
          ;;
    esac

    echo ${resolved_environment}
}

function zip.encrypt() {
    # Given a filepath, this function will encrypt that file with a GUID as its password.
    # The original file is removed.
    # E.g. zip.encrypt ./blah.txt will create blah.txt.zip
    file_to_encrypt=$1

    output_filename="${file_to_encrypt}.zip"

    encryption_key=$(python3 -c 'import uuid; print(str(uuid.uuid4()))')

    zip --encrypt --password ${encryption_key} ${output_filename} ${file_to_encrypt} && rm ${file_to_encrypt}

    echo "Encryption key: ${encryption_key}"
    echo "Encrypted file: ${output_filename}"
    file ${output_filename}
}

function check_for_install(){
    application=$1

    local location=$(which ${application})

    if [ -d "$application" ]
    then
        echo "FOUND ${application}"
    elif [ -z "$location" ]
    then
        echo "MISSING ${application}"
    else
        echo "FOUND ${application} in ${location}"
    fi

}

function check_common_apps(){

    echo "####################################################"
    echo "Applications"
    echo "####################################################"
    for app in BBEdit Firefox "Google Drive File Stream" KeePassXC MySQLWorkbench \
                "pgAdmin 4" Postman Slack VirtualBox "Visual Studio Code"
    do
        check_for_install "/Applications/${app}.app"
    done

    echo
    echo "####################################################"
    echo "Python and Coding"
    echo "####################################################"
    for app in code git python python3 pip pip3 virtualenv
    do
        check_for_install ${app}
    done

    echo
    echo "####################################################"
    echo "Docker, k8s, etc."
    echo "####################################################"
    for app in docker docker-compose helm kubectl minikube
    do
        check_for_install ${app}
    done
    
    echo
    echo "####################################################"
    echo "Cloud Utilities"
    echo "####################################################"
    for app in bq gcloud gsutil
    do
        check_for_install ${app}
    done

    echo
    echo "####################################################"
    echo "Databases"
    echo "####################################################"
    for app in mysql psql sqlite3
    do
        check_for_install ${app}
    done

    echo
    echo "####################################################"
    echo "Miscellaneous Utilities"
    echo "####################################################"
    for app in brew cut gzip jq less parallel zip
    do
        check_for_install ${app}
    done
}


##########################################################################################
# Database functions
##########################################################################################

# I use these to launch instances of MySQL / Postgres locally in Docker to play around
# with specific database engine versions. 

function mysql.docker.destroy() {
    echo "Killing mysq-testing container"
    docker rm --force mysq-testing
}

# launch a MySQL version to play around with
function mysql.docker.redeploy() {
    version_to_launch_input=$1
    version_to_launch=${version_to_launch_input:-'5.6.35'}
    
    image_to_pull="mysql:${version_to_launch}"
    
    mysql.docker.destroy
    
    echo "Attempting to launch MySQL ${version_to_launch} from image ${image_to_pull}"
    
    docker run --name mysq-testing -e MYSQL_ROOT_PASSWORD=blah -d -p 13306:3306 --rm $image_to_pull
    
    sleep 5
    
    docker ps --filter name=mysq-testing
    
    echo "If things appear to have gone well..."
    echo "- connect to localhost:13306"
    echo "- user: root"
    echo "- pass: blah"
}

function postgres.docker.destroy() {
    echo "Killing postgres-testing container"
    docker rm --force postgres-testing
}

# launch a Postgres version to play around with
function postgres.docker.redeploy() {
    version_to_launch_input=$1
    version_to_launch=${version_to_launch_input:-'9.6.6'}
    
    image_to_pull="postgres:${version_to_launch}"
    
    postgres.docker.destroy
    
    echo "Attempting to launch Postgres ${version_to_launch} from image ${image_to_pull}"
    
    docker run --name postgres-testing -e POSTGRES_PASSWORD=blah -d -p 25432:5432 --rm $image_to_pull
    
    sleep 5
    
    docker ps --filter name=postgres-testing
    
    echo "If things appear to have gone well..."
    echo "- connect to localhost:25432"
    echo "- user: postgres"
    echo "- pass: blah"
}
