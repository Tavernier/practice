# Bash Functions

## Setup

```bash
GIT_HOME=/your/path/to/local/git

if [ -f "${GIT_HOME}/practice/bash/functions.sh" ]
then
    source "${GIT_HOME}/practice/bash/functions.sh"
fi
```