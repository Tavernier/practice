#! /usr/bin/env python3
"""Compare two dataframes.

The scenario here is:
- Extract a data set from System A.
- Extract the same data set from System B.
- Compare System A to System B to see what data is in sync.

Example
- Count orders by order date in both System A and System B.
- So, we get one row per day vs. millions of rows.
- Compare the row counts on each day between System A and System B.
"""

import argparse
import pandas as pd


def get_dataframe(filename, index):
    """Given a json file, return a dataframe with the provided index."""
    # import as strings since some techs output json numbers
    # with quotes and others do not.
    df = pd.read_json(filename, dtype=str)
    df = df.set_index(index)

    return df


def compare_dataframes(first_dataframe, second_dataframe, index, sort_columns=True):
    """Given two dataframes, compare them and return a dataframe with the results of that comparison."""

    combined_dataframe = first_dataframe.join(
        other=second_dataframe,
        on=index,
        how='outer',
        lsuffix='_a',
        rsuffix='_b'
    )

    # use the first dataframe to determine the list of columns
    # to compare in the first and last
    for column in first_dataframe.columns:
        combined_dataframe[f"{column}_match"] = combined_dataframe[f"{column}_a"] == combined_dataframe[f"{column}_b"]

        if column.endswith("_ct"):
            combined_dataframe[f"{column}_match_diff"] = pd.to_numeric(combined_dataframe[f"{column}_a"]) - pd.to_numeric(combined_dataframe[f"{column}_b"])

    # add a helper column to indicate whether all _match columns are True
    combined_dataframe['all_columns_match'] = combined_dataframe[[x for x in combined_dataframe.columns if x.endswith("_match")]].all(axis=1)

    # sort columns alphabetically so the display
    # shows the A file vs B file nicely.
    if sort_columns:
        combined_dataframe.sort_index(axis=1, inplace=True)

    return combined_dataframe


df1 = get_dataframe('./sample_data/one.json', 'date_id')
df2 = get_dataframe('./sample_data/two.json', 'date_id')

df_combined = compare_dataframes(df1, df2, 'date_id')

print(df_combined.head())

# write to a file for later visualization
df_combined.to_json(
    './output/results.json',
    orient='index'
    )
