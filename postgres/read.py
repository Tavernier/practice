#! /usr/bin/env python3

import textwrap

import psycopg2

with psycopg2.connect(
        dbname='postgres',
        user='postgres',
        password='testing',
        host='localhost',
        port='25432'
    ) as conn:

    with conn.cursor() as cur:
        
        # query and query params
        cur.execute(
            textwrap.dedent("""\
            select current_timestamp as ct
            """),
            tuple()
        )

        for record in cur:
            print(record[0])
