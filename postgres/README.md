# Postgres Practice

Let's play around with different postgres versions.

## Usage

- I named each compose service with the Postgres version (e.g. `db96`).  [Launch](https://docs.docker.com/compose/reference/up/) all or only the ones we want.
- SQL included in the `init_*` dirs will be executed when the container launches.
    - "These initialization files will be executed in sorted name order as defined by the current locale..."
    - See the ["How to extend this image"](https://hub.docker.com/_/postgres/) section.
    - This is a good enough approach for now.

## pgAdmin

pgAdmin created a [Docker Image](https://hub.docker.com/r/dpage/pgadmin4/).  That is good to know and can be included in compose, but I prefer using my local installation.

```yaml
admin:
    image: dpage/pgadmin4
    ports:
    - "8088:80"
    environment:
      PGADMIN_DEFAULT_EMAIL: blah@blah.com
      PGADMIN_DEFAULT_PASSWORD: testing
    depends_on:
    - db
```