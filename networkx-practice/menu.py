#! /usr/bin/env python3
"""Play around with a menu as a graph.

Graph, Nodes, and Edges methods:
https://networkx.org/documentation/stable/reference/functions.html#graph
"""

import json
import logging

import networkx as nx


def get_source_data():
    """Returns a list of dictionaries with parents defined."""

    # here's a variable node depth menu represented
    # as a list of dictionaries.  i added indents
    # to better understand the structure.
    return [
        {
            "category_id": "C0",
            "title": "Top"
        },
        {
            "category_id": "C10100",
            "title": "Software",
            "parent_category_id": "C0",
        },
        {
            "category_id": "C9225435",
            "title": "Games",
            "parent_category_id": "C10100",
        },
            {
                "category_id": "C2130521",
                "title": "Mario",
                "parent_category_id": "C9225435",
            },
            {
                "category_id": "C0284732",
                "title": "Sports",
                "parent_category_id": "C9225435",
            },
                {
                    "category_id": "C63492723",
                    "title": "Pro Football 2.0",
                    "parent_category_id": "C0284732",
                },
        {
            "category_id": "C8234092",
            "title": "Work",
            "parent_category_id": "C10100",
        },
            {
                "category_id": "C0185723",
                "title": "Super Document Writer 3.0",
                "parent_category_id": "C8234092",
            },
            {
                "category_id": "C0184723",
                "title": "Super Spreadsheet 3.0",
                "parent_category_id": "C8234092",
            },
        {
            "category_id": "C10200",
            "title": "Hardware",
            "parent_category_id": "C0",
        },
            {
                "category_id": "C10200-0A",
                "title": "Desktops",
                "parent_category_id": "C10200",
            },
            {
                "category_id": "C10200-0B",
                "title": "Laptops",
                "parent_category_id": "C10200",
            }
    ]


def get_nodes(taxonomy):
    """Returns networkx nodes from a list of dicts."""
    return [(x["category_id"], x) for x in taxonomy]


def get_edges(taxonomy):
    """Return source to target edges from a list of dicts."""
    return [
            (x["parent_category_id"], x["category_id"])
            for x in taxonomy
            if "parent_category_id" in x
        ]


def enrich_breadcrumbs(breadcrumbs, nodes):
    """Take a list of Node IDs and replace with Nodes."""
    enriched_breadcrumbs = dict()
    for node_id, crumbs in breadcrumbs.items():
        enriched_breadcrumbs[node_id] = [
            nodes.get(x)
            for x in crumbs
        ]
    return enriched_breadcrumbs


def enrich_tree(tree, nodes):
    """Recursively traverse the Tree and add Node attributes."""
    tree.update(nodes.get(tree["id"]))
    for child in tree.get("children", []):
        enrich_tree(child, nodes)
    return tree

# shortcut for showing json blobs
show = lambda x: print(json.dumps(x, indent=1))

def title(x):
    """Make viewing console output easier."""
    print("-" * 80)
    print(x)
    print("-" * 80)


def main():
    logging.basicConfig(level=logging.DEBUG)

    title("Getting and preparing source data.")
    taxonomy = get_source_data()
    nodes = get_nodes(taxonomy)
    edges = get_edges(taxonomy)

    title("Adding nodes and edges to graph.")
    menu = nx.DiGraph(title="Web Taxonomy") # Directed Graph
    menu.add_nodes_from(nodes)
    menu.add_edges_from(edges)

    title("Showing Graph")
    print(nx.algorithms.dag.is_directed_acyclic_graph(menu))
    # shows all Software children but no relationship data.
    # a set is returned.
    print(nx.algorithms.dag.descendants(menu, "C10100"))

    # this looks fantastic for getting breadcrumbs
    # the need here is given a node, get the path to it from the top.
    breadcrumbs = nx.algorithms.shortest_paths.generic.shortest_path(menu, source="C0")
    enriched_breadcrumbs = enrich_breadcrumbs(breadcrumbs, menu.nodes)
    title("breadcrumbs")
    show(breadcrumbs)

    title("enrichedbreadcrumbs")
    show(enriched_breadcrumbs)

    title("menu as nested tree")
    try:
        tree = nx.algorithms.traversal.depth_first_search.dfs_tree(menu, source="C0")
        json_tree = nx.readwrite.json_graph.tree_data(tree, root="C0")
        show(json_tree)
    except Exception as e:
        logging.error("Unable to parse into tree.")
        logging.error(str(e))

    title("enriched tree")
    enriched_tree = enrich_tree(json_tree, menu.nodes)
    show(enriched_tree)

    title("node info")
    # many of the functions return just the node id
    # so the need here is to get info about that node.
    show(menu.nodes.get("C10200-0A"))

if __name__ == "__main__":
    main()
