#! /usr/bin/env python3
"""Indexing documents in mongodb."""

import random
import argparse

from pymongo import MongoClient
from faker import Faker

def create_user_id(id:int):
    return 'U' + str(id).zfill(7)


def populate_mongo_with_fake_data(users_to_create=100000):
    """Drops and populates collection with N fake users."""

    fake = Faker()

    with MongoClient('localhost', 27017) as client:

        database = client['my-database']
        
        database.drop_collection('index-testing')
        
        print(f"Adding {users_to_create} fake people")
        users = []
        for i in range(users_to_create):
            users.append(
                {
                    'first_name': fake.first_name(),
                    'last_name': fake.last_name(),
                    'user_id': create_user_id(i)
                }
            )

            if (i+1) % 50000 == 0:
                print("Inserting batch")
                database['index-testing'].insert_many(users)
                users = []

        del users

def add_indexes_to_collection():
    """Add indexes to the fake user collection."""

    with MongoClient('localhost', 27017) as client:

        database = client['my-database']

        print("Adding an index on user_id field")
        database['index-testing'].create_index(
            'user_id',
            name='ix_users_user_id',
            unique=True
        )

        print("Adding an index on last_name field")
        database['index-testing'].create_index(
            'last_name',
            name='ix_users_last_name'
        )

def show_some_users():
    """Display users with various find calls."""

    with MongoClient('localhost', 27017) as client:

        database = client['my-database']
    
        user_id = create_user_id(random.randint(1, 100000))
        print(f"Finding user {user_id}")
        print(database['index-testing'].find_one({'user_id': user_id}))

        print(f"Finding Wilkersons")
        print(database['index-testing'].find({'last_name': 'Wilkerson'}).count())
        print(database['index-testing'].find({'last_name': 'Wilkerson'}).explain())

        # without index in place on last_name: totalDocsExamined: 10000
        # without index in place on last_name: totalDocsExamined: 33 (N depends on fake data being loaded)


if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--data', action='store_true', help='Repopulate mongodb with fake data.')
    parser.add_argument('-i', '--index', action='store_true', help='Add indexes to collection.')
    
    args = parser.parse_args()

    if args.data:
        populate_mongo_with_fake_data()

    if args.index:
        add_indexes_to_collection()

    show_some_users()
