#! /usr/bin/env python3
"""Writing documents to mongodb."""

from pymongo import MongoClient

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    # this database will be created if it does not exist
    database = client['my-database']

    chicago = {'city': 'Chicago', 'state': 'IL'}

    # likewise, the cities collection will be created if it does not exist.
    # since we are testing, let's nuke the collection first so we don't get 
    # a bazillion documents when i run this over and over again.
    database.drop_collection('cities')
    city_id = database['cities'].insert_one(chicago)

    print(city_id.inserted_id)

    # bulk inserts are supported too
    cities = database['cities'].insert_many(
        [
            {'city': 'Madison', 'state': 'WI'},
            {'city': 'Buffalo', 'state': 'NY'},
            {'city': 'Boulder', 'state': 'CO'}
        ]
    )

    for insert_id in cities.inserted_ids:
        print(insert_id)
    
