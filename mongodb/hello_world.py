#! /usr/bin/env python3
"""Basic mongo connectivity."""

from pymongo import MongoClient

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    print(client)
    print(type(client))
