#! /usr/bin/env python3
"""Reading documents from mongodb."""

from pymongo import MongoClient

import helpers

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    # this database will be created if it does not exist
    database = client['my-database']

    # returns the first found city as a dictionary
    first_city = database['cities'].find_one()
    print(first_city)
    print(type(first_city))

    # only finds the first chicago entry.
    # pass a dictionary on which to search.
    # search by _id to get a specific document.
    first_chicago = database['cities'].find_one({'city': 'Chicago'})
    print(first_chicago)
    print(type(first_chicago))

    # we can read multiple documents too.
    helpers.heading('All cities')
    for city in database['cities'].find():
        print(city)

    # we can read multiple documents too and search on specific attributes.
    helpers.heading('Wisconsin cities')
    for city in database['cities'].find({'state': 'WI'}):
        print(city)

    # we can count, perform fancier searches, and add indexes too.
    # http://api.mongodb.com/python/current/tutorial.html#counting
