#! /usr/bin/env bash

docker run \
    --name mongo-test \
    --publish '27017:27017' \
    --rm \
    mongo:3.2.21-jessie
