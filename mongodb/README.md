# MongoDB

## Launching

* Just mongodb: `./launch.sh`
* mongodb with Mongo Express web interface: `docker-compose up`
    * then `open http://localhost:8081`
    * i stole the compose file from Docker Hub

## Terminology

* Mongo Term = Rough RDBMS Equivalent
* Database = Database
    * A single server can house multiple databases.
    * A database will be created when writing data.
* Collection = Table
* Document = Row
* Field = Column
* Embedded Documents = Joins
* _id = Primary Key

# Miscellaneous Questions

* How is indexing handled?
* How are transactions handled?
    * Single document?
    * Multiple documents?

## Python

* I will follow the `pymongo` [tutorial](http://api.mongodb.com/python/current/tutorial.html).
* RealPython also has a [nice tutorial](https://realpython.com/introduction-to-mongodb-and-python/).
* Mongo has great documentation too such as their [CRUD document](https://docs.mongodb.com/manual/crud/).

## System Administration and Performance

Questions I want to answer at some point...

* To where does mongodb write its data?
* What are some performance best practices?
    * See projection param on `find()` to select only needed data.
    * See `find()`'s `limit()` method for number of documents to return to client.
    * Implement covering queries.  Use explain to view query operations (e.g. number of documents scanned).
* How does mongo horizontally scale?
* How is document indexing handled?
* How does user authentication and authorization work?
* What document metadata exists?  Could we keep another system in sync?
    * Inserted timestamp?
    * Updated timestamp?
    * `_id` values of deleted documents?
