#! /usr/bin/env python3
"""Aggregation within mongodb.

Aggregation operations process data records and return computed results.
"""

import random
import pprint

from pymongo import MongoClient

import helpers

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    # this database will be created if it does not exist
    database = client['my-database']

    database.drop_collection('aggregation-example')

    donuts = []
    for i in range(10000):
        donuts.append(
            {
                'donuts': i,
                'category': random.choice(['chocolate', 'vanilla', 'strawberry', 'pecan'])
            }
        )

    database['aggregation-example'].insert_many(donuts)

    del donuts

    # stolen from mongo's site
    helpers.heading('Simple Aggregation')
    results = database['aggregation-example'].aggregate(
        [
            {'$match': {'category': 'chocolate'}},
            {'$group': {'_id': '$category', 'total': {'$sum': '$donuts'}}}
        ]
    )
    print(list(results))

    helpers.heading('Aggregate All')
    results = database['aggregation-example'].aggregate(
        [
            {'$group': {'_id': '$category', 'total': {'$sum': '$donuts'}, 'max': {'$max': '$donuts'}}}
        ]
    )
    pprint.pprint(list(results))


