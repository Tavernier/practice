#! /usr/bin/env python3
"""Deleting documents in mongodb."""

from pymongo import MongoClient
import bson.objectid

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    # this database will be created if it does not exist
    database = client['my-database']

    # let's start with a fresh collection.
    database.drop_collection('delete-test')

    document_id = database['delete-test'].insert_one({'title': 'Now you see me'})
    print(f'Document ID: {document_id}')
    
    # a delete_many method exists too.
    database['delete-test'].delete_one({'title': 'Now you see me'})

    print("Any documents found?")
    print(database['delete-test'].find().count())
    for doc in database['delete-test'].find():
        print(doc)