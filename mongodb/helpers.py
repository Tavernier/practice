#! /usr/bin/env bash

def heading(title, level=1):
    print('-' * int(80/level))
    print(title)
    print('-' * int(80/level))

def print_all_documents(collection):
    heading(f'All documents in {collection.name}')
    for doc in collection.find():
        print(doc)
