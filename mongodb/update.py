#! /usr/bin/env python3
"""Updating documents in mongodb."""

from pymongo import MongoClient

import helpers

# mongo's logs show this actually establishes a connection.
# other technologies might only establish a connection once
# an action is taken against it.
with MongoClient('localhost', 27017) as client:

    # this database will be created if it does not exist
    database = client['my-database']

    # let's start fresh since I'm playing around here
    database.drop_collection('update-test')

    # we'll modify these.
    # note that different update operators exist such as incrementing values
    # see: https://docs.mongodb.com/manual/reference/operator/update/
    database['update-test'].insert_many(
        [
            {'public_id': '100800', 'state': 'WI'},
            {'public_id': '300800', 'state': 'IL'}
        ]
    )

    # modify one document
    database['update-test'].update_one(
        {'public_id': '100800'},
        {'$set': {'city': 'Madison'}}
    )

    # modify all documents
    # no query criteria is provided
    database['update-test'].update_many(
        {},
        {'$set': {'country': 'USA'}}
    )

    helpers.print_all_documents(database['update-test'])
    
    # replace an entire document except its ID
    database['update-test'].replace_one(
        {'public_id': '300800'},
        {'public_id': '300800', 'state': 'NY', 'city': 'Buffalo', 'country': 'USA'}
    )

    helpers.print_all_documents(database['update-test'])
