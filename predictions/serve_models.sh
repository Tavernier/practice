#! /usr/bin/env bash

set -euxo pipefail

build_images(){
    docker-compose -f ./serve_models.yaml build
}

serve_r_model(){
    docker-compose -f ./serve_models.yaml run --rm -p "8050:8000" -d r_server
}

serve_python_model(){
    docker-compose -f ./serve_models.yaml run --rm -p "8060:5000" -d python_server
}


main(){
    build_images
    serve_r_model
    serve_python_model
}

main
