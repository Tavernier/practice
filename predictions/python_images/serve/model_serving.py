#! /usr/bin/env python3

import pickle
import yaml

import pandas as pd

from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)

@app.route("/echo")
def echo():
    return jsonify({"message": request.args.get('text')})


@app.route("/predict", methods=["POST"])
def predict_mdv():
    
    model = pickle.load(open('/trained_models/predict_housing_price.pickle', 'rb'))
    
    prediction_payload = {
        'CRIM': [request.json['crime_rate']], 
        'RM': [request.json['rooms']], 
        'TAX': [request.json['property_tax_rate']], 
        'LSTAT': [request.json['lower_status_population_percentage']]
    }

    predcited_value = model.predict(pd.DataFrame(data=prediction_payload))

    return jsonify(list(predcited_value))


def get_model_settings(settings_yaml="/trained_models/models.yaml"):
    contents = None

    with open(settings_yaml, 'r') as f:
        contents = yaml.safe_load(f)

    return contents


def load_model_from_disk(pickled_model_object):
    return pickle.load(open(pickled_model_object, 'rb'))

# let's try to dynamically predict
@app.route("/predict/<model>/<version>", methods=["POST"])
def dynamic_prediction(model, version):

    all_models = get_model_settings()

    try:
        model_filename = all_models[model][version]['model']
        inputs = all_models[model][version]['inputs']
    except KeyError:
        # return 404
        pass

    model = load_model_from_disk(f"/trained_models/{model_filename}")

    prediction_payload = {}

    for prediction_payload_key, request_json_key in inputs.items():
        prediction_payload[prediction_payload_key] = [request.json[request_json_key]]

    predcited_value = model.predict(pd.DataFrame(data=prediction_payload))

    return jsonify(list(predcited_value))

