#! /usr/bin/env python3

import pickle

import numpy as np
import pandas as pd
import sklearn
import sklearn.model_selection
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston

########################################
# Gather Input and Prepare Data
########################################

# Reference
# https://medium.com/@haydar_ai/learning-data-science-day-9-linear-regression-on-boston-housing-dataset-cd62a80775ef

boston = load_boston()

df = pd.DataFrame(boston.data)
df.columns = boston.feature_names
df['price'] = boston.target


########################################
# Train Model
########################################

# split into training and test data
X = df[['CRIM','RM','TAX','LSTAT']]
Y = df['price']

X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(
    X, Y, 
    train_size = 0.75, 
    random_state = 5
)

# create a model
lm = LinearRegression()
lm.fit(X_train, Y_train)


########################################
# Test Model
########################################
Y_pred = lm.predict(X_test)


########################################
# Save the Model
########################################
with open('/trained_models/predict_housing_price.pickle', 'wb') as f:
    pickle.dump(lm, f)

