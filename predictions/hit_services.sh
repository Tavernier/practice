#! /usr/bin/env bash

set -euo pipefail

get_echo(){
    local port=$1
    echo
    echo -n "Echo endpoint on port ${port}: "
    curl http://localhost:${port}/echo?text=howdy+there
    echo
    echo
}

get_prediction(){
    local port=$1
    echo
    echo -n "Predicted value on port ${port}: "
    curl \
        -X POST \
        -d '{"crime_rate": "0.145", "rooms": 2.4, "property_tax_rate": "150", "lower_status_population_percentage": "7.7"}' \
        -H 'Content-Type: application/json' \
        http://localhost:${port}/predict
    echo
    echo
}

get_dynamic_prediction(){
    local port=$1
    local version=$2
    echo
    echo -n "Dynamic predicted value on port ${port}: "
    curl \
        -X POST \
        -d '{"crime_rate": "0.145", "rooms": 2.4, "property_tax_rate": "150", "lower_status_population_percentage": "7.7"}' \
        -H 'Content-Type: application/json' \
        http://localhost:${port}/predict/house-prices/${version}
    echo
    echo
}

main(){
    echo "R echo endpoint"
    get_echo "8050"

    echo "Python echo endpoint"
    get_echo "8060"

    echo "R prediction endpoint"
    get_prediction "8050"

    echo "Dynamic R prediction endpoint v1"
    get_dynamic_prediction "8050" "v1"

    echo "Dynamic R prediction endpoint v2"
    get_dynamic_prediction "8050" "v2"

    echo "Python prediction endpoint"
    get_prediction "8060"

    echo "Dynamic Python prediction endpoint v1"
    get_dynamic_prediction "8060" "v1"

    echo "Dynamic Python prediction endpoint v2"
    get_dynamic_prediction "8060" "v2"
}

main