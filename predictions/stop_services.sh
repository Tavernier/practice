#! /usr/bin/env bash

set -euxo pipefail

main(){
    docker-compose -f ./serve_models.yaml rm --stop --force
}

main
