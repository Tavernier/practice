# Training and Serving Predictive Models

## Goal

- Train a predictive model in both R and Python.
- Load those models later on to serve real-time predictions.

## Background

I have often seen the [Boston Housing dataset](https://www.kaggle.com/c/boston-housing) used in "hello world" type exercices.  Let's try to predict a house's price using these input variables:

- `crime_rate`: per capita crime rate by town.
- `rooms`: average number of rooms per dwelling.
- `property_tax_rate`: full-value property-tax rate per $10,000.
- `lower_status_population_percentage`: lower status of the population (percent).

We will need to:

- Train and save models in both R and Python.
- Load those trained models later on and use them to make predictions.

References:

- [Predictions with Python](https://medium.com/@haydar_ai/learning-data-science-day-9-linear-regression-on-boston-housing-dataset-cd62a80775ef)
- [Predictions with R](https://www.r-exercises.com/2017/12/04/boston-regression-solutions/)

Notes:

- You will need Docker and Docker Compose for this exercise.
- I am not a data scientist and have no idea on how to evaluate whether these models are good.  Even if they are not, I still just want to prove out saving a model and using later on to make predictions.
- I hope the predictions are close to one another, but they will not be exact.
- The output format of the R and Python endpoints will be close, but they will not match.  I am not an R expert and did not investigate why responses are being put into a list.

## Short Version

The start-to-finish flow for this PoC is below.  Note that Docker Images will be built.  The R training image takes about 10 minutes.

1. Run `./train_models.sh` to train both the R and Python models.
1. Run `./serve_models.sh` to launch REST APIs for both the R and Python models.
1. Run `./hit_services.sh` to submit requests to both R and Python endpoints.
1. Run `./stop_services.sh` to stop the running Docker containers.

See more details below.

## Model Training

1. Run the `./train_models.sh` script to train both the R and Python models.
1. Trained models will be stored in `./trained_models/`.
    - The R model is materialized as an `rds` file.
    - The Python model is materialized as a `python` file.

## Model Serving

1. Train the models if you have not done so already (see above).
1. Run the `./serve_models.sh` script to launch REST APIs for both the R and Python models.
    - R is launched on `localhost:8050`.
    - Python is launched on `localhost:8060`.
1. Run the `./hit_services.sh` script to submit requests to both R and Python endpoints.
    - GET `/echo?text=whatever`
        - input
            - text = string
        - output
            - Responds with text value in json blob.
            - Note: the output format will be slightly different between R and Python.  I'm no R expert.
    - POST `/predict`
        - input
            - crime_rate = float
            - rooms = float
            - property_tax_rate = int
            - lower_status_population_percentage = float
        - output
            - Predicted home price given those four values.
            - Note: the output format will be slightly different between R and Python.  I'm no R expert.
    - POST `/predict/{model}/{version}`
        - This will hit the metadata-driven versions for prediction.
        - input
            - same as above
        - output
            - same as above
1. Stop the services with `./stop_services.sh` script.

### Metadata-Driven Model Serving

1. Python
    - See `./trained_models/python/models.yaml` for model settings in a yaml file.
        - The goal here is to have a model with diferent versions.
        - We have full control over the endpoint of course.  This is just an example.  Maybe we want to do something else with our approach to model versioning.
        - We may also want to enhance this settings file such as including data types for the input data, etc.
        - I'm using a linear regression in this exercise.  I have no idea how complicated other model inputs can get.
    - That file has model settings.
        - The different versions all produce the same prediction because I didn't actually train the model with different data or anything.  I just made multiple copies of the same pickled object.
    - The REST API has an endpoint `/predict/{model}/{version}`
        - I added no error checking.  In reality, we'd want errors such as:
            - 400 for missing required values or bad input
            - 404 for model or version that does not exist
            - 500 if pickled object not found
    - It'll take those values, the model name and version, pull in the appropriate pickled model object, parse the request json, and make a prediction.
1. R
    - See `./trained_models/r/models.yaml` for model settings in a yaml file.
        - This YAML conforms to the `config` library in R, which is why `default` is needed at the top.
    - The REST API has an endpoint `/predict/{model}/{version}`
    - The `dynamic_prediction` function in `./r_images/serve/serve_model.R` is not production ready, due to my lack of R knowledge.  These issues need to be addressed at the very least:
        - How we can dynamically assemble the prediction model's input data?
