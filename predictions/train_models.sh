#! /usr/bin/env bash

set -euxo pipefail

build_images(){
    docker-compose -f ./train_models.yaml build
}

train_r_model(){
    docker-compose -f ./train_models.yaml run --rm r_trainer '/app/model_training.R'
}

train_python_model(){
    docker-compose -f ./train_models.yaml run --rm python_trainer '/app/model_training.py'
}

main(){
    build_images
    train_r_model
    train_python_model
}

main
