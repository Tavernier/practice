#! /usr/bin/env bash

set -euxo pipefail

PORT=8090

function start_container(){
    docker run \
        -d \
        --name javascript-practice \
        -p ${PORT}:80 \
        -v "$(pwd)/scripts":/usr/share/nginx/html:ro \
        nginx:1.15.12-alpine
}

function open_site() {
    open "http://localhost:${PORT}"
}

function main(){
    start_container
    open_site
}

main
