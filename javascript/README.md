# JavaScript

I will work though [The Modern JavaScript Tutorial](https://javascript.info/).

## Basics

- A JavaScript engine executes scripts. The V8 engine is used in Chrome and Opera for example.
- Browsers will execute JavaScript code that exists in the `<script>` tag.  No need to specify a type or language attribute anymore.  Just the tag is enough.  External code can be added via the `src` attribute.
- Node can be used to execute server-side code.
- Use [JSFiddle](https://jsfiddle.net/) for quick testing.
- Add `"use strict"` to the top of code...it must be at the top. This enables ES5+ features, which were introduced in 2009.

## Recaps

- [Basics](https://javascript.info/javascript-specials)
