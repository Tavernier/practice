#! /usr/bin/env bash

set -euxo pipefail

docker stop javascript-practice \
&& docker rm javascript-practice
