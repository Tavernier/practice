# TypeScript

I'm going through [TypeScript's tutorial](https://www.typescriptlang.org/docs/). Most of the statements below are copied directly from that website.

- TypeScript checks a program for errors before execution, and does so based on the kinds of values, it’s a static type checker.
- TypeScript never changes the runtime behavior of JavaScript code.

## Getting Started

1. Install typescript via `npm install -g typescript`
    - `npm` is to install packages.
    - `npx` is to execute those packages.
1. Run `tsc {script}.ts` on your TypeScript file to create its matching `{script}.js` JavaScript file.
1. Run `node {script}.js` on the command line (no need to open Chrome for simple scripts).

```shell
tsc hello.ts
node hello.js
```

Since `tsconfig.json` exists, can also run `tsc` from this directory and all `examples/*.ts` files will be compiled.

`tsc` notes:

- Use the `--noEmitOnError` flag to avoid generating JavaScript. But by default, TypeScript assumes you will know best and generate JavaScript even when errors exist.
- Use the `--target es2015` flag to build JavaScript compatible down to that specified JavaScript version.
    - While the default target is ES3, the great majority of current browsers support ES2015. Most developers can therefore safely specify ES2015 or above as a target, unless compatibility with certain ancient browsers is important.
- Use [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html) to control cli settings too.
    - Use `noImplicitAny` and `strictNullChecks`
- The compiler tries to emit clean readable code that looks like something a person would write. While that’s not always so easy, TypeScript indents consistently, is mindful of when our code spans across different lines of code, and tries to keep comments around.

## The Basics

- Read and re-read this page on [type checking primitive types](https://www.typescriptlang.org/docs/handbook/2/everyday-types.html).
- [Objects can also be typed](https://www.typescriptlang.org/docs/handbook/2/objects.html).
- Many [Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html) exist too.
- Use [narrowing](https://www.typescriptlang.org/docs/handbook/2/narrowing.html) to modify code when you accept multiple data types such as `param: number | string`.
- See index.ts for DOM manipulation example.

## Local Development

Run `tsc --watch` to stay awake and compile changes.

## Build Pipeline

- [ESLint](https://palantir.github.io/tslint/) appears to be a popular linter.
- [Prettier](https://prettier.io/) appears to be a popular formatter.
- [Jest](https://jestjs.io/) appears to be popular for unit testing.
    - Run `npm test` since I added the scripts block to package.json per Jest's instructions.
    - I added the basic sum test per Jest's website and that worked as expected. Yay!
    - Seems like recommendation is to first compile TypeScript to JavaScript then run Jest.
