"use strict";

// Note the data type annotations. These will be removed when
// tsc compiles the code. Also, the compiled code may look
// different but will be functionally equivalent.
// Add a ? after the param name to make it optional.
function greet(person: string, date: Date): string {
  return `Hello ${person}, today is ${date.toDateString()}!`;
}

// Here's how to annotate variables.
// In most cases, though, this isn’t needed. Wherever possible,
// TypeScript tries to automatically infer the types in your
// code. For example, the type of a variable is inferred based
// on the type of its initializer.
// If you’re starting out, try using fewer type annotations
// than you think - you might be surprised how few you need
// for TypeScript to fully understand what’s going on.
let firstName: string = "Homer";

console.log(greet(firstName, new Date()));

// Here's an example of a Type Alias and its usage.
// Interfaces are also similar. Differences are documented here:
// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces
type Point = {
  x: number;
  y: number;
};
 
function printCoord(pt: Point) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}

printCoord({x: 120, y: 64});


// Interfaces and other types can be extended so you can
// re-use type information, such as in this example. You
// can also combine types and more.
interface BasicAddress {
  name?: string;
  street: string;
  city: string;
  state: string;
  country: string;
  postalCode: string;
}
 
interface AddressWithUnit extends BasicAddress {
  unit: string;
}

let shippingAddress: AddressWithUnit = {
  "street": "Main Street",
  "unit": "Apt 210",
  "city": "Bozeman",
  "state": "MT",
  "postalCode": "59717",
  "country": "USA"
}

console.log(shippingAddress);

// Must import from .js even though we're using TypeScript.
import helloWorld from "./greetings.js";
helloWorld();
