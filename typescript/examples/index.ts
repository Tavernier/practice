// 1. Select the div element using the id property
// document.getElementById function returns an HTMLElement
const app = document.getElementById("app");

// 2. Create a new <p></p> element programmatically
const p = document.createElement("p");

// 3. Add the text content
p.textContent = "Hello World!";

// 4. Append the p element to the div element
// ?. is a safe navigation operator. It prevents object paths from undefined or null reference errors.
app?.appendChild(p);
