# Jenkins

Jenkins exists everywhere.  By the time I join a company, Jenkins is established.  I want to understand its setup and create some "hello world" stuff such as piplines.

## Docker Image

- Image: `jenkins/jenkins:lts`
- [Documentation](https://github.com/jenkinsci/docker/blob/master/README.md)

## Launching

Getting up and running was painless:

1. `docker-compose up --build`
    - Grab the password shown in the container's output.
1. Jenkins is available on [http://localhost:8080](8080).
1. When I first logged in:
    - Jenkins asked me to install plugins.  I chose the default.  After finishing setup, I saw the [1.2 billion plugins installed](http://localhost:8080/pluginManager/installed).
    - I created an admin user.  Since I'm playing around here, the credentials are `jon/howdy`.

The `JENKINS_VERSION` environment variable shows `2.138.2` as the running version (see [system information](http://localhost:8080/systemInfo)).  Jenkins publishes its [version history](https://jenkins.io/changelog/).

### Cleanup

Nuke the container and leave the volume: `docker-compose down && docker-compose rm`

## Pipelines

- A pipeline is a series of steps to getting your code to environments.
    - I have seen Jenkins used for other stuff too such as data movement, which I think is odd if this is a CD tool.
- A `Jenkinsfile` defines the pipeline.
- A pipline can be written in either [declarative or scripted](https://jenkins.io/doc/book/pipeline/#pipeline-syntax-overview) fashion.
    - [Declarative](https://jenkins.io/doc/book/pipeline/syntax/) is newer.  At first glance, I understand its approach better so I will start there.

### Questions and Answers

1. How can a pipeline prompt for information?
    - Pipeline level: see the [parameters directive](https://jenkins.io/doc/book/pipeline/syntax/#parameters).
    - Stage level: see [inputs](https://jenkins.io/doc/book/pipeline/syntax/#input).
1. How can a pipline be created from git?
1. What types of steps exist?  My hello world uses [`sh`](https://jenkins.io/doc/pipeline/steps/workflow-durable-task-step/#-sh-%20shell%20script) for instance.
    - [Lots of steps](https://jenkins.io/doc/pipeline/steps/) are available.
    - I will start with [basic steps](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/).
1. In some repos, I see little content in the `Jenkinsfile` such as just a name and version.  How does that get used?
1. What options exists for setting environment variables and secrets in particular?
    - See the [enviornment directive](https://jenkins.io/doc/book/pipeline/syntax/#environment).
1. Can a Docker image be run as a stage or step?  I am able to do this in GitLab for example.
    - Yes, both an image or Dockerfile can be used as an [agent](https://jenkins.io/doc/book/pipeline/syntax/#agent).
1. What actions can start a pipline build?
1. How can a pipeline target many git repos?
    - For example, we have one at work that automatically builds Docker images when the repo's name starts with a particular prefix.


### Hello World

I created my first pipeline through the web interface.  I'll tackle git in a bit.  For now, behold!

```groovy
pipeline { 
    agent any 
    stages {
        stage('Build') { 
            steps { 
                sh 'echo "build stage"' 
            }
        }
        stage('Test'){
            steps {
                sh 'echo "test stage"'
            }
        }
        stage('Deploy') {
            steps {
                sh 'echo "deploy stage"'
            }
        }
    }
}
```

Build number one was an amazing success.  Here is its console output:

```
Started by user Jon
Resume disabled by user, switching to high-performance, low-durability mode.
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/hello world
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Build)
[Pipeline] sh
+ echo build stage
build stage
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Test)
[Pipeline] sh
+ echo test stage
test stage
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Deploy)
[Pipeline] sh
+ echo deploy stage
deploy stage
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```
