#! /usr/bin/env python3
"""cities: First In, First Out

Use collections.deque unless threading is needed.
Use queue.Queue when threading is needed.
See ./stacks_with_threads.py as an example.

deque: https://docs.python.org/3/library/collections.html#collections.deque
"""

from collections import deque
import logging


def main():
    cities = deque(["buffalo", "chicago", "madison"])
    logging.info(cities)
    logging.info([x for x in dir(cities) if not x.startswith("_")])

    work_on = cities.popleft()
    logging.info(work_on)

    work_on = cities.popleft()
    logging.info(work_on)
    logging.info(cities)

    cities.append("atlanta")
    logging.info(cities)

    cities.extend(["hammond", "new york"])
    logging.info(cities)

    # move entries to the right two places
    cities.rotate(2)
    logging.info(cities)

    while cities:
        city = cities.popleft()
        logging.info(f"Working on {city}")

    logging.info(cities)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
