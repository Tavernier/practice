#! /usr/bin/env python3
"""cities: Last In, First Out

Use collections.deque unless threading is needed.
Use queue.LifoQueue when threading is needed.

deque: https://docs.python.org/3/library/collections.html#collections.deque
"""

import logging
from queue import LifoQueue
import threading

cities_queue = LifoQueue()

def workon_on_queue():
    while True:
        city = cities_queue.get()
        logging.info(f"{city.title()} on thread {threading.get_ident()}")
        cities_queue.task_done()

def main():
    number_of_threads = 2
    logging.info(f"Starting {number_of_threads} worker threads")
    for _ in range(number_of_threads):
        worker = threading.Thread(target=workon_on_queue, daemon=True)
        worker.start()

    logging.info("Populating queue")
    cities = ["atlanta", "bozeman", "buffalo", "chicago", "madison"]
    for city in cities:
        cities_queue.put(city)
    
    logging.info("Waiting for all work to complete.")
    cities_queue.join()    

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info("hello")
    main()
    logging.info("bye!")