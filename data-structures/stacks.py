#! /usr/bin/env python3
"""cities: Last In, First Out

Use collections.deque unless threading is needed.
Use queue.LifoQueue when threading is needed.

deque: https://docs.python.org/3/library/collections.html#collections.deque
"""

from collections import deque
import logging


def main():
    cities = deque(["buffalo", "chicago", "madison"])
    logging.info(cities)
    logging.info([x for x in dir(cities) if not x.startswith("_")])

    work_on = cities.pop()
    logging.info(work_on)

    work_on = cities.pop()
    logging.info(work_on)
    logging.info(cities)

    cities.append("atlanta")
    logging.info(cities)

    cities.appendleft("bozeman")
    logging.info(cities)

    cities.extend(["hammond", "new york"])
    logging.info(cities)

    # move entries to the right two places
    cities.rotate(2)
    logging.info(cities)

    while cities:
        city = cities.pop()
        logging.info(f"Working on {city}")

    logging.info(cities)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
