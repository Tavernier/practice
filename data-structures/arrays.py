#! /usr/bin/env python3

import logging


def main():
    cities = ["atlanta", "bozeman", "chicago", "madison"]
    logging.info(cities)
    logging.info([x for x in dir(cities) if not x.startswith("_")])

    # add to the end
    cities.append("append city")
    logging.info(cities)

    # size of list and number of occurrences
    logging.info(len(cities))
    logging.info(cities.count("chicago"))

    # add many to the end
    cities.extend(["buffalo", "hammond"])
    logging.info(cities)

    # add at a specific index
    cities.insert(3, "inserted city")
    logging.info(cities)

    # sort alphabetically
    cities.sort()
    logging.info(cities)

    # sort descending alphabetically
    cities.reverse()
    logging.info(cities)

    # sort using custom function
    cities.sort(key=lambda x: len(x), reverse=True)
    logging.info(cities)

    # remove first occurrence of value
    cities.remove("madison")
    logging.info(cities)

    # remove and return a value at a specific index
    last_city = cities.pop() # defaults to last value
    logging.info(last_city)
    logging.info(cities)

    # return first index of value
    logging.info(cities.index("chicago"))

    # check whether something is in the list
    logging.info("chicago" in cities)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
