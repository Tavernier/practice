#! /usr/bin/env bash

set -euo pipefail

function start_webapp(){
    python -m gunicorn \
        wsgi:app \
        -w ${GUNICORN_WORKERS} \
        -b 0.0.0.0:8080
}

main(){
    start_webapp
}

main
