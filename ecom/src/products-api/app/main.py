#! /usr/bin/env python3

import json

from flask import Flask
from flask import abort, jsonify, request

app = Flask(__name__)

###############################################################################
# Products API
###############################################################################

def _load_products():
    app.logger.info("Getting products.")
    products = None
    with open("/app/products.json", "r") as f:
        products = json.load(f)
    app.logger.info("Done getting products.")
    return products


def _create_index(products, lookup_key):
    index = dict()
    app.logger.info(f"Indexing products on {lookup_key}.")
    if lookup_key == "product_id":
        index = {
            x["product_id"]: x
            for x in products
        }
    else:
        for product in products:
            index.setdefault(product[lookup_key], []).append(product)
    return index


PRODUCTS = _load_products()
PRODUCT_ID_INDEX = _create_index(PRODUCTS, "product_id")
CATEGORY_ID_INDEX = _create_index(PRODUCTS, "category_id")


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


@app.route("/")
def get_products():
    category_ids = request.args.getlist("category_id")
    app.logger.info(category_ids)
    found_products = []
    if category_ids:
        for category_id in category_ids:
            found_products += CATEGORY_ID_INDEX.get(category_id, [])
    else:
        found_products = PRODUCTS
    return jsonify(found_products)


@app.route("/<product_id>/")
def get_product(product_id):
    app.logger.debug(f"Got request for {product_id}")
    product = PRODUCT_ID_INDEX.get(product_id, None)
    if product is None:
        return abort(404, description=f"No product found with ID {product_id}")
    return jsonify(product)
