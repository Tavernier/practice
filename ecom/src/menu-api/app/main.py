#! /usr/bin/env python3

from flask import Flask
from flask import jsonify

app = Flask(__name__)

###############################################################################
# Home Page
###############################################################################

@app.route("/")
def get_menu():
    menu = [
            {
                "category_id": "C100",
                "parent_category_id": None,
                "title": "Games",
                "slug": "games"
            },
            {
                "category_id": "C200",
                "parent_category_id": None,
                "title": "Hardware",
                "slug": "hardware"
            },
            {
                "category_id": "C22639",
                "parent_category_id": "C200",
                "title": "Accessories",
                "slug": "accessories"
            },
            {
                "category_id": "C78766",
                "parent_category_id": "C100",
                "title": "Action & Adventure",
                "slug": "action-and-adventure"
            },
            {
                "category_id": "C56915",
                "parent_category_id": "C100",
                "title": "Arcade",
                "slug": "arcade"
            },
            {
                "category_id": "C60792",
                "parent_category_id": "C100",
                "title": "Baseball",
                "slug": "baseball"
            },
            {
                "category_id": "C80431",
                "parent_category_id": "C100",
                "title": "Basketball",
                "slug": "basketball"
            },
            {
                "category_id": "C18395",
                "parent_category_id": "C200",
                "title": "Controllers",
                "slug": "controllers"
            },
            {
                "category_id": "C37373",
                "parent_category_id": "C100",
                "title": "Extreme Sports",
                "slug": "extreme-sports"
            },
            {
                "category_id": "C36767",
                "parent_category_id": "C100",
                "title": "Fighting",
                "slug": "fighting"
            },
            {
                "category_id": "C34400",
                "parent_category_id": "C100",
                "title": "Football",
                "slug": "football"
            },
            {
                "category_id": "C79910",
                "parent_category_id": "C100",
                "title": "Other",
                "slug": "other"
            },
            {
                "category_id": "C64557",
                "parent_category_id": "C100",
                "title": "Party",
                "slug": "party"
            },
            {
                "category_id": "C54091",
                "parent_category_id": "C100",
                "title": "Puzzle",
                "slug": "puzzle"
            },
            {
                "category_id": "C18444",
                "parent_category_id": "C100",
                "title": "RPG",
                "slug": "rpg"
            },
            {
                "category_id": "C39238",
                "parent_category_id": "C100",
                "title": "Racing",
                "slug": "racing"
            },
            {
                "category_id": "C80864",
                "parent_category_id": "C100",
                "title": "Soccer",
                "slug": "soccer"
            },
            {
                "category_id": "C54023",
                "parent_category_id": "C100",
                "title": "Sports",
                "slug": "sports"
            },
            {
                "category_id": "C42631",
                "parent_category_id": "C100",
                "title": "Strategy",
                "slug": "strategy"
            },
            {
                "category_id": "C53090",
                "parent_category_id": "C200",
                "title": "Systems",
                "slug": "systems"
            },
            {
                "category_id": "C50518",
                "parent_category_id": "C100",
                "title": "Wrestling",
                "slug": "wrestling"
            }
        ]

    return jsonify(menu)
