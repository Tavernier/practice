#! /usr/bin/env python3

from functools import lru_cache
import os
from urllib.parse import urljoin

from flask import Flask
from flask import render_template
import requests

app = Flask(__name__)

###############################################################################
# Home Page
###############################################################################

@app.route("/")
def hello_world():
    return render_template("index.html", page_title="Home", menu=get_menu())

###############################################################################
# Dynamic Pages
###############################################################################
@app.route("/cart/", methods=["GET"])
def cart():
    return render_template("cart.html", page_title="Cart", menu=get_menu())


@app.route("/categories/<category_id>/")
@app.route("/categories/<category_id>/<slug>/")
def category_page(category_id, slug=None):
    # oof, should make this a lot easier.
    menu = get_menu(f"/categories/{category_id}/")
    titles = [
        x.get("title", "Products")
        for x in menu
        if x["category_id"] == category_id
        ]
    if len(titles) == 1:
        page_title = titles[0]
    else:
        page_title = "Products"

    products = get_products_by_category(category_id)
    return render_template(
        "plp.html",
        page_title=page_title,
        menu=menu,
        products=products
        )


@app.route("/products/<product_id>/")
@app.route("/products/<product_id>/<slug>/")
def product_display_page(product_id, slug=None):
    product = get_product(product_id)
    return render_template(
        "pdp.html",
        page_title=product["title"],
        menu=get_menu(),
        product=product
        )


@app.route("/search/")
def search():
    return render_template("search.html", page_title="Search", menu=get_menu())


###############################################################################
# Slow Changing Pages
###############################################################################
@app.route("/about/", methods=["GET"])
def about():
    return render_template("about.html", page_title="About Us", menu=get_menu())


@app.route("/contact/", methods=["GET"])
def contact():
    return render_template("contact.html", page_title="Contact Us", menu=get_menu())


@app.route("/faq/", methods=["GET"])
def faq():
    return render_template("index.html", page_title="Frequently Asked Questions", menu=get_menu())

###############################################################################
# Data from Other Microservices
###############################################################################
@lru_cache(maxsize=100)
def get_menu(menu_path="/"):
    request_url = urljoin(os.environ["MENU_API_ENDPOINT"], menu_path)
    response = requests.get(request_url)
    if response.status_code == 404:
        response = requests.get(os.environ["MENU_API_ENDPOINT"])
    return response.json()

@lru_cache(maxsize=100)
def get_product(product_id):
    request_url = urljoin(os.environ["PRODUCTS_API_ENDPOINT"], product_id)
    response = requests.get(request_url)
    return response.json()

@lru_cache(maxsize=100)
def get_products_by_category(category_id):
    request_url = os.environ["PRODUCTS_API_ENDPOINT"]
    params = {
        "category_id": category_id
    }
    response = requests.get(request_url, params=params)
    return response.json()
