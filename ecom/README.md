## Helpful Sites

* [Color Schemes](https://coolors.co/palettes/trending)
* [CSS Grid Layouts](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout)
