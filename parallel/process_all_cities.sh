#! /usr/bin/env bash

process_all_cities(){
    # source data is tab delimited
    cat ./cities.tsv | \
    parallel \
    --bar \
    --colsep ' ' \
    ./process_one_city.sh {1} {2}
}

main(){
    process_all_cities
}

main
