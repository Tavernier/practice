# GNU Parallel

GNU Parallel is new to me.  It looks like a great way to get a script that's written to process one thing to process many things on a larger box (i.e. process one thing per core).

This "Hello World" exercise shows passing multiple arguments.  `parallel` looks a lot more powerful based on the options I see.  I'm just scratching the surface here.

## Process One City

The `./process_one_city.sh` script simiulates processing one city.  The script has two parameters: `CITY_NAME` and `STATE_ABBREVIATION`.

### Usage

```bash
# crunch data for Chicago, IL
./process_one_city.sh Chicago IL

# output
Chicago, IL - Processing in PID 57675
Chicago, IL - Finished Processing in PID 57675 in 6 seconds
```

## Processing All Cities

The `./process_all_cities.sh` script reads `./cities.tsv` and passes the arguments to `parallel`, which executes `./process_one_city.sh` in parallel.
