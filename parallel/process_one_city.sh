#! /usr/bin/env bash

set -euo pipefail

CITY_NAME=$1
STATE_ABBREVIATION=$2

process_thing(){
    # pretend to process data.
    # sleep a random amount of seconds.
    local sleep_seconds=$[($RANDOM % 10) + 1]

    echo "${CITY_NAME}, ${STATE_ABBREVIATION} - Processing in PID $$"
    sleep "${sleep_seconds}s"
    echo "${CITY_NAME}, ${STATE_ABBREVIATION} - Finished Processing in PID $$ in ${sleep_seconds} seconds"
}

main(){
    process_thing ${CITY_NAME} ${STATE_ABBREVIATION}
}

main
