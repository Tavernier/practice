# Practice

I use this repo to store practice code for playing around with new technology, having handy scripts, or increase my understanding of technologies I use from time to time.  I take a lot of liberty in the code depending on what I'm exploring.  

See each folder's ReadMe for more information.

## Reusable Components

- `./create_networks.sh`
    - Create comment networks for other services to join so they can easily talk to one another.
- `./backend/`
    - Launch to create backend components such as MySQL and Postgres DBs.

