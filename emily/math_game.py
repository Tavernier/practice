#! /usr/bin/env python3

import random
import time

class UserDidNotAnswer(Exception):
    """User did not enter a guess."""
    pass


def add(number_1, number_2):
    """Add two numbers together."""
    return number_1 + number_2


def subtract(number_1, number_2):
    """Subtract the second number from the first."""
    return number_1 - number_2


def multiply(number_1, number_2):
    """Multiply two numbers together."""
    return number_1 * number_2


def divide(number_1, number_2):
    """Divide the first number by the second."""
    return number_1 / number_2

def play_a_game():
    """Play one game of Mathy McMatherson!"""
    possible_numbers = list(range(1, 10))
    random_number_1 = random.choice(possible_numbers)
    possible_numbers.remove(random_number_1)
    random_number_2 = random.choice(possible_numbers)

    possible_operations =[
        ("add", add),
        ("subtract", subtract),
        ("multiply", multiply),
        ("divide", divide),
    ]

    title, math_function = random.choice(possible_operations)

    if title == "divide" and random_number_2 > random_number_1:
        # for division, we always want the higher number first
        temp_number = random_number_1
        random_number_1 = random_number_2
        random_number_2 = temp_number

    actual_answer = math_function(random_number_1, random_number_2)

    print("-" * 40)
    print(f"Your first number is {random_number_1}.")
    print(f"Your second number is {random_number_2}.")

    print(f"Please *{title}* your two numbers.")
    print("")
    user_answer = input("What is your answer? ")

    if len(user_answer) == 0 or user_answer.startswith("exit"):
        raise UserDidNotAnswer
    
    try:
        user_answer = float(user_answer)
    except ValueError:
        print("Could not turn your answer into a number.")
        print("")
        return False

    if round(user_answer, 1) == round(float(actual_answer), 1):
        print("🌟 You got the right answer! 🌟")
        good_things_to_say = [
            "You don't smell like poopy butt fart today!",
            "You don't look like a butt!",
            "You smell like flowers. 🌺",
            "You smell like fart that smells like a bottle of perfume.",
            "Your poop is pink, which is good.",
        ]
        print(random.choice(good_things_to_say))
        print("")
        return True
    else:
        print("❌ You got the wrong answer! ❌")
        print(f"The correct answer is {actual_answer}.")
        bad_things_to_say = [
            "You smell like poopy! 💩💩💩",
            "You look like a butt! 🍑",
            "You are a fart that smells like poopy booty. 💩💨",
            "You look like a soccer ball that's been kicked 100,000,000,000,000,000 times.",
            "You look like a crushed soda can that has dog pee and poop on it.",
        ]
        print(random.choice(bad_things_to_say))
        print("")
    return False


if __name__ == "__main__":
    print("-" * 80)
    print("Welcome to Mathy McMatherson!")
    print("-" * 80)

    number_of_correct_answers = 0
    number_of_questions_asked = 0

    players_name = input("What is your name? ")
    print(f"Welcome to our game, {players_name}!")
    print(f"If you get questions wrong, {players_name}, you are a poopy butt 💩.")
    while True:
        try:
            if play_a_game():
                number_of_correct_answers += 1
            number_of_questions_asked += 1
            time.sleep(2)
        except (KeyboardInterrupt, UserDidNotAnswer):
            number_of_wrong_answers = number_of_questions_asked - number_of_correct_answers
            print("")
            print(f"You got {number_of_correct_answers} questions correct!")
            print(f"You answered {number_of_questions_asked} questions!")
            print(f"So, you got {number_of_wrong_answers} questions wrong!")
            print("Good bye!")
            break
