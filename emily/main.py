#! /usr/bin/env python3

def show_message(message="Food is yummy!"):
    return message


def greet_person(first_name):
    if first_name.lower() == "emily":
        return "Hello, greatest person in the world!"
    else:
        return f"Hello there {first_name}. You seem just okay."


def rate_food(food):
    if food == "🍔":
        return "Cheeseburgers are yummy especially McDonald's!"
    else:
        return "You should eat a cheeseburger from McDonald's instead."


if __name__ == "__main__":
    person_name = input("What is your name? ")
    food = input("What food are you eating: 🍩, 🍔? ")

    print(greet_person(person_name))
    print(show_message())
    print(show_message("Food is eaten by animals."))
    if food in ("🍩", "🍔"):
        print(show_message(food * 3))
    print(rate_food(food))
