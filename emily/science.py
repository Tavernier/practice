#! /usr/bin/env python3
"""The Best Computer Program Ever

Emily Agrees
"""

import os
import random
import time

def show_greeting():
    print("🧪" * random.randint(100, 500))
    print("🧬" * random.randint(100, 500))
    print("🔬" * random.randint(100, 500))
    print("If you like science, then you like math because you need math for science.")

if __name__ == "__main__":
    try:
        while True:
            show_greeting()
            time.sleep(.1)
            os.system("clear")
    except KeyboardInterrupt:
        print("How could you ruin the sciency show!")
        print("You are mean Mr. or Ms.")
        print("You smell like 💩💩💩")
        time.sleep(1)
        print("💩" * 5000)
        print("Taking you to the best web page on the internet.")
        time.sleep(10)
        os.system("open https://sites.google.com/view/emilypizzapage/home")
