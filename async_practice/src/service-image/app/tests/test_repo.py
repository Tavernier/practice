#! /usr/bin/env python3

from unittest import TestCase
from uuid import uuid4

import pytest

import repos

fake_id = lambda: str(uuid4()).split("-")[0]

@pytest.fixture(scope="session")
def repo():
    return repos.repo_factory()

def test_create_get_one(repo):
    entity_id = fake_id()
    expected_entity = {"collection_id": "cities", "entity_id": entity_id, "city": "Chicago", "founded": 1837}
    upsert_entity = repo.upsert("cities", entity_id, {"city": "Chicago", "founded": 1837})
    assert upsert_entity == expected_entity
    assert repo.get("cities", entity_id) == expected_entity

def test_delete(repo):
    entity_id = fake_id()
    assert repo.upsert("cities", entity_id, {"city": "Madison", "founded": 1836})
    response = repo.delete("cities", entity_id)
    assert response == {"collection_id": "cities", "entity_id": entity_id}

def test_create_many(repo):
    entities_to_create = [{"entity_id": fake_id(), "city": x} for x in ["Atlanta", "Buffalo", "Seattle"]]
    expected_create_response = [
        {"collection_id": "cities", "entity_id": x["entity_id"], "status_code": 201}
        for x in entities_to_create
    ]
    actual_create_response = repo.upsert_many("cities", entities_to_create)
    assert actual_create_response == expected_create_response

def test_get_many(repo):
    created_entities = []
    for city in ["New York", "San Diego"]:
        entity_id = fake_id()
        created_entities.append(repo.upsert("cities", entity_id, {"city": city}))
    assert repo.get_many("cities", [x["entity_id"] for x in created_entities]) == created_entities
