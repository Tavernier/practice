
import logging
import os

from repos.abstract import AbstractRepo
from repos.firestore import FirestoreRepo
from repos.memory import DictionaryRepo

__all__ = [
    AbstractRepo,
    DictionaryRepo,
]

def repo_factory(data_store: str = os.getenv("DATA_STORE", "memory").lower()) -> AbstractRepo:
    logging.info(f"Using {data_store} data store.")
    match data_store:
        case "firestore":
            repo = FirestoreRepo()
        case _:
            repo = DictionaryRepo()
    return repo
