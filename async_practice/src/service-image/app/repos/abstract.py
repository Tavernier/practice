#! /usr/bin/env python3
"""Abstraction over actual Data Stores"""

from abc import ABC, abstractmethod
from datetime import datetime

class AbstractRepo(ABC):  # pragma: no cover
    @abstractmethod
    def get(self, collection_id: str, entity_id: str) -> dict:
        raise NotImplemented

    @abstractmethod
    def get_many(self, collection_id: str, entity_ids: list[str]) -> list[dict] | None:
        raise NotImplemented

    @abstractmethod
    def upsert(self, collection_id: str, entity_id: str, entity: dict, ttl: datetime = None) -> dict:
        raise NotImplemented

    @abstractmethod
    def upsert_many(self, collection_id: str, entities: list[dict]) -> list[dict]:
        raise NotImplemented

    @abstractmethod
    def delete(self, collection_id: str, entity_id: str) -> dict:
        raise NotImplemented
