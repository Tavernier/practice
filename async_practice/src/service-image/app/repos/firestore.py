#! /usr/bin/env python3
"""Firestore Repository"""

import logging
import os
from functools import cached_property

from google.cloud.firestore import Client

from repos import AbstractRepo

class FirestoreRepo(AbstractRepo):
    """Read, Write, and Delete data stored in GCP Firestore."""
    def __init__(self):
        self.single_get_timeout_seconds = 1.0
        self.single_insert_timeout_seconds = 2.0
        self.single_delete_timeout_seconds = 2.0
        self.bulk_insert_timeout_seconds = 10.0

    @cached_property
    def client(self) -> Client:
        gcp_project = os.getenv("GCP_PROJECT", "local-project")
        if gcp_project == "local-project":
            logging.info("Assuming Firestore Emulator is in use.")
            os.environ["FIRESTORE_EMULATOR_HOST"] = "localhost:9000"
        os.environ["GOOGLE_CLOUD_PROJECT"] = gcp_project  # used by gcp default auth creds
        return Client(project=gcp_project)

    def get(self, collection_id: str, entity_id: str) -> dict | None:
        """Returns the a dictionary of the retrieved Firestore document."""
        reference = self.client.collection(collection_id).document(entity_id)
        document = reference.get(timeout=self.single_get_timeout_seconds)
        if document.exists:
            entity = document.to_dict()
            entity.update({"collection_id": collection_id, "entity_id": document.id})
            return entity
        else:
            return None

    def get_many(self, collection_id: str, entity_ids: list[str]) -> list[dict] | None:
        collection = self.client.collection(collection_id)
        documents = self.client.get_all([collection.document(x) for x in entity_ids])
        entities = []
        for document in documents:
            entity = document.to_dict()
            entity.update({"collection_id": collection_id, "entity_id": document.id})
            entities.append(entity)
            # entity = None
        return entities

    def upsert(self, collection_id: str, entity_id: str, entity: dict) -> dict | None:
        """Write a document to Firestore."""
        self.client.collection(collection_id).document(entity_id).set(entity, timeout=self.single_insert_timeout_seconds)
        return self.get(collection_id, entity_id)

    def upsert_many(self, collection_id: str, entities: list[dict]) -> list[dict]:
        added_entity_ids = []
        batch = self.client.batch()
        for payload in entities:
            entity_id = payload.pop("entity_id")
            doc_ref = self.client.collection(collection_id).document(entity_id)
            batch.set(doc_ref, payload)
            added_entity_ids.append(entity_id)
        # TO DO: understand write_result and use it to derive return value.
        write_result = batch.commit(timeout=self.bulk_insert_timeout_seconds)
        return [
            {"collection_id": collection_id,  "entity_id": x, "status_code": 201}
            for x in added_entity_ids
        ]

    def delete(self, collection_id: str, entity_id: str) -> dict:
        reference = self.client.collection(collection_id).document(entity_id)
        reference.delete(timeout=self.single_delete_timeout_seconds)
        return {"collection_id": collection_id, "entity_id": entity_id}
