#! /usr/bin/env python3
"""In-memory Data Store"""

import logging
from datetime import datetime

from repos import AbstractRepo

class DictionaryRepo(AbstractRepo):
    """Read, Write, and Delete data stored in an in-memory Dictionary."""
    def __init__(self):
        self._store = dict()

    def get(self, collection_id: str, entity_id: str) -> dict | None:
        entity = self._store.setdefault(collection_id, {}).get(entity_id, None)
        if entity:
            entity.update({"collection_id": collection_id, "entity_id": entity_id})
            return entity
        else:
            return None

    def get_many(self, collection_id: str, entity_ids: list[str]) -> list[dict]:
        return [self.get(collection_id, entity_id) for entity_id in entity_ids]

    def upsert(self, collection_id: str, entity_id: str, entity: dict, ttl: datetime = None) -> dict:
        if ttl:
            logging.warning("TTL not implemented for in-memory data store.")
        self._store.setdefault(collection_id, {})[entity_id] = entity
        return self.get(collection_id, entity_id)

    def upsert_many(self, collection_id: str, entities: list[dict]) -> list[dict]:
        new_entities = dict()
        for entity in entities:
            entity_id = entity.pop("entity_id")
            new_entities[entity_id] = entity
        self._store.setdefault(collection_id, {}).update(new_entities)
        return [
            {"collection_id": collection_id, "entity_id": x, "status_code": 201}
            for x in new_entities.keys()
        ]

    def delete(self, collection_id: str, entity_id: str,) -> dict:
        try:
            self._store[collection_id].pop(entity_id)
        except KeyError:
            pass
        return {"collection_id": collection_id, "entity_id": entity_id}
