#! /usr/bin/env python3
"""HTTP Service"""

import asyncio
import logging
from datetime import datetime

from fastapi import FastAPI
from pydantic import BaseModel, Field

import repos

app = FastAPI(title="Product Manager")
logger = logging.getLogger(__name__)
logger.setLevel("INFO")
repo = repos.repo_factory()

async def pause(message, seconds_to_pause=.25) -> str:
    await asyncio.sleep(seconds_to_pause)
    output = f"{message} {seconds_to_pause} seconds."
    logger.info(output)
    return output

class HelloWorld(BaseModel):
    message: str
    effective_at: datetime = Field(default_factory=datetime.utcnow)

@app.get("/", response_model=HelloWorld)
async def get_root():
    async with asyncio.TaskGroup() as tg:
        message1 = tg.create_task(pause("How are you?", .25))
        message2 = tg.create_task(pause("Greetings, earthling.", .1))
    return HelloWorld(message=" ".join([message1.result(), message2.result()]))
