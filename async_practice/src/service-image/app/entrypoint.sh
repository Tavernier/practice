#! /usr/bin/env bash

set -euo pipefail

function start_web_server(){
  uvicorn \
    --host 0.0.0.0 \
    --port ${PORT} \
    main:app --reload
}

main(){
  start_web_server
}

main
