# Data Engineering Base Image

Let's create a base image with tools often used to move data from one place to another.

## Utilities

- `bq version`
- `gsutil --version`
- `jq --version`
- `less --version`
- `mysql --version`
- `parallel --version`
- `psql --version`

## Environmanet Variables

- `GOOGLE_APPLICATION_CREDENTIALS`
    - Absolute path to GCP service account's JSON credential file (e.g. /tmp/credentials.json).
    - Google specified the name of this environment variable.
- `GCP_PROJECT_ID`
    - GCP Project ID to set as the current context.

## To Do

1. Add Script for Easy GCP Setup
    - Either automatically on startup or manually.
    ```bash
    service_account_email=$(cat "${GOOGLE_APPLICATION_CREDENTIALS}" | jq -r '.client_email')
    gcloud auth activate-service-account ${service_account_email} --key-file=${GOOGLE_APPLICATION_CREDENTIALS}
    ```
1. Create Docker Compose file for easier launching with secrets.
