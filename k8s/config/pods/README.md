# Pod Configurations

While we can define yaml docs to deploy Pods, this is not the preferred route here.

Instead, define deployments, which will help keep Pods running.

Then, define services, which will expose those Pods.

So, check the `../deployments` and `../services` directories.
