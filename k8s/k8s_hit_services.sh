#!/usr/bin/env bash

set -ue

# I use this script to quickly check connectivity to k8s services
# from my laptop, which should mean they are exposed publicly.

# I also hit the custom ingress, which requires adding minikube's
# IP to /etc/hosts and associating it with jonk8s.info

hit_all_endpoints(){
    echo "Hitting k8s Services"
    echo "I disabled all external access directly to services.  Must go through ingress."
    echo "Change types from ClusterIP if needing to expose these directly to world."
    echo
}

hit_ingress_endpoints(){
    echo "Hitting Travels Ingress"
    curl "http://jonk8s.info/api/trips/trips/"
    echo ""
    echo ""

    echo "Hitting Books Ingress"
    curl "http://jonk8s.info/api/reviews/reviews/"
    echo ""
    echo ""

    echo "Hitting Website Ingress"
    curl "http://jonk8s.info" | head
    echo ""
    echo ""
}


main(){
    hit_all_endpoints
    hit_ingress_endpoints
}

main
