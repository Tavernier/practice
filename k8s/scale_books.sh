#!/usr/bin/env bash

# i wanted to put this into a script to play with manually
# scaling a deployment up and down.

set -eux

scale_books(){
    local number_of_replicas=$1

    kubectl scale deployment jon-books-k8s-deployment \
        --replicas=${number_of_replicas}

    kubectl get pods
}

scale_books $1
