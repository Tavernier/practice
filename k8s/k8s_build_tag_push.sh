#! /usr/bin/env bash

set -xe

# This script will build Docker images within minikube k8s
# and push them to the k8s locally running image repo.

point_docker_at_k8s(){
    eval $(minikube docker-env)
}

build_docker_images(){
    pushd ./images
    ./build.sh
    popd
}

show_docker_images(){
    docker images | grep -Ei 'jon.*k8s' | sort
}

push_images_to_repo(){
    docker tag jon-travels-k8s localhost:5000/jon-travels-k8s
    docker push localhost:5000/jon-travels-k8s

    docker tag jon-books-k8s localhost:5000/jon-books-k8s
    docker push localhost:5000/jon-books-k8s

    docker tag jon-site-k8s localhost:5000/jon-site-k8s
    docker push localhost:5000/jon-site-k8s

    docker tag jon-counter-k8s localhost:5000/jon-counter-k8s
    docker push localhost:5000/jon-counter-k8s

    docker tag jon-worker-k8s localhost:5000/jon-worker-k8s
    docker push localhost:5000/jon-worker-k8s
}

main(){
    point_docker_at_k8s
    build_docker_images
    push_images_to_repo
    show_docker_images
}

main
