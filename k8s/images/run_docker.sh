#! /usr/bin/env bash

set -xe

# Launch in Docker for quick testing.

build_images(){
    ./build.sh
}

run_all_images(){
    docker run \
        --detach \
        --publish 5050:5000 \
        --name jon_books_k8s \
        --rm \
        jon-books-k8s

    docker run \
        --detach \
        --publish 5000:5000 \
        --name jon_travels_k8s \
        --rm \
        jon-travels-k8s

    docker run \
        --detach \
        --publish 8080:80 \
        --name jon_site_k8s \
        --rm \
        jon-site-k8s
}

destroy_all_containers(){
    docker rm -f jon_books_k8s
    docker rm -f jon_travels_k8s
    docker rm -f jon_site_k8s
}

main(){

    build_images

    # the containers may not be running so
    # ignore rm failures.
    destroy_all_containers || true
    
    run_all_images
}

main
