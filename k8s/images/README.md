# Docker Images

I am using these Docker Images to test playing around with k8s and deploying PODs.

## Images

I created these images.  More information can be found within their own ReadMe documents.

* `travels`
    * REST API to return trip information.
    * Tag: `jon-travels-k8s`
    * Local port: 5000
* `books`
    * REST API to return book review information.
    * Tag: `jon-books-k8s`
    * Local port: 5000
* `site`
    * Static site, which reads from other endpoints.
    * Tag: `jon-site-k8s`
    * Local port: 80

## Resources

* [Container building tips](https://cloudplatform.googleblog.com/2018/07/7-best-practices-for-building-containers.html)
