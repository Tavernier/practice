# Book Reviews Data

Let us assume Team Beta built this microservice, which returns book review information.  

## Endpoints 

These endpoints exist as of today:

* GET `/reviews/`
    * Gets all book reviews as a list.
* GET `/reviews/<review_id>`
    * Returns a single book review.

## Entity

Each book review has these attributes:

* id
    * Public facing review ID.
    * String.
* date
    * ISO-8601 date of when book reviews was created.
    * String.
* title
    * Short blurb describing the trip.
    * String.
* rating
    * Rating from 1 to 4 with 4 being the best.
    * Integer.

Here is an example trip:

```json
{
    "id":"R812312",
    "date":"2017-11-20",
    "title": "The Match",
    "rating": 4
}
```

## Running for Development

```bash
docker build . --tag your_image_name \
&& docker run -d -p 5000:5000 your_image_name
```

## Warning

Since my goal here is learning k8s, I do a number of bad things here:

* Returning all entities on the GET request.
* Running Flask with its built-in web server.
* Storing data within the application itself.
* Etc.
