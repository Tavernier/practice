#!/usr/bin/env python3

import json
import random
import os
from bson.json_util import dumps

from flask import Flask
from pymongo import MongoClient

app = Flask(__name__)

############################################
# Book Reviews Data
############################################

# normally, this data would be fetched from
# an external data store.  since my goal is
# more to learn k8s stuff, i'm going to 
# embed the reviews data here.

# for the reviews endpoint, all reviews will be
# returned.  normally, this would be a paged
# response.

def retrive_review_info(review_id=None):
    """Returns a list of reviews of either all reviews or those found with a given ID.
    
    input
        review_id : string
            Public ID of a the review.
            Matches on the id attribute of a review entity.
            If None, all reviews will be returned.

    output
        List of reviews.
    """

    with MongoClient(os.environ.get('MONGODB_HOST', 'jon-mongodb-k8s-service'), 27017) as client:

        reviews = []

        # this database will be created if it does not exist
        database = client['my-website']

        if review_id:
            review = database['reviews'].find_one({'id': review_id})
            if review:
                reviews.append(
                    {
                        'id': review['id'],
                        'date': review['date'],
                        'title': review['title'],
                        'rating': review['rating']
                    }
                )
        else:
            for review in database['reviews'].find().limit(10):
                reviews.append(
                    {
                        'id': review['id'],
                        'date': review['date'],
                        'title': review['title'],
                        'rating': review['rating']
                    }
                )
                
    return reviews

@app.route("/reviews/")
def get_reviews():
    """Return all reviews found."""    
    return (json.dumps(retrive_review_info()), {'Access-Control-Allow-Origin': '*'})

@app.route("/reviews/<review_id>")
def get_review(review_id):
    """Return a single reviews or 404 if not found."""
    review = retrive_review_info(review_id)

    if len(review) == 1:
        return (json.dumps(review[0]), {'Access-Control-Allow-Origin': '*'})
    
    return 'Review not found', 404

@app.route("/probes/liveness/")
def get_liveness_status():
    """Simulate a liveness probe with a 3% chance of failure."""
    return 'Liveness probe', random.choices([200, 503], weights=[.9, .1])[0]
