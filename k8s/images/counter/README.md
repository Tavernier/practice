# Counter

Let us assume Team Gamma built this microservice, which returns counter information.

## Endpoints 

These endpoints exist as of today:

* GET `/count/`
    * Get's the current count from Redis.

Here is an example response:

```json
{
    "count": 10
}
```

## Warning

Since my goal here is learning k8s, I do a number of bad things here:

* The GET endpoint also increments the counter.
* Data is lost when Redis is destoryed.
* Running Flask with its built-in web server.
* Storing data within the application itself.
* Etc.
