#!/usr/bin/env python3

import json
import redis
import os

from flask import Flask

app = Flask(__name__)

############################################
# COUNTER
############################################

REDIS_CLIENT = redis.StrictRedis(
    host=os.environ.get('REDIS_MASTER_HOST', 'jon-redis-k8s-service'),
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

@app.route("/count/")
def get_count():
    """Return counter value."""
    return (json.dumps({"count": REDIS_CLIENT.incr('whatever:1000')}), {'Access-Control-Allow-Origin': '*'})
