# Trips Data

Let us assume Team Alpha built this microservice, which returns trip information.

## Endpoints 

These endpoints exist as of today:

* GET `/trips/`
    * Gets all trips as a list.
* GET `/trips/<trip_id>`
    * Returns a single trip.

## Entity

Each trip has these attributes:

* id
    * Public facing trip ID.
    * String.
* date
    * ISO-8601 date of when trip occurred.
    * String.
* title
    * Short blurb describing the trip.
    * String.
* latitude
    * Latitude.
    * Float.
* longitude
    * Longitude.
    * Float.

Here is an example trip:

```json
{
    "id":"T12345",
    "date":"2017-11-20",
    "title": "Paris",
    "latitude": 48.864716,
    "longitude": 2.349014
}
```

## Running for Development

```bash
docker build . --tag your_image_name \
&& docker run -d -p 5000:5000 your_image_name
```

## Warning

Since my goal here is learning k8s, I do a number of bad things here:

* Returning all entities on the GET request.
* Running Flask with its built-in web server.
* Storing data within the application itself.
* Etc.
