#!/usr/bin/env python3

import json

from flask import Flask

app = Flask(__name__)

############################################
# Trips Data
############################################

# normally, this data would be fetched from
# an external data store.  since my goal is
# more to learn k8s stuff, i'm going to 
# embed the trip data here.

# for the trips endpoint, all trips will be
# returned.  normally, this would be a paged
# response.

def retrive_trip_info(trip_id=None):
    """Returns a list of trips of either all trips or those found with a given ID.
    
    input
        trip_id : string
            Public ID of a the trip.
            Matches on the id attribute of a trip entity.
            If None, all trips will be returned.

    output
        List of trips.
    """
    trips = [
        {
            "id": "T12345",
            "date": "2017-11-20",
            "title": "Paris",
            "latitude": 48.864716,
            "longitude": 2.349014
        },
        {
            "id": "T131312",
            "date": "2016-09-20",
            "title":"Dortmund",
            "latitude":51.5136,
            "longitude":7.4653
        },
        {
            "id": "T2032424",
            "date": "2016-03-20",
            "title":"Tallahassee",
            "latitude":30.444826,
            "longitude":-84.291798
        },
        {
            "id": "T898132",
            "date": "2017-12-20",
            "title":"San Antonio",
            "latitude":29.425656,
            "longitude":-98.484984
        }
    ]

    if trip_id:
        return [x for x in trips if x['id'] == trip_id]
    
    return trips

@app.route("/trips/")
def get_trips():
    """Return all trips found."""    
    return (json.dumps(retrive_trip_info()), {'Access-Control-Allow-Origin': '*'})

@app.route("/trips/<trip_id>")
def get_trip(trip_id):
    """Return a single trip or 404 if not found."""
    trip = retrive_trip_info(trip_id)

    if len(trip) == 1:
        return (json.dumps(trip[0]), {'Access-Control-Allow-Origin': '*'})
    
    return 'Trip not found', 404
