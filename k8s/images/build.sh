#! /usr/bin/env bash

set -xe

# Use this script to build all Docker images for testing within k8s.

build_image(){
    # Use this to build simple Docker images.
    # Create specific functions for more complicated build processes.
    # Input
    #   image_location: directory holding Docker image (e.g. ./travels)
    # Output
    #   A Docker image names "jon-{directory basename}-k8s:latest".
    #   For the travels dir, for instance, jon-travels-k8s:latest is built.
    image_location=$1
    image_basename=$(basename "$image_location")
    pushd "$image_location"
    docker build . --tag jon-"$image_basename"-k8s:latest
    popd
}

main() {
    build_image travels
    build_image books
    build_image site
    build_image counter
    build_image worker
}

main
