# Static Website

This is a one-page static website.  I will use it to consume from the microservices (books and travels).

## Warning

Since my goal here is learning k8s, I do a number of bad things here:

* No styling exists here.
* nginx is running as root.
* nginx services the site so other than that, I'm skipping investigating best practices here.
* Etc.
