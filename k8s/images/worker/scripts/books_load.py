#! /usr/bin/env python3

import os
import sys

import pymongo

import logger

LOGGER = logger.logger(__file__)

def load_reviews_into_mongo():

    LOGGER.info('Attempting to load reviews into mongo.')

    host = os.environ.get('MONGODB_HOST', 'jon-mongodb-k8s-service')

    LOGGER.info(f'Connecting to host {host}')
    
    try:
        with pymongo.MongoClient(
                host, 
                27017,
                connectTimeoutMS=3000,
                serverSelectionTimeoutMS=3000
            ) as client:

            database = client['my-website']

            database.drop_collection('reviews')

            database['reviews'].create_index(
                'id',
                name='ix_reviews_id',
                unique=True
            )

            reviews = [
                {
                    "id": "R12345",
                    "date": "2017-11-20",
                    "title": "The Match",
                    "rating": 4
                },
                {
                    "id": "R0912321",
                    "date": "2018-01-20",
                    "title": "The City of Chicago",
                    "rating": 3
                },
                {
                    "id": "R6342134",
                    "date": "2018-06-20",
                    "title": "The Shipwrecks of Lake Michigan",
                    "rating": 4
                }
            ]
            
            database['reviews'].insert_many(reviews)

            number_of_reviews = database['reviews'].count_documents({})

            LOGGER.info(f'Collection now holds {number_of_reviews} reviews.')

            return number_of_reviews > 0

    except pymongo.errors.ServerSelectionTimeoutError as e:
        LOGGER.error(f'Unable to connect to mongo. {type(e).__name__}')
        sys.exit(1)

if __name__ == '__main__':
    
    load_reviews_into_mongo()
