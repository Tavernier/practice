#! /usr/bin/env bash

set -xe

# This script will run k8s jobs.
# Run this after the deploy of course.

point_docker_at_k8s(){
    eval $(minikube docker-env)
}

main(){
    kubectl delete -f ./config/jobs/
    kubectl get jobs
}

main
