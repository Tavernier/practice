# kubernetes

My goals are to:

1. Understand k8s concepts
1. Launch stuff in k8s that talk to each other
    1. Disclaimer: I will do some bad things here like host Postgres and not care about its data.  I just want to launch and get things talking to one another.

## Dependencies

1. Docker is installed
1. Minikube is installed and running k8s

## Setup

If Minikube is setup for the first time or nuked and recreated:

1. Enable the ingress addon.
    - `minikube addons enable ingress`
1. Create a container registry within k8s.
    ```bash
    eval $(minikube docker-env)
    docker run -d -p 5000:5000 --restart=always --name registry registry:2
    ```
1. Add the minikube IP to `/etc/hosts`
    - `echo "$(minikube ip) jonk8s.info" | sudo tee -a /etc/hosts`

## Helpers

I created some scripts to get up and running.

* `./k8s_build_tag_push.sh`
    * Points `docker` at minikube k8s docker
    * Builds images within minikube k8s
    * Pushes those images to image repo running within k8s
* `./k8s_redeploy.sh`
    * Destroys all k8s services and deployments then recreates them
* `./k8s_hit_services.sh`
    * Issue GET requests against running services
    * Useful as a smoke test
* `./k8s_run_jobs.sh` and `./k8s_kill_jobs.sh`
    * Kickoff and nuke k8s jobs such as loading information into mongo.
* `./images/`
    * `build.sh`: Build Docker images for testing k8s deploys and services
    * `run_docker.sh`: Will run images in Docker for quick testing

## Architecture

Here is what I want:

* Static Website
    * Launch 1 or more pods.
    * Should be pubicly accesible.
    * Calls Book Reviews API, Trips API, and Counter API via client-side code.
* Book Reviews API
    * Launch 1 or more pods.
    * Should be pubicly accesible.
    * Called by Static Website via client-side code.
    * Stores data in MongoDB.
* Trips API
    * Launch 1 or more pods.
    * Should be pubicly accesible.
    * Called by Static Website via client-side code.
* Counter API
    * Launch 1 or more pods.
    * Should be pubicly accesible.
    * Called by Static Website via client-side code.
    * Stores data in Redis.
* Data Stores
    * Neither should be publicly accessible.
    * Accessible only to PODs running within k8s.
    * MongoDB: Stores Book Reviews API Data
    * Redis: Stores Counter API Data


Here is what I think I need to do that:

* Docker Images accessible to k8s
* k8s pods via deployments
    * with labels
* k8s services
    * available externally

## Tutorials and Concepts

I took the [Scalable Microservices with Kubernetes](https://classroom.udacity.com/courses/ud615) course on Udacity.

Lesson 3 of this course focuses on Kubernetes.  The first two lessons discuss microservices and Docker.

* Machines have transitioned to very specific machines (i.e. pets) into generic computing machines.  Containers can now run for seconds, minutes, days, etc..  Many can run in parallel.  
* Trend went from Bare Metal > Virtual Machines > Containers

* Pods
    * Pods are the core of k8s.  A Pod represents a logical application.  
    * Pods contain one or more containers.  You can include multiple containers when they have hard dependencies with one another.
    * Pods have volumes, which live for the duration of the Pod.
    * Pods also share a network namespace.  So, a Pod gets a single IP address.  By default, Pods get private IP addresses.
        * `kubectl port-forward` can be used to map a local port to a port within a k8s Pod.
* Services
    * A Service is a stable endpoint for underlying Pods.  Pods can restarted for all kinds of reasons.
    * A Service finds Pods based on labels.  If Pods have correct labels, they are automatically picked up and exposed by the Service.
    * More here: http://kubernetes.io/docs/user-guide/services/
    * Types: ClusterIP, NodePort, LoadBalancer
* Config files
    * We can create yaml files to describe our pods then use `kubectl create` to launch them.
* Secrets and Configmaps
    * Never put secrets and config within the Pod itself.  There's a better way!
    * Secerts:
        * Used for sensitive application config data
        * Secret must exist before Pod will launch
        * More here: http://kubernetes.io/docs/user-guide/secrets/
    * Configmaps:
        * Insensitve application config data
        * Can tell downstream Pods data has changed
        * More here: http://kubernetes.io/docs/user-guide/configmap/
* Health and Monitoring
    * We can add *user-generated* application readiness and liveness checks at the *container* level.  k8s will respect these.
    * Readiness probe = indicates when a *Pod* is ready to serve traffic.  If a container's check fails, it is marked as Not ready = removed from any load balancers.
    * Liveness probe = indicates a container is alive.  If it fails multiple times, the container will be restarted.
    * [See more in k8s docs](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/)

### Getting Started

#### Adding Docker Images to Minikube k8s

Thank you Kevin Smets for posting this article showing how to get a Docker Image repo running locally in minikube k8s: https://gist.github.com/kevin-smets/b91a34cea662d0c523968472a81788f7

I created the `./k8s_build_tag_push.sh` script to do this for my Docker images here.  I can call that to point `docker` at minikube, build the Docker images, and then push them to the locally running repo.  Below is me manually running through the process to understand what is happening and when.

If a Docker Image repo does not exist in minikube, create one with:

```bash
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

Here's my run through:

```bash
# point docker cli at minikube
eval $(minikube docker-env)

# setup Docker Image registry wihink k8s
# we will build our images then push them here so k8s can find them
docker run -d -p 5000:5000 --restart=always --name registry registry:2

# since Docker is now pointed at k8s Docker, build my images in k8s
cd ./images && ./build.sh && cd -

# verify images are built in k8s
docker images

# let's tag an image for the locally running image repo then push it
docker tag jon-travels-k8s localhost:5000/jon-travels-k8s
docker push localhost:5000/jon-travels-k8s

# run a pod with that image. note that a deployment is created.
# output: deployment.apps/jon-travels-k8s created
kubectl run jon-travels-k8s --image=localhost:5000/jon-travels-k8s:latest

# verify pod is running
kubectl get pods

# NAME                              READY     STATUS    RESTARTS   AGE
# jon-travels-k8s-ccbffc8d5-2tq6w   1/1       Running   0          16s

# wipe out our deployment / pod
kubectl delete deployment jon-travels-k8s
```

#### Deploying Images as Pods

I now have:

1. k8s running in minikube
1. Docker Images built in minikube k8s
1. A Docker Image repo running in minikube k8s
1. My Docker Images push to the Docker Image repo running in minikube k8s

Let's now try launching pods from these images.  This is just a smoke test to see whether things run.  This is not how production deploy will actually happen.

We can test launching Pods with [kubectl run](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run):

`kubectl run {whatever} --image={image}:{tag}`

So, let's run my images: 

```bash
# each call will create a deployment, which will result in one running Pod each
kubectl run jon-travels-k8s --image=localhost:5000/jon-travels-k8s:latest
kubectl run jon-books-k8s --image=localhost:5000/jon-books-k8s:latest
kubectl run jon-site-k8s --image=localhost:5000/jon-site-k8s:latest

kubectl get pods

# NAME                              READY     STATUS    RESTARTS   AGE
# jon-books-k8s-58997c7d9d-d882d    1/1       Running   0          8s
# jon-site-k8s-6bcdbd5495-7s8bq     1/1       Running   0          4s
# jon-travels-k8s-ccbffc8d5-97l4x   1/1       Running   0          12s

# yay!

kubectl get deployments

# NAME              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
# jon-books-k8s     1         1         1            1           1m
# jon-site-k8s      1         1         1            1           1m
# jon-travels-k8s   1         1         1            1           1m

# let's nuke those for now
kubectl delete deployment jon-travels-k8s
kubectl delete deployment jon-books-k8s
kubectl delete deployment jon-site-k8s

# verify pods are deleted
# will eventually show "No resources found." as Pods are drained
kubectl get pods
```

#### Exposing Pods

Let's launch the travels microservice and get to the point where I can launch Chrome and hit its endpoints.

```bash

# first, launch the Pod.
kubectl run jon-travels-k8s --image=localhost:5000/jon-travels-k8s:latest

# then, expose the Pod.
# output should be: service/jon-travels-k8s exposed
kubectl expose deployments jon-travels-k8s --port 5000 --type LoadBalancer

# get public IP we can use to hit the Pod
# NAME              TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
# jon-travels-k8s   LoadBalancer   10.100.230.23   <pending>     5000:31217/TCP   2m

# access service endpoint and append trips
minikube service jon-travels-k8s

# here's the url with a curl
curl http://192.168.99.100:31217/trips/
# [{"id": "T12345", "date": "2017-11-20", "title": "Paris", "latitude": 48.864716, "longitude": 2.349014}, {"id": "T131312", "date": "2016-09-20", "title": "Dortmund", "latitude": 51.5136, "longitude": 7.4653}, {"id": "T2032424", "date": "2016-03-20", "title": "Tallahassee", "latitude": 30.444826, "longitude": -84.291798}, {"id": "T898132", "date": "2017-12-20", "title": "San Antonio", "latitude": 29.425656, "longitude": -98.484984}]

# yay!

# cleanup
kubectl delete service jon-travels-k8s
kubectl delete deployment jon-travels-k8s
```

What happens on `kubectl expose`?

The Udacity video explained:

* External load balancer with public IP attached to it
* The load balancer directs to the Pods

#### Interacting with Pods

Let's pause and see how to interact with Pods.

##### Port Forwarding

A pod gets a private IP and let's say that's appropriate for this pod.  We can setup port forwarding to the pod to test stuff.

```bash
# launch the travels microserivce
kubectl run jon-travels-k8s --image=localhost:5000/jon-travels-k8s:latest

# get the pod name
kubectl get pods

# this pod now has a private ip
# let's use port forwarding to hit its endpoints
# i am purposelly not standing up a service here
kubectl port-forward jon-travels-k8s-ccbffc8d5-4x6mb 10080:5000

# run in another terminal
curl http://localhost:10080/trips/
# [{"id": "T12345", "date": "2017-11-20", "title": "Paris", "latitude": 48.864716, "longitude": 2.349014}, {"id": "T131312", "date": "2016-09-20", "title": "Dortmund", "latitude": 51.5136, "longitude": 7.4653}, {"id": "T2032424", "date": "2016-03-20", "title": "Tallahassee", "latitude": 30.444826, "longitude": -84.291798}, {"id": "T898132", "date": "2017-12-20", "title": "San Antonio", "latitude": 29.425656, "longitude": -98.484984}]

# yay!
```

##### Logs

We can view the stdout / stderr output via logs.  For Pods with multiple containers, we must specify the container name.

```bash
# see application output
kubectl logs jon-travels-k8s-ccbffc8d5-4x6mb

# follow the logs
kubectl logs jon-travels-k8s-ccbffc8d5-4x6mb -f
```

##### Access the Pod

We can shell into a Pod's containers too.  This is helpful for troubleshooting such as testing connectivity to external resources.

```bash
# shell into the running pod
kubectl exec jon-travels-k8s-ccbffc8d5-4x6mb --stdin --tty /bin/sh

# look at stuff
$ whoami
application_user
$ ping google.com
PING google.com (172.217.0.14) 56(84) bytes of data.
64 bytes from ord38s04-in-f14.1e100.net (172.217.0.14): icmp_seq=1 ttl=61 time=12.6 ms
64 bytes from ord38s04-in-f14.1e100.net (172.217.0.14): icmp_seq=2 ttl=61 time=12.0 ms
64 bytes from ord38s04-in-f14.1e100.net (172.217.0.14): icmp_seq=3 ttl=61 time=11.6 ms
```

##### Cleanup

`kubectl delete deployment jon-travels-k8s`

### Deploying

At this point, I have:

1. k8s running in minikube
1. Docker Images built in minikube k8s
1. A Docker Image repo running in minikube k8s
1. My Docker Images push to the Docker Image repo running in minikube k8s
1. k8s configuration files for:
    1. Deployments
    1. Services

#### Getting Started

I started by creating configuration files for the `travels` service.  I followed the Udacity course and referenced k8s documentation on deployments and services.

```bash
# verify no jon* resources exist for services, deployments, and pods
kubectl get services
kubectl get deployments
kubectl get pods

# i started with the travels service
# first, get pods running via a deployment
# output should be: deployment.apps/jon-travels-k8s-deployment created
kubectl create -f ./config/deployments/travels.yaml

# verify pods are running
# in reality, should not need to do this since pods should flow in/out of service
kubectl get pods

# create the service
# output should be: service/jon-travels-k8s-service created
kubectl create -f ./config/services/travels.yaml

# verify service launches and hit its endpoint
kubectl get services
minikube service list
minikube service --url jon-travels-k8s-service

curl "$(minikube service --url jon-travels-k8s-service)/trips/"

# [{"id": "T12345", "date": "2017-11-20", "title": "Paris", "latitude": 48.864716, "longitude": 2.349014}, {"id": "T131312", "date": "2016-09-20", "title": "Dortmund", "latitude": 51.5136, "longitude": 7.4653}, {"id": "T2032424", "date": "2016-03-20", "title": "Tallahassee", "latitude": 30.444826, "longitude": -84.291798}, {"id": "T898132", "date": "2017-12-20", "title": "San Antonio", "latitude": 29.425656, "longitude": -98.484984}]

# yay!
```

So, we now have travels running locally in k8s.  Let's commit that code, create yamls for the other services, and helper scripts to destroy and launch everything.

### Ingress

At this point, I now have:

1. k8s running in minikube
1. Docker Images built in minikube k8s
1. A Docker Image repo running in minikube k8s
1. My Docker Images push to the Docker Image repo running in minikube k8s
1. k8s configuration files for all Deployments and Services

Since my website hits other endpoints, I run into a problem where the website hits an endpoint on another point.  k8s ingress looks like a nice way to solve this problem.

I will use this tutorial as my guide: https://medium.com/@Oskarr3/setting-up-ingress-on-minikube-6ae825e98f82


First, enable minikube's ingress addon: `minikube addons enable ingress`.

I then created an ingress file with the goal of:

```
jonk8s.info --> static site
    /api --> all apis
        /trips  --> travel api
        /reviews --> book reviews api
```
Describe `kubectl describe ingress jon-k8s-ingress`

My ingres defined the `jonk8s.info` domain, which does not exist in the real work of course.  The article had this handy snippet to associate that domain with my minikube ip address:

`echo "$(minikube ip) jonk8s.info" | sudo tee -a /etc/hosts`

Once I did that, I was able to visit the website:

`open http://jonk8s.info`

### Miscellaneous

I now have a static site hitting a couple endpoints running in k8s!  Let's explore other stuff.

#### Scaling Deployments

Business is booming and people love my book reviews!  We've gotta scale that service up.

```bash
# let's double capacity
kubectl scale deployment jon-books-k8s-deployment --replicas=4
```

```
$ kubectl get pods

NAME                                          READY     STATUS    RESTARTS   AGE
jon-books-k8s-deployment-65b4845d5f-6q6vz     1/1       Running   0          4s
jon-books-k8s-deployment-65b4845d5f-bffm9     1/1       Running   0          6m
jon-books-k8s-deployment-65b4845d5f-bv68c     1/1       Running   0          6m
jon-books-k8s-deployment-65b4845d5f-qhkrg     1/1       Running   0          4s
jon-site-k8s-deployment-77f995dcd9-bbqv5      1/1       Running   0          6m
jon-site-k8s-deployment-77f995dcd9-mrr8p      1/1       Running   0          6m
jon-travels-k8s-deployment-77657df84d-8bbmm   1/1       Running   0          6m
jon-travels-k8s-deployment-77657df84d-d6k6m   1/1       Running   0          6m

$ kubectl get deployments
NAME                         DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
jon-books-k8s-deployment     4         4         4            4           6m
jon-site-k8s-deployment      2         2         2            2           6m
jon-travels-k8s-deployment   2         2         2            2           6m
```

Fancier options exist too such as auto scaling.

Anywho, traffic has dropped so let's scale this service back:

```bash
kubectl scale deployment jon-books-k8s-deployment --replicas=2
```


References:

* https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#scaling-a-deployment
