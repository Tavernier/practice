#! /usr/bin/env bash

set -xe

# This script will nuke all k8s resources and redeploy them.

point_docker_at_k8s(){
    eval $(minikube docker-env)
}

delete_ingresses(){
    kubectl delete -f ./config/ingress/ --wait=true
}

delete_services(){
    kubectl delete -f ./config/services/ --wait=true
}

delete_deployments(){
    kubectl delete -f ./config/deployments/ --wait=true
}

create_services(){
    kubectl create -f ./config/services/
}

create_deployments(){
    kubectl create -f ./config/deployments/
}

create_ingresses(){
    kubectl create -f ./config/ingress/
}

delete_helm_chart(){
    local api=$1
    helm delete --purge "jon-${api}-release"
}

install_helm_chart(){
    local api=$1
    pushd ./charts/jon-apis
    helm install -f "${api}.yaml" --name "jon-${api}-release" .
    popd
}

show_info(){
    kubectl get services
    kubectl get deployments
    kubectl get pods
    minikube service list
    helm list
}

main(){

    # Let's practice creating k8s resources using both k8s 
    # config files and helm charts.
    
    ############################################################
    # k8s config files
    ############################################################
    delete_ingresses || true
    delete_services || true
    delete_deployments || true
    
    create_deployments
    create_services
    create_ingresses

    ############################################################
    # helm charts
    ############################################################

    # This is not how things would work in production. Normamly,
    # we would do something like this:
    #
    # if chart exists then upgrade
    # else install;
    #
    # For this exercise, I'm taking a simpler approach of 
    # destroying everything then recreating.

    delete_helm_chart books || true

    install_helm_chart books
    
    echo "Pausing then showing k8s state"
    sleep 10
    show_info
}

main
