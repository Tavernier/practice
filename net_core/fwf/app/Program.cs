﻿using System;
using System.IO;

namespace fwf
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var reader = new StreamReader("/app/example_input.txt"))
            {
                Console.WriteLine("Begin reading text file.");
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    Console.WriteLine(line);

                    string city = line.Substring(0, 13).Trim();
                    string state = line.Substring(13, 6).Trim();
                    string country = line.Substring(20, 3).Trim();

                    Console.WriteLine(city);
                    Console.WriteLine(state);
                    Console.WriteLine(country);

                }
                Console.WriteLine("Done reading text file.");
            }
        }
    }
}
