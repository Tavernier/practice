# .Net Core

I wanted to play around .net core.  My code here is very hard-coded as my goal right now is "hello world" type work.

- hello_world: just that. a new console app.
- fwf: playing around with dotnet build then image creation in one docker build.

## Installtion

I followed Microsoft's installation instructions for OSX and ran into no issues.

## Hello World

I created a [hello world](https://docs.microsoft.com/en-us/dotnet/core/tutorials/using-with-xplat-cli) then followed their [docker guide](https://docs.microsoft.com/en-us/dotnet/core/docker/build-container).

```bash
dotnet run

dotnet publish -c Release

docker build . --tag jt-core-hw

docker run --rm jt-core-hw
```