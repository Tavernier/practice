def call() {

pipeline {

  agent {
    kubernetes {
      containerTemplate {
        name 'terraform'
        image 'hashicorp/terraform:0.12.12'
        ttyEnabled true
        command 'cat'
      }
    }
  }

  environment {
    TF_IN_AUTOMATION = 1
    TF_INPUT = 0
  }

  stages {

    stage('validate') {
        steps {
            container('terraform') {
              sh 'terraform version'
            }
        }
    }

    stage('plan') {
        steps {
            container('terraform') {
              sh 'terraform version'
            }
        }
    }

    stage('apply') {
        input {
          message "Do you want to apply the plan generated in the previous step?"
        }
        steps {
            container('terraform') {
              sh 'terraform version'
            }
        }
    }
  }
}


}