# Metabase

> Metabase is the easy, open source way for everyone in your company to ask questions and learn from data.

I played around with Metabase for an hour.  It made an excellent first impression.

## First Time Setup

- Create the common networks.
    - `../create_networks.sh`
- Start the backend databases.
    - `../backend/up.sh`
- Launch `./docker-compose.yaml`.
    - Metabase will launch plus other random data stores I add for testing purposes.
    - `docker-compose up --build --force`
- Visit `http://localhost:3000`.
    - Create my account.
    - Add the Postgres database.
        - Name: `Purchases`
        - Host: `db-postgres`
        - Port: `5432`
        - DB: `postgres`
        - User: `postgres`
        - Password: `blah`
        - I like the option of choosing what syncs and when.  I opted not to do that for now though.
    - Disable anonymous stats.

## Future Launches

- I decided to save the Metabase H2 db to make testing faster.  It's stored in `./h2db/` and gets mounted to the running Docker Container.
- Login email: `testing@testing.com`
- Password: `testing`

## Using Metabase

- After logging in, I see "Users" and "Orders" under "Our Data".
- I see options for X-ray on the Orders table, which shows me 50,000 total transactions.  It auto discovered the product category table too and permitted me to drill-down to see aggregate counts on that field. 
- Metabase shows the table comment I added in Postgres.  The columns comments do not appear.
- Metabase is making an excellent first impression!
    - The interface is fantastic.
    - It's fast and its auto discovery / ui is great.

### Data Model

I accessed the "Admin" mode then...

- Created a "Large Purchases" segment on the Orders table, which are orders over $75.
- Created "Number of Large Purchases" and "Average Amount Paid of Large Purchases" Metrics.
- Set the Order ID, which is the table's primary key, visibilty to "Only in Detail Views."
- Set the Order Number type to "Entity Name."  I'm unsure whether there's a better type.

I exited "Admin" mode then...

- Explore the new Metrics.  I like how easy it is to use the Metrics and further customize what I want to see (e.g. group by product category, change the viz type, etc.).
    - I wonder what options exist to control the Data Model via git, similar to LookML.
- Created a question, added some metrics, and chose the bar viz.
- Added this to my dashboard.
- Created a custom SQL question on orders over $90.
    - Added the order number and amount paid.
    - Added conditional formatting color scale to amount paid.
    - Had to multiply the amount paid field by `0.01` to get cents converted to dollars.

## Next Steps

I am thrilled with Metabase.  

- How does Metabase handle data?  How does its cache work?  Does it always cache?
- Can the Data Model and other resources be version controlled via git?
- How does scaling work?  Horizontally?  Vertically?
