# Redis

Redis is an in-memory key/value store.  It has lots of functionality beyond simple string key and string value storage.

## Launching

Version 4.0.9 is the current stable version as of April 2018 so I will play with that.  Execute `./launch.sh` to start Redis via Docker.

## Client Installation

The Redis website lists a [bunch of clients](https://redis.io/clients#python).  `redis-py` is the recommended client library.  That library's git repo is https://github.com/andymccurdy/redis-py and their documentation is located at http://redis-py.readthedocs.io/en/latest/.

Let's create a virtual environment then install the redis client:

```bash
mkvirtualenv redis_practice
python --version
pip install redis
``` 

## Functionality

### Key / Values

A key / value store with extra cool features:

* Expire a particular key and check its life with ttl.
* Increment and Decrement keys.

### Sets

Sets just like in SQL land!

* All the set operators one would expect (union, intersect, except).
* Determine whether a value is in a set.
* Store the new set based on the set operator.

#### Sorted Sets

### Pub / Sub

I need to play with this more.  It's not the same as other pub / subs I've explored.  I have questions such as:

* So, there's no offset so a new client only gets messages after they have joined?
* How does one get multiple messages in one round trip?


## Administration Stuff

### Authentication

### Disaster Recovery
