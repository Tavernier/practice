#!/usr/bin/env python3

import redis
import time
import random

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

for i in range(0, 10):
    clients_received = redis_client.publish(
        'observations',
        f"reading: {random.randint(1, 1000)}"
    )

    print(f"Received by {clients_received} clients")

    time.sleep(2)
