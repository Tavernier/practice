#!/usr/bin/env python3

import redis
import time
import random

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

max_items_to_keep = 20

new_list_length = redis_client.lpush(
    'favorites:10',
    *[f"item {i}" for i in range(random.randint(5, 47))]

)

if new_list_length > max_items_to_keep:
    redis_client.ltrim('favorites:10', 0, max_items_to_keep - 1)

print(f"Previous list length: {new_list_length}")
print(redis_client.lrange('favorites:10', 0, 30))
print(redis_client.llen('favorites:10'))

for _ in range(5000):
    print(redis_client.lrange('favorites:10', 0, random.randint(1, 20)))