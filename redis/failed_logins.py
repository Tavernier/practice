#!/usr/bin/env python3

"""Failed Logins

I wanted to play around with expirations and simple key sets/deletes here.
"""

import redis
import time
import random

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

# lock out after this number of failed login attempts in a row
# failed logins must occur within a minute
MAX_FAILED_LOGINS = 2
LOCKOUT_SECONDS = 20

def is_failed_login_attempt(random_number):
    return random_number < 5

def is_locked_out():
    return redis_client.exists('user-locked:1000')

# clear lock for testing purposes
# redis_client.delete('user-locked:1000')
failed_logins = 0

# let's try 100 logins
# 50% of login attempts will succeed
for i in range(0, 100):

    if is_locked_out():
        seconds_until_unlocked = redis_client.ttl('user-locked:1000')
        print(f"You are locked out! Try again in {seconds_until_unlocked} seconds.")
        time.sleep(3)
        continue
    
    if is_failed_login_attempt(random.randint(0, 9)):
        print("Failed login!")
        failed_logins = redis_client.incr('user-failed-login:1000')
        # expiration must be less than lock out time window
        redis_client.expire('user-failed-login:1000', LOCKOUT_SECONDS - 5)
    else:
        print("Success!")
        failed_logins = 0
        redis_client.delete('user-failed-login:1000')

    if failed_logins > MAX_FAILED_LOGINS:
        redis_client.set('user-locked:1000', 'too many failed logins')
        redis_client.expire('user-locked:1000', LOCKOUT_SECONDS)

    time.sleep(1)
