#!/usr/bin/env python3

import redis
import time

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

pubsub_client = redis_client.pubsub()

pubsub_client.subscribe('observations')

for i in range(0, 20):
    print(pubsub_client.get_message())
    time.sleep(3)