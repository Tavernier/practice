#!/usr/bin/env python3

import redis
import time

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

# single key set / del via hset / hdel
redis_client.hset(
    'user:10',
    'first_name',
    'Jon'
)

redis_client.hset(
    'user:10',
    'city',
    'Chicago'
)

# multi key set via hmset
redis_client.hmset(
    'user:10',
    {
        'food': 'cheeseburgers',
        'shoes': 'new balance',
        'cities': ",".join([
            'Chicago',
            'Buffalo',
            'Atlanta'
        ])
    }
)


# single get
print(redis_client.hget('user:10', 'first_name'))

# multiple get.  list returned is same as passed key order
print(redis_client.hmget('user:10', 'city', 'food'))

# get all the keys and their values
# returns a dictionary
print(redis_client.hgetall('user:10'))