#!/usr/bin/env python3

import redis
import time

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

# setting and getting a key.
redis_client.set('greeting', 'Hello World')
print(redis_client.get('greeting'))

# incrementing a key.
for i in range(0,3):
    redis_client.incr('visitors')
    print("Visitors: ", redis_client.get('visitors'))
    time.sleep(1)

# setting TTL on a key, which will delete after N seconds.
# see also expireat to expire at a specific time
# and persist to remove the key's expiration.
redis_client.set(
    'system_message', 
    'This message will self destruct.'
    )

redis_client.expire('system_message', 8)

time.sleep(5)

print("Seconds remaining: ", redis_client.ttl('system_message'))
print(redis_client.get('system_message'))

time.sleep(5)

print(redis_client.get('system_message'))
