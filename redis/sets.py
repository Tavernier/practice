#!/usr/bin/env python3

import redis
import time

# default is that redis appears to be open to the world.
# i need to read up on credentials, databases, and
# connection pooling.
redis_client = redis.StrictRedis(
    host='localhost',
    port=6379,
    db=0,
    decode_responses=True  # return strings, not bytes
)

# redis has two flavors of sets: sets and sorted sets 
# i'll look at sets first

redis_client.sadd(
    'cities',
    'Chicago',
    'Madison',
    'Buffalo'
)

# sets are unsorted so the type here is indeed a python set
print(
    redis_client.smembers('cities'),
    type(redis_client.smembers('cities'))
)

print(redis_client.sismember('cities','Chicago'))

redis_client.sadd(
    'towns',
    'Valpo',
    'Oak Park',
    'Buffalo'
)

# union
print(redis_client.sunion('cities', 'towns'))

# except: subtract multiple stes from left to right
print(redis_client.sdiff('cities', 'towns'))

# intersection
print(redis_client.sinter('cities', 'towns'))
