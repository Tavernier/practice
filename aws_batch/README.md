# AWS Batch

AWS batch looks pretty sweet.  You can easily define a compute cluster then submit jobs (e.g. a Docker container to run to completion).  You need not worry about spinning up actual boxes.  AWS will create and destroy EC2 instances as needed.

Regarding the jobs themselves, AWS batch tracks job dependencies, which is fantastic.

AWS Batch excludes scheduling so that still needs to be addressed (via Airflow or AWS Cloudwatch scheduled events for instance).
