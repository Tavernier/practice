#!/usr/bin/env python3
"""AWS Batch Example

I wrote this as a quick way to illustrate using AWS Batch.
So, I hard-coded some values such as the job queue and definition names.

Expected environment variables (per boto3):
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
"""

import boto3
import logging
import sys

AWS_CLIENT = boto3.client(
    'batch',
    region_name='us-east-1'
)

AWS_BATCH_MAX_DEPENDS = 20

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s:%(levelname)s:%(message)s"
)

def submit_batch_job(job_name, job_dependencies):
    """Submit an AWS batch job and returns its job_id."""

    dependencies = job_dependencies or []

    if len(dependencies) > 0 and len(dependencies) <= AWS_BATCH_MAX_DEPENDS:
        logging.info(f"submitting {job_name} with {len(dependencies)} dependencies: {','.join(dependencies)}")
    elif len(dependencies) > AWS_BATCH_MAX_DEPENDS:
        msg = f'Max AWS job dependencies is {AWS_BATCH_MAX_DEPENDS}. Submitted are {dependencies}.'
        logging.info(msg)
        raise ValueError(msg)
    else:
        logging.info(f"submitting {job_name}")

    job_info = AWS_CLIENT.submit_job(
        jobName=job_name,
        jobQueue='first-run-job-queue',
        jobDefinition='first-run-job-definition:2',
        dependsOn=get_aws_depends_on(dependencies)
    )

    job_id = job_info['jobId']
    logging.info(f"submitted {job_name}, job_id: {job_id}")

    return job_id

def get_aws_depends_on(job_ids):
    """Given a list of Job IDs, this will return the proper AWS Batch dependecy structure."""
    return [{'jobId': id} for id in job_ids]

if __name__=="__main__":
    logging.error("Do not speak directly to the commander!")
    sys.exit(1)
