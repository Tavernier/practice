#!/usr/bin/env python3
"""Example of starting dependent jobs on AWS Batch.

All retry logic is deferred to AWS Batch.
Failures are okay so if an exception occurs, we expect it to bring things to a stop.
These are batch workloads (i.e. not mission critical).

In this example, I wanted to see how job dependencies and parallel jobs worked.
"""

import commander

# first, let's create some states like Indiana and Wisconsin.
states_job_id = commander.submit_batch_job('build_states', None)

# then, divide those states up into counties.
counties_job_id = commander.submit_batch_job('build_counties', [states_job_id])

# once the counties are defined, we can build cities and roads.
cities_job_id = commander.submit_batch_job('build_cities', [counties_job_id])

roads_job_id = commander.submit_batch_job('build_roads', [counties_job_id])

# finally, let's start adding homes since the cities and roads now exist.
homes_id = commander.submit_batch_job('build_homes', [cities_job_id, roads_job_id])
